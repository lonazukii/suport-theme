-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 12, 2018 at 09:44 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `customp`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_bp_activity`
--

CREATE TABLE `wp_bp_activity` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `component` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `primary_link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `secondary_item_id` bigint(20) DEFAULT NULL,
  `date_recorded` datetime NOT NULL,
  `hide_sitewide` tinyint(1) DEFAULT '0',
  `mptt_left` int(11) NOT NULL DEFAULT '0',
  `mptt_right` int(11) NOT NULL DEFAULT '0',
  `is_spam` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_bp_activity`
--

INSERT INTO `wp_bp_activity` (`id`, `user_id`, `component`, `type`, `action`, `content`, `primary_link`, `item_id`, `secondary_item_id`, `date_recorded`, `hide_sitewide`, `mptt_left`, `mptt_right`, `is_spam`) VALUES
(1, 1, 'members', 'last_activity', '', '', '', 0, NULL, '2018-09-12 07:38:09', 0, 0, 0, 0),
(2, 2, 'members', 'new_member', '<a href=\"http://localhost/customp/members/cebongcaper/\">CEBONG_CAPER</a> became a registered member', '', 'http://localhost/customp/members/cebong/', 0, 0, '2018-09-03 03:58:20', 0, 0, 0, 0),
(3, 3, 'members', 'new_member', '<a href=\"http://localhost/customp/members/lonazukii/\">kii</a> became a registered member', '', 'http://localhost/customp/members/cebong/', 0, 0, '2018-09-03 03:58:51', 0, 0, 0, 0),
(4, 2, 'members', 'last_activity', '', '', '', 0, NULL, '2018-09-03 05:48:38', 0, 0, 0, 0),
(5, 1, 'bbpress', 'bbp_topic_create', '<a href=\"http://localhost/customp/members/cebong/\" rel=\"nofollow\">cebong</a> started the topic <a href=\"http://localhost/customp/forums/topic/faq/\">FAQ</a> in the forum <a href=\"http://localhost/customp/forums/topic/faq/\">FAQ</a>', '', 'http://localhost/customp/forums/topic/faq/', 54, 54, '2018-09-03 05:39:02', 0, 0, 0, 0),
(6, 1, 'bbpress', 'bbp_topic_create', '<a href=\"http://localhost/customp/members/cebong/\" rel=\"nofollow\">cebong</a> started the topic <a href=\"http://localhost/customp/forums/topic/customize-gain-theme/\">Customize Gain Theme</a> in the forum <a href=\"http://localhost/customp/?post_type=topic&amp;p=56\">Customize Gain Theme</a>', '', 'http://localhost/customp/forums/topic/customize-gain-theme/', 56, 0, '2018-09-03 05:39:40', 1, 0, 0, 0),
(7, 2, 'bbpress', 'bbp_reply_create', '<a href=\"http://localhost/customp/members/cebongcaper/\" rel=\"nofollow\">CEBONG_CAPER</a> replied to the topic <a href=\"http://localhost/customp/forums/topic/customize-gain-theme/\">Customize Gain Theme</a> in the forum <a href=\"http://localhost/customp/forums/forum/gain-theme/\">Gain Theme</a>', 'Dik sakedik sakedik', 'http://localhost/customp/forums/topic/customize-gain-theme/#post-58', 58, 56, '2018-09-03 05:45:16', 0, 0, 0, 0),
(9, 3, 'members', 'last_activity', '', '', '', 0, NULL, '2018-09-03 05:49:00', 0, 0, 0, 0),
(10, 3, 'bbpress', 'bbp_reply_create', '<a href=\"http://localhost/customp/members/lonazukii/\" rel=\"nofollow\">kii</a> replied to the topic <a href=\"http://localhost/customp/forums/topic/customize-gain-theme/\">Customize Gain Theme</a> in the forum <a href=\"http://localhost/customp/forums/forum/gain-theme/\">Gain Theme</a>', 'Lorem ipsum Dolor Sit Amet', 'http://localhost/customp/forums/topic/customize-gain-theme/#post-61', 61, 56, '2018-09-03 05:52:08', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_bp_activity_meta`
--

CREATE TABLE `wp_bp_activity_meta` (
  `id` bigint(20) NOT NULL,
  `activity_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_bp_notifications`
--

CREATE TABLE `wp_bp_notifications` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `secondary_item_id` bigint(20) DEFAULT NULL,
  `component_name` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `component_action` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_notified` datetime NOT NULL,
  `is_new` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_bp_notifications`
--

INSERT INTO `wp_bp_notifications` (`id`, `user_id`, `item_id`, `secondary_item_id`, `component_name`, `component_action`, `date_notified`, `is_new`) VALUES
(1, 1, 56, 2, 'forums', 'bbp_new_reply', '2018-09-03 05:45:16', 1),
(2, 1, 56, 3, 'forums', 'bbp_new_reply', '2018-09-03 05:52:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_bp_notifications_meta`
--

CREATE TABLE `wp_bp_notifications_meta` (
  `id` bigint(20) NOT NULL,
  `notification_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_bp_xprofile_data`
--

CREATE TABLE `wp_bp_xprofile_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `field_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_bp_xprofile_data`
--

INSERT INTO `wp_bp_xprofile_data` (`id`, `field_id`, `user_id`, `value`, `last_updated`) VALUES
(1, 1, 2, 'CEBONG_CAPER', '2018-09-03 03:40:15'),
(2, 1, 3, 'kii', '2018-09-03 03:49:00');

-- --------------------------------------------------------

--
-- Table structure for table `wp_bp_xprofile_fields`
--

CREATE TABLE `wp_bp_xprofile_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_required` tinyint(1) NOT NULL DEFAULT '0',
  `is_default_option` tinyint(1) NOT NULL DEFAULT '0',
  `field_order` bigint(20) NOT NULL DEFAULT '0',
  `option_order` bigint(20) NOT NULL DEFAULT '0',
  `order_by` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `can_delete` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_bp_xprofile_fields`
--

INSERT INTO `wp_bp_xprofile_fields` (`id`, `group_id`, `parent_id`, `type`, `name`, `description`, `is_required`, `is_default_option`, `field_order`, `option_order`, `order_by`, `can_delete`) VALUES
(1, 1, 0, 'textbox', 'Name', '', 1, 0, 0, 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_bp_xprofile_groups`
--

CREATE TABLE `wp_bp_xprofile_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_order` bigint(20) NOT NULL DEFAULT '0',
  `can_delete` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_bp_xprofile_groups`
--

INSERT INTO `wp_bp_xprofile_groups` (`id`, `name`, `description`, `group_order`, `can_delete`) VALUES
(1, 'Base', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_bp_xprofile_meta`
--

CREATE TABLE `wp_bp_xprofile_meta` (
  `id` bigint(20) NOT NULL,
  `object_id` bigint(20) NOT NULL,
  `object_type` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_cfs_sessions`
--

CREATE TABLE `wp_cfs_sessions` (
  `id` varchar(32) NOT NULL,
  `data` text,
  `expires` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_cfs_sessions`
--

INSERT INTO `wp_cfs_sessions` (`id`, `data`, `expires`) VALUES
('0e49744a9e92407e0e18d8b22728fcc4', 'a:7:{s:7:\"post_id\";i:6;s:9:\"post_type\";s:4:\"post\";s:11:\"post_status\";s:5:\"draft\";s:12:\"field_groups\";a:1:{i:0;i:10;}s:20:\"confirmation_message\";s:0:\"\";s:16:\"confirmation_url\";s:0:\"\";s:9:\"front_end\";b:0;}', '1536578726'),
('23ffae2f4404f771a30bad970d1285e2', 'a:7:{s:7:\"post_id\";i:1;s:9:\"post_type\";s:4:\"post\";s:11:\"post_status\";s:5:\"draft\";s:12:\"field_groups\";a:1:{i:0;i:10;}s:20:\"confirmation_message\";s:0:\"\";s:16:\"confirmation_url\";s:0:\"\";s:9:\"front_end\";b:0;}', '1536578877'),
('3ff6eaef5f4c010530545aa7f8a9134d', 'a:7:{s:7:\"post_id\";i:1;s:9:\"post_type\";s:4:\"post\";s:11:\"post_status\";s:5:\"draft\";s:12:\"field_groups\";a:1:{i:0;i:10;}s:20:\"confirmation_message\";s:0:\"\";s:16:\"confirmation_url\";s:0:\"\";s:9:\"front_end\";b:0;}', '1536737162'),
('74d67470f709f8fcfb9b0fb069570461', 'a:7:{s:7:\"post_id\";i:1;s:9:\"post_type\";s:4:\"post\";s:11:\"post_status\";s:5:\"draft\";s:12:\"field_groups\";a:1:{i:0;i:10;}s:20:\"confirmation_message\";s:0:\"\";s:16:\"confirmation_url\";s:0:\"\";s:9:\"front_end\";b:0;}', '1536577787'),
('78e6128c220516c75d8a83426c884c0c', 'a:7:{s:7:\"post_id\";i:1;s:9:\"post_type\";s:4:\"post\";s:11:\"post_status\";s:5:\"draft\";s:12:\"field_groups\";a:1:{i:0;i:10;}s:20:\"confirmation_message\";s:0:\"\";s:16:\"confirmation_url\";s:0:\"\";s:9:\"front_end\";b:0;}', '1536577924'),
('8ced282c53c443e2429fa92ae6552e95', 'a:7:{s:7:\"post_id\";i:1;s:9:\"post_type\";s:4:\"post\";s:11:\"post_status\";s:5:\"draft\";s:12:\"field_groups\";a:1:{i:0;i:10;}s:20:\"confirmation_message\";s:0:\"\";s:16:\"confirmation_url\";s:0:\"\";s:9:\"front_end\";b:0;}', '1536741063'),
('d735c46ca13367011f35b4075f312d19', 'a:7:{s:7:\"post_id\";i:1;s:9:\"post_type\";s:4:\"post\";s:11:\"post_status\";s:5:\"draft\";s:12:\"field_groups\";a:1:{i:0;i:10;}s:20:\"confirmation_message\";s:0:\"\";s:16:\"confirmation_url\";s:0:\"\";s:9:\"front_end\";b:0;}', '1536577898');

-- --------------------------------------------------------

--
-- Table structure for table `wp_cfs_values`
--

CREATE TABLE `wp_cfs_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `field_id` int(10) UNSIGNED DEFAULT NULL,
  `meta_id` int(10) UNSIGNED DEFAULT NULL,
  `post_id` int(10) UNSIGNED DEFAULT NULL,
  `base_field_id` int(10) UNSIGNED DEFAULT '0',
  `hierarchy` text,
  `depth` int(10) UNSIGNED DEFAULT '0',
  `weight` int(10) UNSIGNED DEFAULT '0',
  `sub_weight` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_cfs_values`
--

INSERT INTO `wp_cfs_values` (`id`, `field_id`, `meta_id`, `post_id`, `base_field_id`, `hierarchy`, `depth`, `weight`, `sub_weight`) VALUES
(57, 3, 690, 6, 1, '1:0:3', 1, 0, 0),
(58, 2, 691, 6, 1, '1:0:2', 1, 0, 0),
(59, 3, 692, 6, 1, '1:1:3', 1, 1, 0),
(60, 2, 693, 6, 1, '1:1:2', 1, 1, 0),
(61, 3, 694, 6, 1, '1:2:3', 1, 2, 0),
(62, 2, 695, 6, 1, '1:2:2', 1, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-09-03 02:34:33', '2018-09-03 02:34:33', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_logy_users`
--

CREATE TABLE `wp_logy_users` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `provider` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identifier` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `websiteurl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profileurl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photourl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `displayname` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` int(11) NOT NULL,
  `birthmonth` int(11) NOT NULL,
  `birthyear` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `emailverified` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `region` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_hash` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/customp', 'yes'),
(2, 'home', 'http://localhost/customp', 'yes'),
(3, 'blogname', 'Customp', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '1', 'yes'),
(6, 'admin_email', 'aridyansyah.kii@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/theme/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:9:{i:0;s:57:\"acf-content-analysis-for-yoast-seo/yoast-acf-analysis.php\";i:1;s:30:\"advanced-custom-fields/acf.php\";i:2;s:19:\"bbpress/bbpress.php\";i:3;s:24:\"buddypress/bp-loader.php\";i:4;s:26:\"custom-field-suite/cfs.php\";i:5;s:31:\"what-the-file/what-the-file.php\";i:6;s:24:\"wordpress-seo/wp-seo.php\";i:7;s:17:\"youzer/youzer.php\";i:8;s:22:\"yuuki/yuuki-plugin.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:3:{i:0;s:67:\"/opt/lampp/htdocs/customp/wp-content/themes/suport-theme/single.php\";i:1;s:87:\"/opt/lampp/htdocs/customp/wp-content/themes/suport-theme/template-parts/content-doc.php\";i:2;s:0:\"\";}', 'no'),
(40, 'template', 'suport-theme', 'yes'),
(41, 'stylesheet', 'suport-theme', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '0', 'yes'),
(93, 'initial_db_version', '38590', 'yes'),
(94, 'wp_user_roles', 'a:12:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:62:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:20:\"wpseo_manage_options\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:13:\"bbp_keymaster\";a:2:{s:4:\"name\";s:9:\"Keymaster\";s:12:\"capabilities\";a:0:{}}s:13:\"bbp_spectator\";a:2:{s:4:\"name\";s:9:\"Spectator\";s:12:\"capabilities\";a:0:{}}s:11:\"bbp_blocked\";a:2:{s:4:\"name\";s:7:\"Blocked\";s:12:\"capabilities\";a:0:{}}s:13:\"bbp_moderator\";a:2:{s:4:\"name\";s:9:\"Moderator\";s:12:\"capabilities\";a:0:{}}s:15:\"bbp_participant\";a:2:{s:4:\"name\";s:11:\"Participant\";s:12:\"capabilities\";a:0:{}}s:13:\"wpseo_manager\";a:2:{s:4:\"name\";s:11:\"SEO Manager\";s:12:\"capabilities\";a:37:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;s:20:\"wpseo_manage_options\";b:1;}}s:12:\"wpseo_editor\";a:2:{s:4:\"name\";s:10:\"SEO Editor\";s:12:\"capabilities\";a:36:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:7:{s:19:\"wp_inactive_widgets\";a:0:{}s:15:\"yz-wall-sidebar\";a:0:{}s:17:\"yz-groups-sidebar\";a:0:{}s:16:\"yz-forum-sidebar\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:8:\"header-1\";a:1:{i:0;s:13:\"media_image-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_image', 'a:2:{i:2;a:15:{s:13:\"attachment_id\";i:65;s:3:\"url\";s:67:\"http://localhost/customp/wp-content/uploads/2018/09/logo-300x77.png\";s:5:\"title\";s:0:\"\";s:4:\"size\";s:6:\"medium\";s:5:\"width\";i:528;s:6:\"height\";i:135;s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:9:\"link_type\";s:6:\"custom\";s:8:\"link_url\";s:25:\"http://localhost/customp/\";s:13:\"image_classes\";s:0:\"\";s:12:\"link_classes\";s:0:\"\";s:8:\"link_rel\";s:0:\"\";s:17:\"link_target_blank\";b:0;s:11:\"image_title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'nonce_key', '(U`1Be<K_kUN9DH&DUaJK[oU,|v6~nwhY>*RU|C]U/r;=Ow$|:Z?htDd8me{/m1<', 'no'),
(109, 'nonce_salt', '/9ams;}8Ap5;<.dxpWp9dqaN4+G3%kVg$d-@`dRYgt bFbF;WL2hWNGV,@q ^PEp', 'no'),
(110, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'cron', 'a:6:{i:1536741275;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1536762875;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1536806091;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1536806288;a:1:{s:19:\"wpseo-reindex-links\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1536806307;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(114, 'theme_mods_twentyseventeen', 'a:3:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1536563715;s:4:\"data\";a:7:{s:19:\"wp_inactive_widgets\";a:1:{i:0;s:13:\"media_image-2\";}s:15:\"yz-wall-sidebar\";a:0:{}s:17:\"yz-groups-sidebar\";a:0:{}s:16:\"yz-forum-sidebar\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}s:18:\"nav_menu_locations\";a:1:{s:3:\"top\";i:19;}}', 'yes'),
(118, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.8.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.8.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.9.8-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.9.8-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.9.8\";s:7:\"version\";s:5:\"4.9.8\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1536719951;s:15:\"version_checked\";s:5:\"4.9.8\";s:12:\"translations\";a:0:{}}', 'no'),
(126, 'auth_key', 'XP]{ne_inPy<*j*v)zya@%CNKk0hO6phx.aR)gBi^fh_3G<<R!~0=7_Gm>nHX}nU', 'no'),
(127, 'auth_salt', ',rZ-5O[iW(oB(:<.;5M7w!HGQR%+8],W3dOpg!3bbOuVoQL`~z.B.39?KNQqd3Iv', 'no'),
(128, 'logged_in_key', 'xu3dLk}C]T/CA[6mzGn+JZ(nQtdz?or,KSk5hEVO47p^)bg@vfX]CLoT[!`Be[q}', 'no'),
(129, 'logged_in_salt', '3>JLO?Z^9DaG1#i$ZI~GRb]0+<R$txLB-!=Hf.AQ!=/hAeOjs1f0DrB0H?1Ku2gg', 'no'),
(133, 'can_compress_scripts', '1', 'no'),
(143, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1536716703;s:7:\"checked\";a:4:{s:12:\"suport-theme\";s:5:\"1.0.0\";s:13:\"twentyfifteen\";s:3:\"2.0\";s:15:\"twentyseventeen\";s:3:\"1.7\";s:13:\"twentysixteen\";s:3:\"1.5\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(147, 'recently_activated', 'a:0:{}', 'yes'),
(148, 'acf_version', '4.4.12', 'yes'),
(151, 'widget_bbp_login_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(152, 'widget_bbp_views_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(153, 'widget_bbp_search_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(154, 'widget_bbp_forums_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(155, 'widget_bbp_topics_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(156, 'widget_bbp_replies_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(157, 'widget_bbp_stats_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(158, '_bbp_private_forums', 'a:0:{}', 'yes'),
(159, '_bbp_hidden_forums', 'a:0:{}', 'yes'),
(160, '_bbp_db_version', '250', 'yes'),
(161, 'wpseo', 'a:19:{s:15:\"ms_defaults_set\";b:0;s:7:\"version\";s:5:\"8.1.2\";s:20:\"disableadvanced_meta\";b:1;s:19:\"onpage_indexability\";b:1;s:11:\"baiduverify\";s:0:\"\";s:12:\"googleverify\";s:0:\"\";s:8:\"msverify\";s:0:\"\";s:12:\"yandexverify\";s:0:\"\";s:9:\"site_type\";s:0:\"\";s:20:\"has_multiple_authors\";s:0:\"\";s:16:\"environment_type\";s:0:\"\";s:23:\"content_analysis_active\";b:1;s:23:\"keyword_analysis_active\";b:1;s:21:\"enable_admin_bar_menu\";b:1;s:26:\"enable_cornerstone_content\";b:1;s:18:\"enable_xml_sitemap\";b:1;s:24:\"enable_text_link_counter\";b:1;s:22:\"show_onboarding_notice\";b:1;s:18:\"first_activated_on\";i:1535942287;}', 'yes'),
(162, 'wpseo_titles', 'a:94:{s:10:\"title_test\";i:0;s:17:\"forcerewritetitle\";b:0;s:9:\"separator\";s:7:\"sc-dash\";s:16:\"title-home-wpseo\";s:42:\"%%sitename%% %%page%% %%sep%% %%sitedesc%%\";s:18:\"title-author-wpseo\";s:41:\"%%name%%, Author at %%sitename%% %%page%%\";s:19:\"title-archive-wpseo\";s:38:\"%%date%% %%page%% %%sep%% %%sitename%%\";s:18:\"title-search-wpseo\";s:63:\"You searched for %%searchphrase%% %%page%% %%sep%% %%sitename%%\";s:15:\"title-404-wpseo\";s:35:\"Page not found %%sep%% %%sitename%%\";s:19:\"metadesc-home-wpseo\";s:0:\"\";s:21:\"metadesc-author-wpseo\";s:0:\"\";s:22:\"metadesc-archive-wpseo\";s:0:\"\";s:9:\"rssbefore\";s:0:\"\";s:8:\"rssafter\";s:53:\"The post %%POSTLINK%% appeared first on %%BLOGLINK%%.\";s:20:\"noindex-author-wpseo\";b:0;s:28:\"noindex-author-noposts-wpseo\";b:1;s:21:\"noindex-archive-wpseo\";b:1;s:14:\"disable-author\";b:0;s:12:\"disable-date\";b:0;s:19:\"disable-post_format\";b:0;s:18:\"disable-attachment\";b:1;s:23:\"is-media-purge-relevant\";b:0;s:20:\"breadcrumbs-404crumb\";s:25:\"Error 404: Page not found\";s:29:\"breadcrumbs-display-blog-page\";b:0;s:20:\"breadcrumbs-boldlast\";b:0;s:25:\"breadcrumbs-archiveprefix\";s:12:\"Archives for\";s:18:\"breadcrumbs-enable\";b:1;s:16:\"breadcrumbs-home\";s:4:\"Home\";s:18:\"breadcrumbs-prefix\";s:0:\"\";s:24:\"breadcrumbs-searchprefix\";s:16:\"You searched for\";s:15:\"breadcrumbs-sep\";s:4:\"&gt;\";s:12:\"website_name\";s:0:\"\";s:11:\"person_name\";s:0:\"\";s:22:\"alternate_website_name\";s:0:\"\";s:12:\"company_logo\";s:0:\"\";s:12:\"company_name\";s:0:\"\";s:17:\"company_or_person\";s:0:\"\";s:17:\"stripcategorybase\";b:0;s:10:\"title-post\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-post\";s:0:\"\";s:12:\"noindex-post\";b:0;s:13:\"showdate-post\";b:0;s:23:\"display-metabox-pt-post\";b:1;s:10:\"title-page\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-page\";s:0:\"\";s:12:\"noindex-page\";b:0;s:13:\"showdate-page\";b:0;s:23:\"display-metabox-pt-page\";b:1;s:16:\"title-attachment\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:19:\"metadesc-attachment\";s:0:\"\";s:18:\"noindex-attachment\";b:0;s:19:\"showdate-attachment\";b:0;s:29:\"display-metabox-pt-attachment\";b:1;s:18:\"title-tax-category\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-category\";s:0:\"\";s:28:\"display-metabox-tax-category\";b:1;s:20:\"noindex-tax-category\";b:0;s:18:\"title-tax-post_tag\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-post_tag\";s:0:\"\";s:28:\"display-metabox-tax-post_tag\";b:1;s:20:\"noindex-tax-post_tag\";b:0;s:21:\"title-tax-post_format\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-post_format\";s:0:\"\";s:31:\"display-metabox-tax-post_format\";b:0;s:23:\"noindex-tax-post_format\";b:1;s:23:\"post_types-post-maintax\";s:8:\"post_tag\";s:11:\"title-forum\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:14:\"metadesc-forum\";s:0:\"\";s:13:\"noindex-forum\";b:0;s:14:\"showdate-forum\";b:0;s:24:\"display-metabox-pt-forum\";b:1;s:11:\"title-topic\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:14:\"metadesc-topic\";s:0:\"\";s:13:\"noindex-topic\";b:0;s:14:\"showdate-topic\";b:0;s:24:\"display-metabox-pt-topic\";b:1;s:11:\"title-reply\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:14:\"metadesc-reply\";s:0:\"\";s:13:\"noindex-reply\";b:0;s:14:\"showdate-reply\";b:0;s:24:\"display-metabox-pt-reply\";b:1;s:21:\"title-ptarchive-forum\";s:51:\"%%pt_plural%% Archive %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-ptarchive-forum\";s:0:\"\";s:23:\"bctitle-ptarchive-forum\";s:0:\"\";s:23:\"noindex-ptarchive-forum\";b:0;s:21:\"title-ptarchive-topic\";s:51:\"%%pt_plural%% Archive %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-ptarchive-topic\";s:0:\"\";s:23:\"bctitle-ptarchive-topic\";s:0:\"\";s:23:\"noindex-ptarchive-topic\";b:0;s:19:\"title-tax-topic-tag\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:22:\"metadesc-tax-topic-tag\";s:0:\"\";s:29:\"display-metabox-tax-topic-tag\";b:1;s:21:\"noindex-tax-topic-tag\";b:0;s:24:\"post_types-topic-maintax\";i:0;s:27:\"taxonomy-topic-tag-ptparent\";i:0;}', 'yes'),
(163, 'wpseo_social', 'a:18:{s:13:\"facebook_site\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:11:\"myspace_url\";s:0:\"\";s:16:\"og_default_image\";s:0:\"\";s:18:\"og_frontpage_title\";s:0:\"\";s:17:\"og_frontpage_desc\";s:0:\"\";s:18:\"og_frontpage_image\";s:0:\"\";s:9:\"opengraph\";b:1;s:13:\"pinterest_url\";s:0:\"\";s:15:\"pinterestverify\";s:0:\"\";s:14:\"plus-publisher\";s:0:\"\";s:7:\"twitter\";b:1;s:12:\"twitter_site\";s:0:\"\";s:17:\"twitter_card_type\";s:19:\"summary_large_image\";s:11:\"youtube_url\";s:0:\"\";s:15:\"google_plus_url\";s:0:\"\";s:10:\"fbadminapp\";s:0:\"\";}', 'yes'),
(164, 'wpseo_flush_rewrite', '1', 'yes'),
(165, '_transient_timeout_wpseo_link_table_inaccessible', '1567478288', 'no'),
(166, '_transient_wpseo_link_table_inaccessible', '0', 'no'),
(167, '_transient_timeout_wpseo_meta_table_inaccessible', '1567478288', 'no'),
(168, '_transient_wpseo_meta_table_inaccessible', '0', 'no'),
(170, 'whatthefile-install-date', '2018-09-03', 'no'),
(174, 'wpseo_sitemap_1_cache_validator', 'PxC4', 'no'),
(175, 'wpseo_sitemap_forum_cache_validator', 'H14t', 'no'),
(177, 'current_theme', 'Suport Theme', 'yes'),
(178, 'theme_mods_suport-theme', 'a:5:{i:0;b:0;s:18:\"nav_menu_locations\";a:2:{s:6:\"menu-1\";i:19;s:6:\"menu-2\";i:21;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1536563602;s:4:\"data\";a:6:{s:19:\"wp_inactive_widgets\";a:0:{}s:15:\"yz-wall-sidebar\";a:0:{}s:17:\"yz-groups-sidebar\";a:0:{}s:16:\"yz-forum-sidebar\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:8:\"header-1\";a:1:{i:0;s:13:\"media_image-2\";}}}s:16:\"header_textcolor\";s:5:\"blank\";}', 'yes'),
(179, 'theme_switched', '', 'yes'),
(183, 'wpseo_sitemap_post_cache_validator', 'cqBB', 'no'),
(184, 'wpseo_sitemap_category_cache_validator', 'cqCi', 'no'),
(185, 'wpseo_sitemap_attachment_cache_validator', 'cp9a', 'no'),
(187, 'wpseo_sitemap_acf_cache_validator', '2Ownj', 'no'),
(189, 'cfs_next_field_id', '4', 'yes'),
(190, 'cfs_version', '2.5.12', 'yes'),
(195, 'wpseo_sitemap_cfs_cache_validator', 'OJuz', 'no'),
(199, 'wpseo_sitemap_6_cache_validator', 'P86h', 'no'),
(201, 'wpseo_taxonomy_meta', 'a:1:{s:8:\"category\";a:1:{i:2;a:3:{s:13:\"wpseo_focuskw\";s:6:\"parent\";s:13:\"wpseo_linkdex\";s:2:\"26\";s:19:\"wpseo_content_score\";s:2:\"30\";}}}', 'yes'),
(202, 'category_children', 'a:0:{}', 'yes'),
(212, 'bp-deactivated-components', 'a:0:{}', 'yes'),
(213, 'bp-xprofile-base-group-name', 'Base', 'yes'),
(214, 'bp-xprofile-fullname-field-name', 'Name', 'yes'),
(215, 'bp-blogs-first-install', '', 'yes'),
(216, 'bp-disable-profile-sync', '', 'yes'),
(217, 'hide-loggedout-adminbar', '1', 'yes'),
(218, 'bp-disable-avatar-uploads', '', 'yes'),
(219, 'bp-disable-cover-image-uploads', '', 'yes'),
(220, 'bp-disable-group-avatar-uploads', '', 'yes'),
(221, 'bp-disable-group-cover-image-uploads', '', 'yes'),
(222, 'bp-disable-account-deletion', '', 'yes'),
(223, 'bp-disable-blogforum-comments', '1', 'yes'),
(224, '_bp_theme_package_id', 'nouveau', 'yes'),
(225, 'bp-emails-unsubscribe-salt', 'YSpwUXlpS2ZeUiBiXjtrLjEtRSolWGMwT2cgNW9zUmBgVyRDYUozM01XbzY/dDsqQEdPQC1OOVt0TEc/aypLcg==', 'yes'),
(226, 'bp_restrict_group_creation', '', 'yes'),
(227, '_bp_enable_akismet', '1', 'yes'),
(228, '_bp_enable_heartbeat_refresh', '1', 'yes'),
(229, '_bp_force_buddybar', '', 'yes'),
(230, '_bp_retain_bp_default', '', 'yes'),
(231, '_bp_ignore_deprecated_code', '1', 'yes'),
(232, 'widget_bp_core_login_widget', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(233, 'widget_bp_core_members_widget', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(234, 'widget_bp_core_whos_online_widget', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(235, 'widget_bp_core_recently_active_widget', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(236, 'widget_bp_groups_widget', '', 'yes'),
(237, 'widget_bp_messages_sitewide_notices_widget', '', 'yes'),
(242, 'widget_bp_nouveau_sidebar_object_nav_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(243, 'bp-active-components', 'a:5:{s:8:\"activity\";i:1;s:7:\"members\";i:1;s:8:\"settings\";i:1;s:8:\"xprofile\";i:1;s:13:\"notifications\";i:1;}', 'yes'),
(244, 'bp-pages', 'a:4:{s:7:\"members\";i:13;s:8:\"activity\";i:12;s:8:\"register\";i:42;s:8:\"activate\";i:43;}', 'yes'),
(245, '_bp_db_version', '11105', 'yes'),
(246, 'wpseo_sitemap_page_cache_validator', '9Qvo', 'no'),
(247, 'wpseo_sitemap_bp-email_cache_validator', 'RZ3E', 'no'),
(248, 'widget_bp_latest_activities', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(255, 'yz_social_networks', 'a:4:{s:7:\"yz_sn_1\";a:3:{s:4:\"icon\";s:8:\"facebook\";s:4:\"name\";s:8:\"facebook\";s:5:\"color\";s:7:\"#4987bd\";}s:7:\"yz_sn_2\";a:3:{s:4:\"icon\";s:11:\"google-plus\";s:4:\"name\";s:11:\"google plus\";s:5:\"color\";s:7:\"#ed4242\";}s:7:\"yz_sn_3\";a:3:{s:4:\"icon\";s:7:\"twitter\";s:4:\"name\";s:7:\"twitter\";s:5:\"color\";s:7:\"#63CDF1\";}s:7:\"yz_sn_4\";a:3:{s:4:\"icon\";s:9:\"instagram\";s:4:\"name\";s:9:\"instagram\";s:5:\"color\";s:7:\"#ffcd21\";}}', 'yes'),
(256, 'yz_next_snetwork_nbr', '5', 'yes'),
(257, 'yz_next_widget_nbr', '1', 'yes'),
(258, 'yz_next_field_nbr', '1', 'yes'),
(259, 'yz_next_ad_nbr', '1', 'yes'),
(260, 'yz_next_custom_widget_nbr', '1', 'yes'),
(261, 'yz_next_custom_tab_nbr', '1', 'yes'),
(262, 'yz_next_user_tag_nbr', '1', 'yes'),
(263, 'logy_social_providers', 'a:5:{i:0;s:8:\"Facebook\";i:1;s:7:\"Twitter\";i:2;s:6:\"Google\";i:3;s:8:\"LinkedIn\";i:4;s:9:\"Instagram\";}', 'yes'),
(264, 'yz_profile_default_tab', 'overview', 'yes'),
(265, 'install_youzer_2.0.1_options', '1', 'yes'),
(266, 'yz_profile_sidebar_widgets', 'a:13:{i:0;a:1:{s:12:\"user_balance\";s:7:\"visible\";}i:1;a:1:{s:11:\"user_badges\";s:7:\"visible\";}i:2;a:1:{s:8:\"about_me\";s:7:\"visible\";}i:3;a:1:{s:15:\"social_networks\";s:7:\"visible\";}i:4;a:1:{s:7:\"friends\";s:7:\"visible\";}i:5;a:1:{s:6:\"flickr\";s:7:\"visible\";}i:6;a:1:{s:6:\"groups\";s:7:\"visible\";}i:7;a:1:{s:12:\"recent_posts\";s:7:\"visible\";}i:8;a:1:{s:9:\"user_tags\";s:7:\"visible\";}i:9;a:1:{s:5:\"email\";s:7:\"visible\";}i:10;a:1:{s:7:\"address\";s:7:\"visible\";}i:11;a:1:{s:7:\"website\";s:7:\"visible\";}i:12;a:1:{s:5:\"phone\";s:7:\"visible\";}}', 'yes'),
(267, 'install_youzer_2.0.7_options', '1', 'yes'),
(268, 'logy_pages', 'a:3:{s:5:\"login\";i:30;s:13:\"lost-password\";i:32;s:21:\"complete-registration\";i:34;}', 'yes'),
(269, 'logy_is_installed', '1', 'yes'),
(270, 'widget_yz_author_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(271, 'widget_yz_group_rss', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(272, 'widget_yz_my_account_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(273, 'widget_yz_notifications_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(274, 'widget_yz_post_author_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(275, 'widget_yz_activity_rss', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(276, 'widget_yz_smart_author_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(277, 'widget_yz_group_administrators_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(278, 'widget_yz_group_moderators_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(279, 'widget_yz_group_description_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(280, 'widget_yz_group_suggestions_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(281, 'widget_yz_friend_suggestions_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(282, 'widget_logy_login_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(283, 'widget_logy_register_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(284, 'widget_logy_reset_password_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(286, 'yz_author_photo_effect', 'on', 'yes'),
(287, 'yz_display_author_posts', 'on', 'yes'),
(288, 'yz_display_author_views', 'on', 'yes'),
(289, 'yz_display_author_comments', 'on', 'yes'),
(290, 'yz_display_author_networks', 'on', 'yes'),
(291, 'yz_enable_author_pattern', 'on', 'yes'),
(292, 'yz_enable_author_overlay', 'on', 'yes'),
(293, 'yz_enable_author_photo_border', 'on', 'yes'),
(294, 'yz_author_photo_border_style', 'circle', 'yes'),
(295, 'yz_author_sn_bg_type', 'silver', 'yes'),
(296, 'yz_author_sn_bg_style', 'radius', 'yes'),
(297, 'yz_author_meta_type', 'full_location', 'yes'),
(298, 'yz_author_meta_icon', 'map-marker', 'yes'),
(299, 'yz_author_layout', 'yzb-author-v1', 'yes'),
(300, 'yz_author_use_statistics_bg', 'on', 'yes'),
(301, 'yz_display_widget_networks', 'on', 'yes'),
(302, 'yz_author_use_statistics_borders', 'on', 'yes'),
(303, 'yz_profile_photo_effect', 'on', 'yes'),
(304, 'yz_display_header_site', 'on', 'yes'),
(305, 'yz_display_header_posts', 'on', 'yes'),
(306, 'yz_display_header_views', 'on', 'yes'),
(307, 'yz_display_header_comments', 'on', 'yes'),
(308, 'yz_display_header_networks', 'on', 'yes'),
(309, 'yz_display_header_location', 'on', 'yes'),
(310, 'yz_enable_header_pattern', 'on', 'yes'),
(311, 'yz_enable_header_overlay', 'on', 'yes'),
(312, 'yz_header_enable_user_status', 'on', 'yes'),
(313, 'yz_enable_header_photo_border', 'on', 'yes'),
(314, 'yz_header_use_photo_as_cover', 'off', 'yes'),
(315, 'yz_header_photo_border_style', 'circle', 'yes'),
(316, 'yz_header_sn_bg_type', 'silver', 'yes'),
(317, 'yz_header_sn_bg_style', 'circle', 'yes'),
(318, 'yz_header_layout', 'hdr-v1', 'yes'),
(319, 'yz_header_meta_type', 'full_location', 'yes'),
(320, 'yz_header_meta_icon', 'map-marker', 'yes'),
(321, 'yz_header_use_statistics_bg', 'on', 'yes'),
(322, 'yz_header_use_statistics_borders', 'off', 'yes'),
(323, 'yz_group_photo_effect', 'on', 'yes'),
(324, 'yz_display_group_header_privacy', 'on', 'yes'),
(325, 'yz_display_group_header_posts', 'on', 'yes'),
(326, 'yz_display_group_header_members', 'on', 'yes'),
(327, 'yz_display_group_header_networks', 'on', 'yes'),
(328, 'yz_display_group_header_activity', 'on', 'yes'),
(329, 'yz_enable_group_header_pattern', 'on', 'yes'),
(330, 'yz_enable_group_header_overlay', 'on', 'yes'),
(331, 'yz_enable_group_header_avatar_border', 'on', 'yes'),
(332, 'yz_group_header_use_avatar_as_cover', 'on', 'yes'),
(334, 'yz_group_header_sn_bg_type', 'silver', 'yes'),
(335, 'yz_group_header_sn_bg_style', 'circle', 'yes'),
(336, 'yz_group_header_layout', 'hdr-v1', 'yes'),
(337, 'yz_group_header_avatar_border_style', 'circle', 'yes'),
(338, 'yz_group_header_use_statistics_bg', 'on', 'yes'),
(339, 'yz_group_header_use_statistics_borders', 'off', 'yes'),
(340, 'yz_disable_wp_menu_avatar_icon', 'on', 'yes'),
(341, 'yz_display_navbar_icons', 'on', 'yes'),
(342, 'yz_profile_navbar_menus_limit', '5', 'yes'),
(343, 'yz_navbar_icons_style', 'navbar-inline-icons', 'yes'),
(344, 'yz_vertical_layout_navbar_type', 'wild-navbar', 'yes'),
(345, 'yz_profile_posts_per_page', '5', 'yes'),
(346, 'yz_display_post_meta', 'on', 'yes'),
(347, 'yz_display_post_excerpt', 'on', 'yes'),
(348, 'yz_display_post_date', 'on', 'yes'),
(349, 'yz_display_post_cats', 'on', 'yes'),
(350, 'yz_display_post_comments', 'on', 'yes'),
(351, 'yz_display_post_readmore', 'on', 'yes'),
(352, 'yz_display_posts_tab', 'on', 'yes'),
(353, 'yz_display_post_meta_icons', 'on', 'yes'),
(354, 'yz_posts_tab_icon', 'pencil', 'yes'),
(355, 'yz_posts_tab_title', 'Posts', 'yes'),
(357, 'yz_display_overview_tab', 'on', 'yes'),
(358, 'yz_overview_tab_icon', 'globe', 'yes'),
(359, 'yz_bookmarks_tab_icon', 'bookmark-o', 'yes'),
(360, 'yz_overview_tab_title', 'Overview', 'yes'),
(362, 'yz_display_wall_tab', 'on', 'yes'),
(363, 'yz_wall_tab_icon', 'address-card-o', 'yes'),
(364, 'yz_wall_tab_title', 'Wall', 'yes'),
(365, 'yz_display_infos_tab', 'on', 'yes'),
(366, 'yz_info_tab_icon', 'info', 'yes'),
(367, 'yz_info_tab_title', 'Info', 'yes'),
(369, 'yz_profile_comments_nbr', '5', 'yes'),
(370, 'yz_display_comment_date', 'on', 'yes'),
(371, 'yz_display_comments_tab', 'on', 'yes'),
(372, 'yz_display_view_comment', 'on', 'yes'),
(373, 'yz_display_comment_username', 'on', 'yes'),
(374, 'yz_comments_tab_icon', 'comments-o', 'yes'),
(375, 'yz_comments_tab_title', 'Comments', 'yes'),
(377, 'yz_display_wg_title_icon', 'on', 'yes'),
(378, 'yz_use_wg_title_icon_bg', 'on', 'yes'),
(379, 'yz_wgs_border_style', 'radius', 'yes'),
(380, 'yz_wg_sn_display_title', 'on', 'yes'),
(381, 'yz_wg_link_display_title', 'off', 'yes'),
(382, 'yz_wg_quote_display_title', 'off', 'yes'),
(383, 'yz_wg_video_display_title', 'on', 'yes'),
(384, 'yz_wg_rposts_display_title', 'on', 'yes'),
(385, 'yz_wg_skills_display_title', 'on', 'yes'),
(386, 'yz_wg_flickr_display_title', 'on', 'yes'),
(387, 'yz_wg_about_me_display_title', 'on', 'yes'),
(388, 'yz_wg_services_display_title', 'on', 'yes'),
(390, 'yz_wg_portfolio_display_title', 'on', 'yes'),
(391, 'yz_wg_friends_display_title', 'on', 'yes'),
(392, 'yz_wg_groups_display_title', 'on', 'yes'),
(393, 'yz_wg_instagram_display_title', 'on', 'yes'),
(394, 'yz_wg_slideshow_display_title', 'off', 'yes'),
(395, 'yz_wg_user_tags_display_title', 'off', 'yes'),
(396, 'yz_wg_user_badges_display_title', 'on', 'yes'),
(397, 'yz_wg_user_balance_display_title', 'off', 'yes'),
(398, 'yz_wg_post_title', 'Post', 'yes'),
(399, 'yz_wg_link_title', 'Link', 'yes'),
(400, 'yz_wg_video_title', 'Video', 'yes'),
(401, 'yz_wg_quote_title', 'Quote', 'yes'),
(402, 'yz_wg_skills_title', 'Skills', 'yes'),
(403, 'yz_wg_flickr_title', 'Flickr', 'yes'),
(404, 'yz_wg_friends_title', 'Friends', 'yes'),
(405, 'yz_wg_groups_title', 'Groups', 'yes'),
(406, 'yz_wg_project_title', 'Project', 'yes'),
(407, 'yz_wg_aboutme_title', 'About me', 'yes'),
(408, 'yz_wg_services_title', 'Services', 'yes'),
(409, 'yz_wg_portfolio_title', 'Portfolio', 'yes'),
(410, 'yz_wg_instagram_title', 'Instagram', 'yes'),
(411, 'yz_wg_user_tags_title', 'User Tags', 'yes'),
(412, 'yz_wg_slideshow_title', 'Slideshow', 'yes'),
(413, 'yz_wg_rposts_title', 'Recent posts', 'yes'),
(414, 'yz_wg_sn_title', 'Keep in touch', 'yes'),
(415, 'yz_wg_user_badges_title', 'user badges', 'yes'),
(416, 'yz_wg_user_balance_title', 'user balance', 'yes'),
(417, 'yz_wg_sn_bg_style', 'radius', 'yes'),
(418, 'yz_wg_sn_bg_type', 'colorful', 'yes'),
(419, 'yz_wg_sn_icons_size', 'full-width', 'yes'),
(420, 'yz_wg_max_user_badges_items', '12', 'yes'),
(421, 'yz_wg_max_skills', '5', 'yes'),
(422, 'yz_wg_aboutme_img_format', 'circle', 'yes'),
(423, 'yz_display_prjct_meta', 'on', 'yes'),
(424, 'yz_display_prjct_tags', 'on', 'yes'),
(425, 'yz_display_prjct_meta_icons', 'on', 'yes'),
(427, 'yz_wg_project_types', 'a:2:{i:0;s:16:\"featured project\";i:1;s:14:\"recent project\";}', 'yes'),
(428, 'yz_display_wg_post_meta', 'on', 'yes'),
(429, 'yz_display_wg_post_readmore', 'on', 'yes'),
(430, 'yz_display_wg_post_tags', 'on', 'yes'),
(431, 'yz_display_wg_post_excerpt', 'on', 'yes'),
(432, 'yz_display_wg_post_date', 'on', 'yes'),
(433, 'yz_display_wg_post_comments', 'on', 'yes'),
(434, 'yz_display_wg_post_meta_icons', 'on', 'yes'),
(435, 'yz_wg_post_types', 'a:2:{i:0;s:13:\"featured post\";i:1;s:11:\"recent post\";}', 'yes'),
(436, 'yz_login_page_type', 'url', 'yes'),
(437, 'yz_enable_ajax_login', 'off', 'yes'),
(438, 'yz_enable_login_popup', 'off', 'yes'),
(439, 'yz_login_page_url', 'http://localhost/customp/wp-login.php', 'yes'),
(440, 'yz_wg_max_services', '4', 'yes'),
(441, 'yz_display_service_icon', 'on', 'yes'),
(442, 'yz_display_service_text', 'on', 'yes'),
(443, 'yz_display_service_title', 'on', 'yes'),
(445, 'yz_wg_service_icon_bg_format', 'circle', 'yes'),
(446, 'yz_wg_services_layout', 'vertical-services-layout', 'yes'),
(447, 'yz_wg_max_slideshow_items', '3', 'yes'),
(448, 'yz_slideshow_height_type', 'fixed', 'yes'),
(449, 'yz_display_slideshow_title', 'off', 'yes'),
(450, 'yz_wg_max_portfolio_items', '9', 'yes'),
(451, 'yz_display_portfolio_url', 'on', 'yes'),
(452, 'yz_display_portfolio_zoom', 'on', 'yes'),
(453, 'yz_display_portfolio_title', 'on', 'yes'),
(454, 'yz_wg_max_flickr_items', '6', 'yes'),
(455, 'yz_wg_max_friends_items', '5', 'yes'),
(456, 'yz_wg_friends_layout', 'list', 'yes'),
(457, 'yz_wg_friends_avatar_img_format', 'circle', 'yes'),
(458, 'yz_wg_max_groups_items', '3', 'yes'),
(459, 'yz_wg_groups_avatar_img_format', 'circle', 'yes'),
(460, 'yz_wg_max_instagram_items', '9', 'yes'),
(461, 'yz_display_instagram_url', 'on', 'yes'),
(462, 'yz_display_instagram_zoom', 'on', 'yes'),
(463, 'yz_display_instagram_title', 'on', 'yes'),
(464, 'yz_wg_max_rposts', '3', 'yes'),
(465, 'yz_wg_rposts_img_format', 'circle', 'yes'),
(466, 'yz_use_effects', 'off', 'yes'),
(467, 'yz_profile_login_button', 'on', 'yes'),
(468, 'yz_profile_main_widgets', 'a:10:{i:0;a:1:{s:9:\"slideshow\";s:7:\"visible\";}i:1;a:1:{s:7:\"project\";s:7:\"visible\";}i:2;a:1:{s:6:\"skills\";s:7:\"visible\";}i:3;a:1:{s:9:\"portfolio\";s:7:\"visible\";}i:4;a:1:{s:5:\"quote\";s:7:\"visible\";}i:5;a:1:{s:9:\"instagram\";s:7:\"visible\";}i:6;a:1:{s:8:\"services\";s:7:\"visible\";}i:7;a:1:{s:4:\"post\";s:7:\"visible\";}i:8;a:1:{s:4:\"link\";s:7:\"visible\";}i:9;a:1:{s:5:\"video\";s:7:\"visible\";}}', 'yes'),
(469, 'yz_profile_404_button', 'Go Back Home', 'yes'),
(470, 'yz_profile_404_desc', 'we\'re sorry the profile you\'re looking for cannot be found.', 'yes'),
(471, 'yz_profile_scheme', 'yz-blue-scheme', 'yes'),
(472, 'yz_enable_profile_custom_scheme', 'on', 'yes'),
(473, 'yz_enable_panel_fixed_save_btn', 'on', 'yes'),
(474, 'yz_panel_scheme', 'uk-yellow-scheme', 'yes'),
(475, 'yz_tabs_list_icons_style', 'yz-tabs-list-gradient', 'yes'),
(476, 'yz_display_profile_tabs_count', 'on', 'yes'),
(477, 'yz_msgbox_mailchimp', 'on', 'yes'),
(478, 'yz_msgbox_captcha', 'on', 'yes'),
(479, 'yz_msgbox_logy_login', 'on', 'yes'),
(480, 'yz_msgbox_mail_tags', 'off', 'yes'),
(481, 'yz_msgbox_mail_content', 'on', 'yes'),
(482, 'yz_msgbox_ads_placement', 'on', 'yes'),
(484, 'yz_msgbox_profile_schemes', 'on', 'yes'),
(485, 'yz_msgbox_profile_structure', 'on', 'yes'),
(487, 'yz_msgbox_instagram_wg_app_setup_steps', 'on', 'yes'),
(488, 'yz_msgbox_custom_widgets_placement', 'on', 'yes'),
(489, 'yz_msgbox_user_badges_widget_notice', 'on', 'yes'),
(490, 'yz_msgbox_user_balance_widget_notice', 'on', 'yes'),
(491, 'yz_enable_account_scroll_button', 'on', 'yes'),
(492, 'yz_files_max_size', '3', 'yes'),
(493, 'yz_enable_youzer_activity_filter', 'on', 'yes'),
(494, 'yz_enable_wall_url_preview', 'on', 'yes'),
(495, 'yz_enable_wall_posts_reply', 'on', 'yes'),
(496, 'yz_enable_wall_posts_likes', 'on', 'yes'),
(498, 'yz_enable_wall_posts_comments', 'on', 'yes'),
(499, 'yz_enable_wall_posts_deletion', 'on', 'yes'),
(500, 'yz_enable_activity_directory_filter_bar', 'on', 'yes'),
(501, 'yz_attachments_max_size', '10', 'yes'),
(502, 'yz_attachments_max_nbr', '200', 'yes'),
(503, 'yz_atts_allowed_images_exts', 'a:4:{i:0;s:3:\"png\";i:1;s:3:\"jpg\";i:2;s:4:\"jpeg\";i:3;s:3:\"gif\";}', 'yes'),
(504, 'yz_atts_allowed_videos_exts', 'a:4:{i:0;s:3:\"mp4\";i:1;s:3:\"ogg\";i:2;s:3:\"ogv\";i:3;s:4:\"webm\";}', 'yes'),
(505, 'yz_atts_allowed_audios_exts', 'a:3:{i:0;s:3:\"mp3\";i:1;s:3:\"ogg\";i:2;s:3:\"wav\";}', 'yes'),
(506, 'yz_atts_allowed_files_exts', 'a:13:{i:0;s:3:\"png\";i:1;s:3:\"jpg\";i:2;s:4:\"jpeg\";i:3;s:3:\"gif\";i:4;s:3:\"doc\";i:5;s:4:\"docx\";i:6;s:3:\"pdf\";i:7;s:3:\"rar\";i:8;s:3:\"zip\";i:9;s:3:\"mp4\";i:10;s:3:\"mp3\";i:11;s:3:\"ogg\";i:12;s:3:\"pfi\";}', 'yes'),
(507, 'yz_enable_bookmarks', 'on', 'yes'),
(509, 'yz_enable_bookmarks_privacy', 'private', 'yes'),
(510, 'yz_enable_sticky_posts', 'on', 'yes'),
(511, 'yz_enable_groups_sticky_posts', 'on', 'yes'),
(512, 'yz_enable_activity_sticky_posts', 'on', 'yes'),
(513, 'yz_display_scrolltotop', 'on', 'yes'),
(514, 'yz_display_group_scrolltotop', 'off', 'yes'),
(515, 'yz_enable_settings_copyright', 'on', 'yes'),
(516, 'yz_activity_wall_posts_per_page', '20', 'yes'),
(517, 'yz_profile_wall_posts_per_page', '20', 'yes'),
(518, 'yz_groups_wall_posts_per_page', '20', 'yes'),
(519, 'yz_enable_wall_file', 'on', 'yes'),
(520, 'yz_enable_wall_link', 'on', 'yes'),
(521, 'yz_enable_wall_photo', 'on', 'yes'),
(522, 'yz_enable_wall_audio', 'on', 'yes'),
(523, 'yz_enable_wall_video', 'on', 'yes'),
(524, 'yz_enable_wall_quote', 'on', 'yes'),
(525, 'yz_enable_wall_status', 'on', 'yes'),
(527, 'yz_enable_wall_comments', 'off', 'yes'),
(528, 'yz_enable_wall_new_cover', 'on', 'yes'),
(529, 'yz_enable_wall_new_member', 'on', 'yes'),
(530, 'yz_enable_wall_slideshow', 'on', 'yes'),
(531, 'yz_enable_wall_filter_bar', 'on', 'yes'),
(532, 'yz_enable_wall_new_avatar', 'on', 'yes'),
(533, 'yz_enable_wall_joined_group', 'on', 'yes'),
(534, 'yz_enable_wall_posts_embeds', 'on', 'yes'),
(536, 'yz_enable_wall_new_blog_post', 'on', 'yes'),
(537, 'yz_enable_wall_created_group', 'on', 'yes'),
(538, 'yz_enable_wall_comments_embeds', 'on', 'yes'),
(540, 'yz_enable_wall_updated_profile', 'off', 'yes'),
(541, 'yz_enable_wall_new_blog_comment', 'off', 'yes'),
(542, 'yz_enable_wall_friendship_created', 'on', 'yes'),
(543, 'yz_enable_wall_friendship_accepted', 'on', 'yes'),
(544, 'yz_allow_private_profiles', 'off', 'yes'),
(545, 'yz_disable_bp_registration', 'off', 'yes'),
(546, 'yz_md_users_per_page', '18', 'yes'),
(547, 'yz_md_card_meta_icon', 'at', 'yes'),
(548, 'yz_enable_md_cards_cover', 'on', 'yes'),
(549, 'yz_enable_md_cards_status', 'on', 'yes'),
(550, 'yz_show_md_cards_online_only', 'on', 'yes'),
(551, 'yz_enable_md_users_statistics', 'on', 'yes'),
(552, 'yz_md_card_meta_field', 'user_login', 'yes'),
(553, 'yz_enable_md_custom_card_meta', 'off', 'yes'),
(554, 'yz_enable_md_cards_avatar_border', 'off', 'yes'),
(555, 'yz_enable_md_user_views_statistics', 'on', 'yes'),
(556, 'yz_enable_md_cards_actions_buttons', 'on', 'yes'),
(557, 'yz_enable_md_user_posts_statistics', 'on', 'yes'),
(558, 'yz_enable_md_user_friends_statistics', 'on', 'yes'),
(559, 'yz_enable_md_user_comments_statistics', 'on', 'yes'),
(560, 'yz_gd_groups_per_page', '18', 'yes'),
(561, 'yz_enable_gd_cards_cover', 'on', 'yes'),
(562, 'yz_show_gd_cards_online_only', 'on', 'yes'),
(563, 'yz_enable_gd_groups_statistics', 'on', 'yes'),
(564, 'yz_enable_gd_cards_avatar_border', 'on', 'yes'),
(565, 'yz_enable_gd_cards_actions_buttons', 'on', 'yes'),
(566, 'yz_enable_gd_group_posts_statistics', 'on', 'yes'),
(567, 'yz_enable_gd_group_members_statistics', 'on', 'yes'),
(568, 'yz_enable_gd_group_activity_statistics', 'on', 'yes'),
(569, 'yz_gd_cards_buttons_border_style', 'oval', 'yes'),
(570, 'yz_gd_cards_avatar_border_style', 'circle', 'yes'),
(571, 'yz_gd_cards_buttons_layout', 'block', 'yes'),
(572, 'yz_md_cards_buttons_layout', 'block', 'yes'),
(573, 'yz_md_cards_buttons_border_style', 'oval', 'yes'),
(574, 'yz_md_cards_avatar_border_style', 'circle', 'yes'),
(575, 'yz_enable_global_custom_styling', 'on', 'yes'),
(576, 'yz_enable_profile_custom_styling', 'on', 'yes'),
(577, 'yz_enable_account_custom_styling', 'on', 'yes'),
(578, 'yz_enable_activity_custom_styling', 'off', 'yes'),
(579, 'yz_enable_groups_custom_styling', 'on', 'yes'),
(580, 'yz_enable_groups_directory_custom_styling', 'off', 'yes'),
(581, 'yz_enable_members_directory_custom_styling', 'on', 'yes'),
(582, 'yz_enable_posts_emoji', 'on', 'yes'),
(583, 'yz_enable_comments_emoji', 'on', 'yes'),
(584, 'yz_enable_messages_emoji', 'on', 'yes'),
(585, 'yz_buttons_border_style', 'oval', 'yes'),
(586, 'yz_activate_membership_system', 'on', 'yes'),
(588, 'yz_enable_account_verification', 'on', 'yes'),
(589, 'logy_login_form_enable_header', 'on', 'yes'),
(590, 'logy_user_after_login_redirect', 'home', 'yes'),
(591, 'logy_after_logout_redirect', 'login', 'yes'),
(592, 'logy_admin_after_login_redirect', 'dashboard', 'yes'),
(593, 'logy_login_form_layout', 'logy-field-v1', 'yes'),
(594, 'logy_login_icons_position', 'logy-icons-left', 'yes'),
(596, 'logy_login_actions_layout', 'logy-actions-v1', 'yes'),
(597, 'logy_login_btn_icons_position', 'logy-icons-left', 'yes'),
(598, 'logy_login_btn_format', 'logy-border-radius', 'yes'),
(599, 'logy_login_fields_format', 'logy-border-flat', 'yes'),
(600, 'logy_login_form_title', 'Login', 'yes'),
(601, 'logy_login_signin_btn_title', 'Log In', 'yes'),
(602, 'logy_login_register_btn_title', 'Create New Account', 'yes'),
(603, 'logy_login_lostpswd_title', 'Lost password?', 'yes'),
(604, 'logy_login_form_subtitle', 'Sign in to your account', 'yes'),
(605, 'logy_social_btns_icons_position', 'logy-icons-left', 'yes'),
(606, 'logy_social_btns_format', 'logy-border-radius', 'yes'),
(607, 'logy_social_btns_type', 'logy-only-icon', 'yes'),
(608, 'logy_enable_social_login', 'on', 'yes'),
(609, 'logy_use_auth_modal', 'on', 'yes'),
(610, 'logy_lostpswd_form_enable_header', 'on', 'yes'),
(611, 'logy_lostpswd_form_title', 'Forgot your password?', 'yes'),
(612, 'logy_lostpswd_submit_btn_title', 'Reset Password', 'yes'),
(613, 'logy_lostpswd_form_subtitle', 'Reset your account password', 'yes'),
(614, 'logy_show_terms_privacy_note', 'on', 'yes'),
(616, 'logy_signup_form_enable_header', 'on', 'yes'),
(617, 'logy_signup_form_layout', 'logy-field-v1', 'yes'),
(618, 'logy_signup_icons_position', 'logy-icons-left', 'yes'),
(619, 'logy_signup_actions_layout', 'logy-regactions-v1', 'yes'),
(620, 'logy_signup_btn_icons_position', 'logy-icons-left', 'yes'),
(621, 'logy_signup_btn_format', 'logy-border-radius', 'yes'),
(622, 'logy_signup_fields_format', 'logy-border-flat', 'yes'),
(623, 'logy_signup_signin_btn_title', 'Log In', 'yes'),
(624, 'logy_signup_form_title', 'Sign Up', 'yes'),
(625, 'logy_signup_register_btn_title', 'Sign Up', 'yes'),
(626, 'logy_signup_form_subtitle', 'Create New Account', 'yes'),
(627, 'logy_long_lockout_duration', '86400', 'yes'),
(628, 'logy_short_lockout_duration', '43200', 'yes'),
(629, 'logy_retries_duration', '1200', 'yes'),
(630, 'logy_enable_limit_login', 'on', 'yes'),
(631, 'logy_allowed_retries', '4', 'yes'),
(632, 'logy_allowed_lockouts', '2', 'yes'),
(633, 'yz_enable_user_tags', 'on', 'yes'),
(634, 'yz_enable_user_tags_icon', 'on', 'yes'),
(635, 'yz_enable_user_tags_description', 'on', 'yes'),
(636, 'yz_wg_user_tags_border_style', 'radius', 'yes'),
(637, 'yz_enable_mailster', 'off', 'yes'),
(638, 'yz_enable_mailchimp', 'off', 'yes'),
(639, 'logy_notify_admin_on_registration', 'on', 'yes'),
(640, 'logy_hide_subscribers_dash', 'off', 'yes'),
(641, 'logy_enable_recaptcha', 'on', 'yes'),
(642, 'logy_msgbox_captcha', 'on', 'yes'),
(643, 'yz_activate_options_autoload', '1', 'yes'),
(645, 'set_active_styles', 'a:1:{i:0;s:20:\"yz_plugin_background\";}', 'yes'),
(646, 'notifier-cache', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<notifier>\n	<latest>2.1.5</latest>\n	<changelog>\n<![CDATA[\n<h4># Version 2.1.5 August 31, 2018</h4>\n<pre>\n\nHello Guys !\n\nI created a facebook group where you can guys interact with each others, ask questions, request new features, share tips and tricks and build youzer together :\n\nhttps://www.facebook.com/groups/235003947151622/\n\n______________________________________________\n\n# Version 2.1.5 August 31, 2018\n    New : RTL Support.\n    Fix : Profile Subn navigation icons not working !\n    Fix : Open Privacy and Terms links in a new tabs.\n    Fix : Make Slideshow Image appear on one image only also.\n    Fix : Adding rel=\"nofollow noopener\" to links.\n    Fix : Making Captcha Language Customizable.\n    Fix : Fixing offline status in widgets.\n    Fix : Activity slideshow not working.\n    Fix : Bookmarks function not working !\n    Fix : Settings Current Page xprofile fields color not working.\n    New : Styling the BP Followers Widget.\n\n</pre>\n]]>\n	</changelog>\n</notifier>', 'yes'),
(647, 'notifier-cache-last-updated', '1536738077', 'yes'),
(650, 'theme_mods_twentyfifteen', 'a:1:{i:0;b:0;}', 'yes'),
(653, 'wpseo_sitemap_nav_menu_item_cache_validator', 'KX4W', 'no'),
(654, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(662, 'WPLANG', '', 'yes'),
(663, 'new_admin_email', 'aridyansyah.kii@gmail.com', 'yes'),
(667, 'illegal_names', 'a:20:{i:0;s:3:\"www\";i:1;s:3:\"web\";i:2;s:4:\"root\";i:3;s:5:\"admin\";i:4;s:4:\"main\";i:5;s:6:\"invite\";i:6;s:13:\"administrator\";i:7;s:6:\"groups\";i:8;s:7:\"members\";i:9;s:6:\"forums\";i:10;s:5:\"blogs\";i:11;s:8:\"activity\";i:12;s:7:\"profile\";i:13;s:7:\"friends\";i:14;s:6:\"search\";i:15;s:8:\"settings\";i:16;s:13:\"notifications\";i:17;s:8:\"register\";i:18;s:8:\"activate\";i:19;s:4:\"wall\";}', 'no'),
(668, 'logy_login_retries', 'a:0:{}', 'yes'),
(676, 'wpseo_sitemap_37_cache_validator', '23Zt1', 'no'),
(677, 'wpseo_sitemap_39_cache_validator', '245uH', 'no'),
(682, 'yz_compress_user_registered1', '1', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(683, 'yz_compress_user_login1', '1', 'yes'),
(684, 'yz_compress_first_name1', '1', 'yes'),
(685, 'yz_compress_last_name1', '1', 'yes'),
(686, 'yz_compress_user_country1', '1', 'yes'),
(687, 'yz_compress_user_city1', '1', 'yes'),
(688, 'yz_compress_description1', '1', 'yes'),
(691, 'yz_compress_wg_instagram_account_id1', '1', 'yes'),
(692, 'yz_compress_wg_flickr_account_id1', '1', 'yes'),
(693, 'yz_compress_youzer_portfolio1', '1', 'yes'),
(694, 'yz_compress_youzer_slideshow1', '1', 'yes'),
(695, 'yz_compress_user_url1', '1', 'yes'),
(696, 'yz_compress_wg_about_me_bio1', '1', 'yes'),
(697, 'yz_compress_yz_sn_11', '1', 'yes'),
(698, 'yz_compress_yz_sn_21', '1', 'yes'),
(699, 'yz_compress_yz_sn_31', '1', 'yes'),
(700, 'yz_compress_yz_sn_41', '1', 'yes'),
(701, 'yz_compress_email_address1', '1', 'yes'),
(702, 'yz_compress_user_address1', '1', 'yes'),
(703, 'yz_compress_phone_nbr1', '1', 'yes'),
(705, 'yz_compress_wg_project_desc1', '1', 'yes'),
(706, 'yz_compress_youzer_skills1', '1', 'yes'),
(707, 'yz_compress_wg_quote_txt1', '1', 'yes'),
(708, 'yz_compress_youzer_services1', '1', 'yes'),
(709, 'yz_compress_yz_profile_wg_post_id1', '1', 'yes'),
(710, 'yz_compress_wg_link_url1', '1', 'yes'),
(711, 'yz_compress_wg_video_url1', '1', 'yes'),
(727, 'wpseo_sitemap_topic_cache_validator', 'ajST', 'no'),
(730, 'wpseo_sitemap_reply_cache_validator', '2F2UW', 'no'),
(735, '_transient_bp_active_member_count', '3', 'yes'),
(746, 'yz_compress_user_registered3', '1', 'yes'),
(747, 'yz_compress_user_login3', '1', 'yes'),
(748, 'yz_compress_first_name3', '1', 'yes'),
(749, 'yz_compress_last_name3', '1', 'yes'),
(750, 'yz_compress_user_country3', '1', 'yes'),
(751, 'yz_compress_user_city3', '1', 'yes'),
(752, 'yz_compress_description3', '1', 'yes'),
(758, 'yz_plugin_background', 'a:1:{s:5:\"color\";s:0:\"\";}', 'yes'),
(759, 'yz_plugin_content_width', '', 'yes'),
(760, 'yz_login_page', '0', 'yes'),
(761, 'yz_plugin_margin_top', '', 'yes'),
(762, 'yz_plugin_margin_bottom', '', 'yes'),
(763, 'yz_plugin_padding_top', '', 'yes'),
(764, 'yz_plugin_padding_bottom', '', 'yes'),
(765, 'yz_plugin_padding_left', '', 'yes'),
(766, 'yz_plugin_padding_right', '', 'yes'),
(767, 'yz_scroll_button_color', 'a:1:{s:5:\"color\";s:0:\"\";}', 'yes'),
(768, 'yz_active_styles', 'a:1:{i:0;s:20:\"yz_plugin_background\";}', 'yes'),
(771, 'yz_global_custom_styling', '', 'yes'),
(772, 'yz_profile_custom_styling', '', 'yes'),
(773, 'yz_account_custom_styling', '', 'yes'),
(774, 'yz_groups_custom_styling', '', 'yes'),
(775, 'yz_members_directory_custom_styling', '', 'yes'),
(776, 'yz_groups_directory_custom_styling', '', 'yes'),
(777, 'yz_activity_custom_styling', '', 'yes'),
(778, 'yz_profile_custom_scheme_color', 'a:1:{s:5:\"color\";s:7:\"#bee522\";}', 'yes'),
(798, 'wpseo_sitemap_customize_changeset_cache_validator', '7YWS', 'no'),
(814, 'wpseo_sitemap_73_cache_validator', '7SjT', 'no'),
(827, 'wpseo_sitemap_67_cache_validator', 'bSab', 'no'),
(833, 'wpseo_sitemap_40_cache_validator', '31tyr', 'no'),
(834, 'wpseo_sitemap_41_cache_validator', '2eId', 'no'),
(835, 'wpseo_sitemap_46_cache_validator', '31tC5', 'no'),
(844, 'wpseo_sitemap_53_cache_validator', 'wmXT', 'no'),
(854, 'wpseo_sitemap_96_cache_validator', 'U7gp', 'no'),
(856, 'wpseo_sitemap_98_cache_validator', 'Uldf', 'no'),
(860, 'wpseo_sitemap_100_cache_validator', '22uLy', 'no'),
(861, 'wpseo_sitemap_101_cache_validator', '22uOu', 'no'),
(862, 'wpseo_sitemap_102_cache_validator', '22uQj', 'no'),
(863, 'wpseo_sitemap_104_cache_validator', '23aFD', 'no'),
(864, 'wpseo_sitemap_105_cache_validator', '23z4V', 'no'),
(865, 'wpseo_sitemap_106_cache_validator', '23z7k', 'no'),
(866, 'wpseo_sitemap_107_cache_validator', '23TUM', 'no'),
(870, 'wpseo_sitemap_113_cache_validator', '2gAjm', 'no'),
(922, 'wpseo_sitemap_cache_validator_global', '24bA4', 'no'),
(954, '_site_transient_timeout_browser_c725707436ffb8256b2bbf7bc3ade4e9', '1537154439', 'no'),
(955, '_site_transient_browser_c725707436ffb8256b2bbf7bc3ade4e9', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"69.0.3497.81\";s:8:\"platform\";s:5:\"Linux\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(970, 'wpseo_sitemap_44_cache_validator', 'TY2A', 'no'),
(979, 'rewrite_rules', 'a:194:{s:9:\"forums/?$\";s:25:\"index.php?post_type=forum\";s:39:\"forums/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?post_type=forum&feed=$matches[1]\";s:34:\"forums/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?post_type=forum&feed=$matches[1]\";s:26:\"forums/page/([0-9]{1,})/?$\";s:43:\"index.php?post_type=forum&paged=$matches[1]\";s:9:\"topics/?$\";s:25:\"index.php?post_type=topic\";s:39:\"topics/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?post_type=topic&feed=$matches[1]\";s:34:\"topics/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?post_type=topic&feed=$matches[1]\";s:26:\"topics/page/([0-9]{1,})/?$\";s:43:\"index.php?post_type=topic&paged=$matches[1]\";s:28:\"forums/forum/([^/]+)/edit/?$\";s:34:\"index.php?forum=$matches[1]&edit=1\";s:28:\"forums/topic/([^/]+)/edit/?$\";s:34:\"index.php?topic=$matches[1]&edit=1\";s:28:\"forums/reply/([^/]+)/edit/?$\";s:34:\"index.php?reply=$matches[1]&edit=1\";s:32:\"forums/topic-tag/([^/]+)/edit/?$\";s:38:\"index.php?topic-tag=$matches[1]&edit=1\";s:47:\"forums/user/([^/]+)/topics/page/?([0-9]{1,})/?$\";s:59:\"index.php?bbp_user=$matches[1]&bbp_tops=1&paged=$matches[2]\";s:48:\"forums/user/([^/]+)/replies/page/?([0-9]{1,})/?$\";s:59:\"index.php?bbp_user=$matches[1]&bbp_reps=1&paged=$matches[2]\";s:50:\"forums/user/([^/]+)/favorites/page/?([0-9]{1,})/?$\";s:59:\"index.php?bbp_user=$matches[1]&bbp_favs=1&paged=$matches[2]\";s:54:\"forums/user/([^/]+)/subscriptions/page/?([0-9]{1,})/?$\";s:59:\"index.php?bbp_user=$matches[1]&bbp_subs=1&paged=$matches[2]\";s:29:\"forums/user/([^/]+)/topics/?$\";s:41:\"index.php?bbp_user=$matches[1]&bbp_tops=1\";s:30:\"forums/user/([^/]+)/replies/?$\";s:41:\"index.php?bbp_user=$matches[1]&bbp_reps=1\";s:32:\"forums/user/([^/]+)/favorites/?$\";s:41:\"index.php?bbp_user=$matches[1]&bbp_favs=1\";s:36:\"forums/user/([^/]+)/subscriptions/?$\";s:41:\"index.php?bbp_user=$matches[1]&bbp_subs=1\";s:27:\"forums/user/([^/]+)/edit/?$\";s:37:\"index.php?bbp_user=$matches[1]&edit=1\";s:22:\"forums/user/([^/]+)/?$\";s:30:\"index.php?bbp_user=$matches[1]\";s:40:\"forums/view/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?bbp_view=$matches[1]&paged=$matches[2]\";s:27:\"forums/view/([^/]+)/feed/?$\";s:47:\"index.php?bbp_view=$matches[1]&feed=$matches[2]\";s:22:\"forums/view/([^/]+)/?$\";s:30:\"index.php?bbp_view=$matches[1]\";s:34:\"forums/search/page/?([0-9]{1,})/?$\";s:27:\"index.php?paged=$matches[1]\";s:16:\"forums/search/?$\";s:20:\"index.php?bbp_search\";s:19:\"sitemap_index\\.xml$\";s:19:\"index.php?sitemap=1\";s:31:\"([^/]+?)-sitemap([0-9]+)?\\.xml$\";s:51:\"index.php?sitemap=$matches[1]&sitemap_n=$matches[2]\";s:24:\"([a-z]+)?-?sitemap\\.xsl$\";s:25:\"index.php?xsl=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:53:\"theme/category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:48:\"theme/category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:29:\"theme/category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:41:\"theme/category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:23:\"theme/category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:50:\"theme/tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:45:\"theme/tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:26:\"theme/tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:38:\"theme/tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:20:\"theme/tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:51:\"theme/type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:46:\"theme/type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:27:\"theme/type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:39:\"theme/type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:21:\"theme/type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:38:\"forums/forum/.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:48:\"forums/forum/.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:68:\"forums/forum/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:63:\"forums/forum/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:63:\"forums/forum/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:44:\"forums/forum/.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:27:\"forums/forum/(.+?)/embed/?$\";s:38:\"index.php?forum=$matches[1]&embed=true\";s:31:\"forums/forum/(.+?)/trackback/?$\";s:32:\"index.php?forum=$matches[1]&tb=1\";s:51:\"forums/forum/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?forum=$matches[1]&feed=$matches[2]\";s:46:\"forums/forum/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?forum=$matches[1]&feed=$matches[2]\";s:39:\"forums/forum/(.+?)/page/?([0-9]{1,})/?$\";s:45:\"index.php?forum=$matches[1]&paged=$matches[2]\";s:46:\"forums/forum/(.+?)/comment-page-([0-9]{1,})/?$\";s:45:\"index.php?forum=$matches[1]&cpage=$matches[2]\";s:35:\"forums/forum/(.+?)(?:/([0-9]+))?/?$\";s:44:\"index.php?forum=$matches[1]&page=$matches[2]\";s:40:\"forums/topic/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:50:\"forums/topic/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:70:\"forums/topic/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:65:\"forums/topic/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:65:\"forums/topic/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:46:\"forums/topic/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:29:\"forums/topic/([^/]+)/embed/?$\";s:38:\"index.php?topic=$matches[1]&embed=true\";s:33:\"forums/topic/([^/]+)/trackback/?$\";s:32:\"index.php?topic=$matches[1]&tb=1\";s:53:\"forums/topic/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?topic=$matches[1]&feed=$matches[2]\";s:48:\"forums/topic/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?topic=$matches[1]&feed=$matches[2]\";s:41:\"forums/topic/([^/]+)/page/?([0-9]{1,})/?$\";s:45:\"index.php?topic=$matches[1]&paged=$matches[2]\";s:48:\"forums/topic/([^/]+)/comment-page-([0-9]{1,})/?$\";s:45:\"index.php?topic=$matches[1]&cpage=$matches[2]\";s:37:\"forums/topic/([^/]+)(?:/([0-9]+))?/?$\";s:44:\"index.php?topic=$matches[1]&page=$matches[2]\";s:29:\"forums/topic/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:39:\"forums/topic/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:59:\"forums/topic/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:54:\"forums/topic/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:54:\"forums/topic/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:35:\"forums/topic/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:40:\"forums/reply/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:50:\"forums/reply/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:70:\"forums/reply/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:65:\"forums/reply/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:65:\"forums/reply/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:46:\"forums/reply/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:29:\"forums/reply/([^/]+)/embed/?$\";s:38:\"index.php?reply=$matches[1]&embed=true\";s:33:\"forums/reply/([^/]+)/trackback/?$\";s:32:\"index.php?reply=$matches[1]&tb=1\";s:41:\"forums/reply/([^/]+)/page/?([0-9]{1,})/?$\";s:45:\"index.php?reply=$matches[1]&paged=$matches[2]\";s:48:\"forums/reply/([^/]+)/comment-page-([0-9]{1,})/?$\";s:45:\"index.php?reply=$matches[1]&cpage=$matches[2]\";s:37:\"forums/reply/([^/]+)(?:/([0-9]+))?/?$\";s:44:\"index.php?reply=$matches[1]&page=$matches[2]\";s:29:\"forums/reply/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:39:\"forums/reply/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:59:\"forums/reply/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:54:\"forums/reply/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:54:\"forums/reply/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:35:\"forums/reply/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:57:\"forums/topic-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?topic-tag=$matches[1]&feed=$matches[2]\";s:52:\"forums/topic-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?topic-tag=$matches[1]&feed=$matches[2]\";s:33:\"forums/topic-tag/([^/]+)/embed/?$\";s:42:\"index.php?topic-tag=$matches[1]&embed=true\";s:45:\"forums/topic-tag/([^/]+)/page/?([0-9]{1,})/?$\";s:49:\"index.php?topic-tag=$matches[1]&paged=$matches[2]\";s:27:\"forums/topic-tag/([^/]+)/?$\";s:31:\"index.php?topic-tag=$matches[1]\";s:42:\"forums/search/([^/]+)/page/?([0-9]{1,})/?$\";s:50:\"index.php?bbp_search=$matches[1]&paged=$matches[2]\";s:24:\"forums/search/([^/]+)/?$\";s:32:\"index.php?bbp_search=$matches[1]\";s:61:\"theme/bp_member_type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?bp_member_type=$matches[1]&feed=$matches[2]\";s:56:\"theme/bp_member_type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?bp_member_type=$matches[1]&feed=$matches[2]\";s:37:\"theme/bp_member_type/([^/]+)/embed/?$\";s:47:\"index.php?bp_member_type=$matches[1]&embed=true\";s:49:\"theme/bp_member_type/([^/]+)/page/?([0-9]{1,})/?$\";s:54:\"index.php?bp_member_type=$matches[1]&paged=$matches[2]\";s:31:\"theme/bp_member_type/([^/]+)/?$\";s:36:\"index.php?bp_member_type=$matches[1]\";s:37:\"theme/cfs/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:47:\"theme/cfs/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:67:\"theme/cfs/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"theme/cfs/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"theme/cfs/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:43:\"theme/cfs/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:26:\"theme/cfs/([^/]+)/embed/?$\";s:51:\"index.php?post_type=cfs&name=$matches[1]&embed=true\";s:30:\"theme/cfs/([^/]+)/trackback/?$\";s:45:\"index.php?post_type=cfs&name=$matches[1]&tb=1\";s:38:\"theme/cfs/([^/]+)/page/?([0-9]{1,})/?$\";s:58:\"index.php?post_type=cfs&name=$matches[1]&paged=$matches[2]\";s:45:\"theme/cfs/([^/]+)/comment-page-([0-9]{1,})/?$\";s:58:\"index.php?post_type=cfs&name=$matches[1]&cpage=$matches[2]\";s:34:\"theme/cfs/([^/]+)(?:/([0-9]+))?/?$\";s:57:\"index.php?post_type=cfs&name=$matches[1]&page=$matches[2]\";s:26:\"theme/cfs/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:36:\"theme/cfs/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:56:\"theme/cfs/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"theme/cfs/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"theme/cfs/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:32:\"theme/cfs/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:53:\"theme/author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:48:\"theme/author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:29:\"theme/author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:41:\"theme/author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:23:\"theme/author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:75:\"theme/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:70:\"theme/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:51:\"theme/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:63:\"theme/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:45:\"theme/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:62:\"theme/([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:57:\"theme/([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:38:\"theme/([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:50:\"theme/([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:32:\"theme/([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:49:\"theme/([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:44:\"theme/([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:25:\"theme/([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:37:\"theme/([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:19:\"theme/([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:33:\"theme/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:43:\"theme/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:63:\"theme/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"theme/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"theme/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:39:\"theme/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:22:\"theme/([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:26:\"theme/([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:46:\"theme/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:41:\"theme/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:34:\"theme/([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:41:\"theme/([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:30:\"theme/([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:22:\"theme/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:32:\"theme/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:52:\"theme/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:47:\"theme/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:47:\"theme/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:28:\"theme/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(997, '_transient_twentyseventeen_categories', '2', 'yes'),
(1030, '_site_transient_timeout_theme_roots', '1536721752', 'no'),
(1031, '_site_transient_theme_roots', 'a:4:{s:12:\"suport-theme\";s:7:\"/themes\";s:13:\"twentyfifteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}', 'no'),
(1032, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1536719955;s:7:\"checked\";a:11:{s:57:\"acf-content-analysis-for-yoast-seo/yoast-acf-analysis.php\";s:5:\"2.1.0\";s:30:\"advanced-custom-fields/acf.php\";s:6:\"4.4.12\";s:19:\"bbpress/bbpress.php\";s:6:\"2.5.14\";s:22:\"yuuki/yuuki-plugin.php\";s:3:\"1.0\";s:24:\"buddypress/bp-loader.php\";s:5:\"3.1.0\";s:26:\"custom-field-suite/cfs.php\";s:6:\"2.5.12\";s:43:\"custom-post-type-ui/custom-post-type-ui.php\";s:5:\"1.5.8\";s:23:\"elementor/elementor.php\";s:5:\"2.2.1\";s:31:\"what-the-file/what-the-file.php\";s:5:\"1.5.4\";s:24:\"wordpress-seo/wp-seo.php\";s:5:\"8.1.2\";s:17:\"youzer/youzer.php\";s:5:\"2.1.3\";}s:8:\"response\";a:1:{s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:3:\"8.2\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/wordpress-seo.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=1834347\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1859687\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1859687\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}s:6:\"tested\";s:5:\"4.9.8\";s:12:\"requires_php\";s:5:\"5.2.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:8:{s:57:\"acf-content-analysis-for-yoast-seo/yoast-acf-analysis.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:48:\"w.org/plugins/acf-content-analysis-for-yoast-seo\";s:4:\"slug\";s:34:\"acf-content-analysis-for-yoast-seo\";s:6:\"plugin\";s:57:\"acf-content-analysis-for-yoast-seo/yoast-acf-analysis.php\";s:11:\"new_version\";s:5:\"2.1.0\";s:3:\"url\";s:65:\"https://wordpress.org/plugins/acf-content-analysis-for-yoast-seo/\";s:7:\"package\";s:83:\"https://downloads.wordpress.org/plugin/acf-content-analysis-for-yoast-seo.2.1.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:87:\"https://ps.w.org/acf-content-analysis-for-yoast-seo/assets/icon-256x256.png?rev=1717503\";s:2:\"1x\";s:87:\"https://ps.w.org/acf-content-analysis-for-yoast-seo/assets/icon-128x128.png?rev=1717503\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:90:\"https://ps.w.org/acf-content-analysis-for-yoast-seo/assets/banner-1544x500.png?rev=1717503\";s:2:\"1x\";s:89:\"https://ps.w.org/acf-content-analysis-for-yoast-seo/assets/banner-772x250.png?rev=1717503\";}s:11:\"banners_rtl\";a:0:{}}s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:6:\"4.4.12\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:72:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.4.4.12.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}}s:19:\"bbpress/bbpress.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/bbpress\";s:4:\"slug\";s:7:\"bbpress\";s:6:\"plugin\";s:19:\"bbpress/bbpress.php\";s:11:\"new_version\";s:6:\"2.5.14\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/bbpress/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/bbpress.2.5.14.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:60:\"https://ps.w.org/bbpress/assets/icon-256x256.png?rev=1534011\";s:2:\"1x\";s:51:\"https://ps.w.org/bbpress/assets/icon.svg?rev=978290\";s:3:\"svg\";s:51:\"https://ps.w.org/bbpress/assets/icon.svg?rev=978290\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:62:\"https://ps.w.org/bbpress/assets/banner-1544x500.png?rev=567403\";s:2:\"1x\";s:61:\"https://ps.w.org/bbpress/assets/banner-772x250.png?rev=478663\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/bbpress/assets/banner-1544x500-rtl.png?rev=1534011\";s:2:\"1x\";s:66:\"https://ps.w.org/bbpress/assets/banner-772x250-rtl.png?rev=1534011\";}}s:24:\"buddypress/bp-loader.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:24:\"w.org/plugins/buddypress\";s:4:\"slug\";s:10:\"buddypress\";s:6:\"plugin\";s:24:\"buddypress/bp-loader.php\";s:11:\"new_version\";s:5:\"3.1.0\";s:3:\"url\";s:41:\"https://wordpress.org/plugins/buddypress/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/plugin/buddypress.3.1.0.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:63:\"https://ps.w.org/buddypress/assets/icon-256x256.png?rev=1309232\";s:2:\"1x\";s:54:\"https://ps.w.org/buddypress/assets/icon.svg?rev=977480\";s:3:\"svg\";s:54:\"https://ps.w.org/buddypress/assets/icon.svg?rev=977480\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/buddypress/assets/banner-1544x500.png?rev=1854372\";s:2:\"1x\";s:65:\"https://ps.w.org/buddypress/assets/banner-772x250.png?rev=1854372\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/buddypress/assets/banner-1544x500-rtl.png?rev=1854372\";s:2:\"1x\";s:69:\"https://ps.w.org/buddypress/assets/banner-772x250-rtl.png?rev=1854372\";}}s:26:\"custom-field-suite/cfs.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/custom-field-suite\";s:4:\"slug\";s:18:\"custom-field-suite\";s:6:\"plugin\";s:26:\"custom-field-suite/cfs.php\";s:11:\"new_version\";s:6:\"2.5.12\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/custom-field-suite/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/custom-field-suite.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/custom-field-suite/assets/icon-256x256.png?rev=1112866\";s:2:\"1x\";s:71:\"https://ps.w.org/custom-field-suite/assets/icon-128x128.png?rev=1112866\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:43:\"custom-post-type-ui/custom-post-type-ui.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:33:\"w.org/plugins/custom-post-type-ui\";s:4:\"slug\";s:19:\"custom-post-type-ui\";s:6:\"plugin\";s:43:\"custom-post-type-ui/custom-post-type-ui.php\";s:11:\"new_version\";s:5:\"1.5.8\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/custom-post-type-ui/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/custom-post-type-ui.1.5.8.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/custom-post-type-ui/assets/icon-256x256.png?rev=1069557\";s:2:\"1x\";s:72:\"https://ps.w.org/custom-post-type-ui/assets/icon-128x128.png?rev=1069557\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/custom-post-type-ui/assets/banner-1544x500.png?rev=1069557\";s:2:\"1x\";s:74:\"https://ps.w.org/custom-post-type-ui/assets/banner-772x250.png?rev=1069557\";}s:11:\"banners_rtl\";a:0:{}}s:23:\"elementor/elementor.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:23:\"w.org/plugins/elementor\";s:4:\"slug\";s:9:\"elementor\";s:6:\"plugin\";s:23:\"elementor/elementor.php\";s:11:\"new_version\";s:5:\"2.2.1\";s:3:\"url\";s:40:\"https://wordpress.org/plugins/elementor/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/elementor.2.2.1.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:62:\"https://ps.w.org/elementor/assets/icon-256x256.png?rev=1427768\";s:2:\"1x\";s:54:\"https://ps.w.org/elementor/assets/icon.svg?rev=1426809\";s:3:\"svg\";s:54:\"https://ps.w.org/elementor/assets/icon.svg?rev=1426809\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/elementor/assets/banner-1544x500.png?rev=1475479\";s:2:\"1x\";s:64:\"https://ps.w.org/elementor/assets/banner-772x250.png?rev=1475479\";}s:11:\"banners_rtl\";a:0:{}}s:31:\"what-the-file/what-the-file.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/what-the-file\";s:4:\"slug\";s:13:\"what-the-file\";s:6:\"plugin\";s:31:\"what-the-file/what-the-file.php\";s:11:\"new_version\";s:5:\"1.5.4\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/what-the-file/\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/plugin/what-the-file.1.5.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/what-the-file/assets/icon-256x256.png?rev=1223609\";s:2:\"1x\";s:66:\"https://ps.w.org/what-the-file/assets/icon-128x128.png?rev=1223609\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:67:\"https://ps.w.org/what-the-file/assets/banner-772x250.jpg?rev=685200\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(1033, 'wpseo_sitemap_86_cache_validator', 'KuDb', 'no'),
(1034, 'wpseo_sitemap_90_cache_validator', 'KuEY', 'no'),
(1035, '_transient_timeout_plugin_slugs', '1536808873', 'no'),
(1036, '_transient_plugin_slugs', 'a:11:{i:0;s:57:\"acf-content-analysis-for-yoast-seo/yoast-acf-analysis.php\";i:1;s:30:\"advanced-custom-fields/acf.php\";i:2;s:19:\"bbpress/bbpress.php\";i:3;s:22:\"yuuki/yuuki-plugin.php\";i:4;s:24:\"buddypress/bp-loader.php\";i:5;s:26:\"custom-field-suite/cfs.php\";i:6;s:43:\"custom-post-type-ui/custom-post-type-ui.php\";i:7;s:23:\"elementor/elementor.php\";i:8;s:31:\"what-the-file/what-the-file.php\";i:9;s:24:\"wordpress-seo/wp-seo.php\";i:10;s:17:\"youzer/youzer.php\";}', 'no'),
(1037, 'wpseo_sitemap_129_cache_validator', 'PxEZ', 'no'),
(1038, 'wpseo_sitemap_130_cache_validator', 'PxGD', 'no'),
(1039, 'wpseo_sitemap_131_cache_validator', 'PxIh', 'no'),
(1040, '_site_transient_timeout_available_translations', '1536733560', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1041, '_site_transient_available_translations', 'a:113:{s:2:\"af\";a:8:{s:8:\"language\";s:2:\"af\";s:7:\"version\";s:5:\"4.9.4\";s:7:\"updated\";s:19:\"2018-02-06 13:56:09\";s:12:\"english_name\";s:9:\"Afrikaans\";s:11:\"native_name\";s:9:\"Afrikaans\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.4/af.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"af\";i:2;s:3:\"afr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Gaan voort\";}}s:2:\"ar\";a:8:{s:8:\"language\";s:2:\"ar\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-11 21:04:38\";s:12:\"english_name\";s:6:\"Arabic\";s:11:\"native_name\";s:14:\"العربية\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/ar.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:2;s:3:\"ara\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:3:\"ary\";a:8:{s:8:\"language\";s:3:\"ary\";s:7:\"version\";s:5:\"4.7.7\";s:7:\"updated\";s:19:\"2017-01-26 15:42:35\";s:12:\"english_name\";s:15:\"Moroccan Arabic\";s:11:\"native_name\";s:31:\"العربية المغربية\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.7/ary.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:3;s:3:\"ary\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:2:\"as\";a:8:{s:8:\"language\";s:2:\"as\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-22 18:59:07\";s:12:\"english_name\";s:8:\"Assamese\";s:11:\"native_name\";s:21:\"অসমীয়া\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/as.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"as\";i:2;s:3:\"asm\";i:3;s:3:\"asm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:2:\"az\";a:8:{s:8:\"language\";s:2:\"az\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-06 00:09:27\";s:12:\"english_name\";s:11:\"Azerbaijani\";s:11:\"native_name\";s:16:\"Azərbaycan dili\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/az.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:2;s:3:\"aze\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Davam\";}}s:3:\"azb\";a:8:{s:8:\"language\";s:3:\"azb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-12 20:34:31\";s:12:\"english_name\";s:17:\"South Azerbaijani\";s:11:\"native_name\";s:29:\"گؤنئی آذربایجان\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/azb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:3;s:3:\"azb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:3:\"bel\";a:8:{s:8:\"language\";s:3:\"bel\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-04-04 08:43:29\";s:12:\"english_name\";s:10:\"Belarusian\";s:11:\"native_name\";s:29:\"Беларуская мова\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.5/bel.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"be\";i:2;s:3:\"bel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Працягнуць\";}}s:5:\"bg_BG\";a:8:{s:8:\"language\";s:5:\"bg_BG\";s:7:\"version\";s:5:\"4.9.6\";s:7:\"updated\";s:19:\"2018-06-23 07:27:43\";s:12:\"english_name\";s:9:\"Bulgarian\";s:11:\"native_name\";s:18:\"Български\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.6/bg_BG.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bg\";i:2;s:3:\"bul\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Напред\";}}s:5:\"bn_BD\";a:8:{s:8:\"language\";s:5:\"bn_BD\";s:7:\"version\";s:5:\"4.8.6\";s:7:\"updated\";s:19:\"2017-10-01 12:57:10\";s:12:\"english_name\";s:7:\"Bengali\";s:11:\"native_name\";s:15:\"বাংলা\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.8.6/bn_BD.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"bn\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:23:\"এগিয়ে চল.\";}}s:2:\"bo\";a:8:{s:8:\"language\";s:2:\"bo\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-07-26 07:51:00\";s:12:\"english_name\";s:7:\"Tibetan\";s:11:\"native_name\";s:21:\"བོད་ཡིག\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/bo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bo\";i:2;s:3:\"tib\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"མུ་མཐུད།\";}}s:5:\"bs_BA\";a:8:{s:8:\"language\";s:5:\"bs_BA\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-04 20:20:28\";s:12:\"english_name\";s:7:\"Bosnian\";s:11:\"native_name\";s:8:\"Bosanski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/bs_BA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bs\";i:2;s:3:\"bos\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:2:\"ca\";a:8:{s:8:\"language\";s:2:\"ca\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-05 05:34:46\";s:12:\"english_name\";s:7:\"Catalan\";s:11:\"native_name\";s:7:\"Català\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/ca.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ca\";i:2;s:3:\"cat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:3:\"ceb\";a:8:{s:8:\"language\";s:3:\"ceb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-02 17:25:51\";s:12:\"english_name\";s:7:\"Cebuano\";s:11:\"native_name\";s:7:\"Cebuano\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/ceb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"ceb\";i:3;s:3:\"ceb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Padayun\";}}s:5:\"cs_CZ\";a:8:{s:8:\"language\";s:5:\"cs_CZ\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-05 11:25:11\";s:12:\"english_name\";s:5:\"Czech\";s:11:\"native_name\";s:9:\"Čeština\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/cs_CZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cs\";i:2;s:3:\"ces\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Pokračovat\";}}s:2:\"cy\";a:8:{s:8:\"language\";s:2:\"cy\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-07-27 10:53:54\";s:12:\"english_name\";s:5:\"Welsh\";s:11:\"native_name\";s:7:\"Cymraeg\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/cy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cy\";i:2;s:3:\"cym\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Parhau\";}}s:5:\"da_DK\";a:8:{s:8:\"language\";s:5:\"da_DK\";s:7:\"version\";s:5:\"4.9.7\";s:7:\"updated\";s:19:\"2018-07-06 08:46:24\";s:12:\"english_name\";s:6:\"Danish\";s:11:\"native_name\";s:5:\"Dansk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.7/da_DK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"da\";i:2;s:3:\"dan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsæt\";}}s:12:\"de_DE_formal\";a:8:{s:8:\"language\";s:12:\"de_DE_formal\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-04 07:45:31\";s:12:\"english_name\";s:15:\"German (Formal)\";s:11:\"native_name\";s:13:\"Deutsch (Sie)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/4.9.8/de_DE_formal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:5:\"de_CH\";a:8:{s:8:\"language\";s:5:\"de_CH\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-07-28 11:47:36\";s:12:\"english_name\";s:20:\"German (Switzerland)\";s:11:\"native_name\";s:17:\"Deutsch (Schweiz)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/de_CH.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:14:\"de_CH_informal\";a:8:{s:8:\"language\";s:14:\"de_CH_informal\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-07-28 11:48:22\";s:12:\"english_name\";s:30:\"German (Switzerland, Informal)\";s:11:\"native_name\";s:21:\"Deutsch (Schweiz, Du)\";s:7:\"package\";s:73:\"https://downloads.wordpress.org/translation/core/4.9.8/de_CH_informal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:5:\"de_DE\";a:8:{s:8:\"language\";s:5:\"de_DE\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-10 07:31:38\";s:12:\"english_name\";s:6:\"German\";s:11:\"native_name\";s:7:\"Deutsch\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/de_DE.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:3:\"dzo\";a:8:{s:8:\"language\";s:3:\"dzo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-06-29 08:59:03\";s:12:\"english_name\";s:8:\"Dzongkha\";s:11:\"native_name\";s:18:\"རྫོང་ཁ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/dzo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"dz\";i:2;s:3:\"dzo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:2:\"el\";a:8:{s:8:\"language\";s:2:\"el\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-04 17:01:04\";s:12:\"english_name\";s:5:\"Greek\";s:11:\"native_name\";s:16:\"Ελληνικά\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/el.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"el\";i:2;s:3:\"ell\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Συνέχεια\";}}s:5:\"en_GB\";a:8:{s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-04 09:59:15\";s:12:\"english_name\";s:12:\"English (UK)\";s:11:\"native_name\";s:12:\"English (UK)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/en_GB.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_CA\";a:8:{s:8:\"language\";s:5:\"en_CA\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-04 20:38:16\";s:12:\"english_name\";s:16:\"English (Canada)\";s:11:\"native_name\";s:16:\"English (Canada)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/en_CA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_ZA\";a:8:{s:8:\"language\";s:5:\"en_ZA\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-04 13:34:08\";s:12:\"english_name\";s:22:\"English (South Africa)\";s:11:\"native_name\";s:22:\"English (South Africa)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/en_ZA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_NZ\";a:8:{s:8:\"language\";s:5:\"en_NZ\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-04 21:44:38\";s:12:\"english_name\";s:21:\"English (New Zealand)\";s:11:\"native_name\";s:21:\"English (New Zealand)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/en_NZ.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_AU\";a:8:{s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-04 00:25:28\";s:12:\"english_name\";s:19:\"English (Australia)\";s:11:\"native_name\";s:19:\"English (Australia)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/en_AU.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"eo\";a:8:{s:8:\"language\";s:2:\"eo\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-01 16:09:29\";s:12:\"english_name\";s:9:\"Esperanto\";s:11:\"native_name\";s:9:\"Esperanto\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/eo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eo\";i:2;s:3:\"epo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Daŭrigi\";}}s:5:\"es_VE\";a:8:{s:8:\"language\";s:5:\"es_VE\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-21 14:41:13\";s:12:\"english_name\";s:19:\"Spanish (Venezuela)\";s:11:\"native_name\";s:21:\"Español de Venezuela\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/es_VE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_ES\";a:8:{s:8:\"language\";s:5:\"es_ES\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-04 11:18:39\";s:12:\"english_name\";s:15:\"Spanish (Spain)\";s:11:\"native_name\";s:8:\"Español\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/es_ES.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_GT\";a:8:{s:8:\"language\";s:5:\"es_GT\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-15 15:03:42\";s:12:\"english_name\";s:19:\"Spanish (Guatemala)\";s:11:\"native_name\";s:21:\"Español de Guatemala\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/es_GT.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CO\";a:8:{s:8:\"language\";s:5:\"es_CO\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-15 23:17:08\";s:12:\"english_name\";s:18:\"Spanish (Colombia)\";s:11:\"native_name\";s:20:\"Español de Colombia\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/es_CO.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_MX\";a:8:{s:8:\"language\";s:5:\"es_MX\";s:7:\"version\";s:5:\"4.8.6\";s:7:\"updated\";s:19:\"2017-07-31 15:12:02\";s:12:\"english_name\";s:16:\"Spanish (Mexico)\";s:11:\"native_name\";s:19:\"Español de México\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.8.6/es_MX.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CR\";a:8:{s:8:\"language\";s:5:\"es_CR\";s:7:\"version\";s:5:\"4.8.3\";s:7:\"updated\";s:19:\"2017-10-01 17:54:52\";s:12:\"english_name\";s:20:\"Spanish (Costa Rica)\";s:11:\"native_name\";s:22:\"Español de Costa Rica\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.8.3/es_CR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_PE\";a:8:{s:8:\"language\";s:5:\"es_PE\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-09 09:36:22\";s:12:\"english_name\";s:14:\"Spanish (Peru)\";s:11:\"native_name\";s:17:\"Español de Perú\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/es_PE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_AR\";a:8:{s:8:\"language\";s:5:\"es_AR\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-03 20:43:09\";s:12:\"english_name\";s:19:\"Spanish (Argentina)\";s:11:\"native_name\";s:21:\"Español de Argentina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/es_AR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CL\";a:8:{s:8:\"language\";s:5:\"es_CL\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-06 13:31:53\";s:12:\"english_name\";s:15:\"Spanish (Chile)\";s:11:\"native_name\";s:17:\"Español de Chile\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/es_CL.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"et\";a:8:{s:8:\"language\";s:2:\"et\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-19 14:11:29\";s:12:\"english_name\";s:8:\"Estonian\";s:11:\"native_name\";s:5:\"Eesti\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/et.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"et\";i:2;s:3:\"est\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Jätka\";}}s:2:\"eu\";a:8:{s:8:\"language\";s:2:\"eu\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-09 21:12:23\";s:12:\"english_name\";s:6:\"Basque\";s:11:\"native_name\";s:7:\"Euskara\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/eu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eu\";i:2;s:3:\"eus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Jarraitu\";}}s:5:\"fa_IR\";a:8:{s:8:\"language\";s:5:\"fa_IR\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-04 10:46:48\";s:12:\"english_name\";s:7:\"Persian\";s:11:\"native_name\";s:10:\"فارسی\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/fa_IR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fa\";i:2;s:3:\"fas\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:2:\"fi\";a:8:{s:8:\"language\";s:2:\"fi\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-02 12:18:54\";s:12:\"english_name\";s:7:\"Finnish\";s:11:\"native_name\";s:5:\"Suomi\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/fi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fi\";i:2;s:3:\"fin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Jatka\";}}s:5:\"fr_CA\";a:8:{s:8:\"language\";s:5:\"fr_CA\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-06 16:13:32\";s:12:\"english_name\";s:15:\"French (Canada)\";s:11:\"native_name\";s:19:\"Français du Canada\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/fr_CA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_BE\";a:8:{s:8:\"language\";s:5:\"fr_BE\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-01-31 11:16:06\";s:12:\"english_name\";s:16:\"French (Belgium)\";s:11:\"native_name\";s:21:\"Français de Belgique\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/fr_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_FR\";a:8:{s:8:\"language\";s:5:\"fr_FR\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-10 07:48:09\";s:12:\"english_name\";s:15:\"French (France)\";s:11:\"native_name\";s:9:\"Français\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/fr_FR.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"fr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:3:\"fur\";a:8:{s:8:\"language\";s:3:\"fur\";s:7:\"version\";s:5:\"4.8.6\";s:7:\"updated\";s:19:\"2018-01-29 17:32:35\";s:12:\"english_name\";s:8:\"Friulian\";s:11:\"native_name\";s:8:\"Friulian\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.6/fur.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"fur\";i:3;s:3:\"fur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"gd\";a:8:{s:8:\"language\";s:2:\"gd\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-08-23 17:41:37\";s:12:\"english_name\";s:15:\"Scottish Gaelic\";s:11:\"native_name\";s:9:\"Gàidhlig\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/gd.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"gd\";i:2;s:3:\"gla\";i:3;s:3:\"gla\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"Lean air adhart\";}}s:5:\"gl_ES\";a:8:{s:8:\"language\";s:5:\"gl_ES\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-04 10:46:11\";s:12:\"english_name\";s:8:\"Galician\";s:11:\"native_name\";s:6:\"Galego\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/gl_ES.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gl\";i:2;s:3:\"glg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"gu\";a:8:{s:8:\"language\";s:2:\"gu\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-02-14 06:16:04\";s:12:\"english_name\";s:8:\"Gujarati\";s:11:\"native_name\";s:21:\"ગુજરાતી\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/gu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gu\";i:2;s:3:\"guj\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:31:\"ચાલુ રાખવું\";}}s:3:\"haz\";a:8:{s:8:\"language\";s:3:\"haz\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-05 00:59:09\";s:12:\"english_name\";s:8:\"Hazaragi\";s:11:\"native_name\";s:15:\"هزاره گی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.4.2/haz.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"haz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:5:\"he_IL\";a:8:{s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-04-15 08:49:46\";s:12:\"english_name\";s:6:\"Hebrew\";s:11:\"native_name\";s:16:\"עִבְרִית\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/he_IL.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"he\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"המשך\";}}s:5:\"hi_IN\";a:8:{s:8:\"language\";s:5:\"hi_IN\";s:7:\"version\";s:5:\"4.9.7\";s:7:\"updated\";s:19:\"2018-06-17 09:33:44\";s:12:\"english_name\";s:5:\"Hindi\";s:11:\"native_name\";s:18:\"हिन्दी\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.7/hi_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hi\";i:2;s:3:\"hin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"जारी\";}}s:2:\"hr\";a:8:{s:8:\"language\";s:2:\"hr\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-14 10:04:37\";s:12:\"english_name\";s:8:\"Croatian\";s:11:\"native_name\";s:8:\"Hrvatski\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/hr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hr\";i:2;s:3:\"hrv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:5:\"hu_HU\";a:8:{s:8:\"language\";s:5:\"hu_HU\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-03 10:29:39\";s:12:\"english_name\";s:9:\"Hungarian\";s:11:\"native_name\";s:6:\"Magyar\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/hu_HU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hu\";i:2;s:3:\"hun\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Folytatás\";}}s:2:\"hy\";a:8:{s:8:\"language\";s:2:\"hy\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-03 16:21:10\";s:12:\"english_name\";s:8:\"Armenian\";s:11:\"native_name\";s:14:\"Հայերեն\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/hy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hy\";i:2;s:3:\"hye\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Շարունակել\";}}s:5:\"id_ID\";a:8:{s:8:\"language\";s:5:\"id_ID\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-07-28 13:16:13\";s:12:\"english_name\";s:10:\"Indonesian\";s:11:\"native_name\";s:16:\"Bahasa Indonesia\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/id_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"id\";i:2;s:3:\"ind\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Lanjutkan\";}}s:5:\"is_IS\";a:8:{s:8:\"language\";s:5:\"is_IS\";s:7:\"version\";s:5:\"4.7.7\";s:7:\"updated\";s:19:\"2017-04-13 13:55:54\";s:12:\"english_name\";s:9:\"Icelandic\";s:11:\"native_name\";s:9:\"Íslenska\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.7/is_IS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"is\";i:2;s:3:\"isl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Áfram\";}}s:5:\"it_IT\";a:8:{s:8:\"language\";s:5:\"it_IT\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-05 07:38:36\";s:12:\"english_name\";s:7:\"Italian\";s:11:\"native_name\";s:8:\"Italiano\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/it_IT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"it\";i:2;s:3:\"ita\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:2:\"ja\";a:8:{s:8:\"language\";s:2:\"ja\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-05 06:05:23\";s:12:\"english_name\";s:8:\"Japanese\";s:11:\"native_name\";s:9:\"日本語\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/ja.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"ja\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"続ける\";}}s:5:\"jv_ID\";a:8:{s:8:\"language\";s:5:\"jv_ID\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-24 13:53:29\";s:12:\"english_name\";s:8:\"Javanese\";s:11:\"native_name\";s:9:\"Basa Jawa\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/jv_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"jv\";i:2;s:3:\"jav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Nerusaké\";}}s:5:\"ka_GE\";a:8:{s:8:\"language\";s:5:\"ka_GE\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-21 12:12:01\";s:12:\"english_name\";s:8:\"Georgian\";s:11:\"native_name\";s:21:\"ქართული\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/ka_GE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ka\";i:2;s:3:\"kat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"გაგრძელება\";}}s:3:\"kab\";a:8:{s:8:\"language\";s:3:\"kab\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-08 20:47:16\";s:12:\"english_name\";s:6:\"Kabyle\";s:11:\"native_name\";s:9:\"Taqbaylit\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.8/kab.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"kab\";i:3;s:3:\"kab\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Kemmel\";}}s:2:\"kk\";a:8:{s:8:\"language\";s:2:\"kk\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-12 08:08:32\";s:12:\"english_name\";s:6:\"Kazakh\";s:11:\"native_name\";s:19:\"Қазақ тілі\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.5/kk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"kk\";i:2;s:3:\"kaz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Жалғастыру\";}}s:2:\"km\";a:8:{s:8:\"language\";s:2:\"km\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-07 02:07:59\";s:12:\"english_name\";s:5:\"Khmer\";s:11:\"native_name\";s:27:\"ភាសាខ្មែរ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/km.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"km\";i:2;s:3:\"khm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"បន្ត\";}}s:5:\"ko_KR\";a:8:{s:8:\"language\";s:5:\"ko_KR\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-08 15:27:34\";s:12:\"english_name\";s:6:\"Korean\";s:11:\"native_name\";s:9:\"한국어\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/ko_KR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ko\";i:2;s:3:\"kor\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"계속\";}}s:3:\"ckb\";a:8:{s:8:\"language\";s:3:\"ckb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:48:25\";s:12:\"english_name\";s:16:\"Kurdish (Sorani)\";s:11:\"native_name\";s:13:\"كوردی‎\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/ckb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ku\";i:3;s:3:\"ckb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"به‌رده‌وام به‌\";}}s:2:\"lo\";a:8:{s:8:\"language\";s:2:\"lo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 09:59:23\";s:12:\"english_name\";s:3:\"Lao\";s:11:\"native_name\";s:21:\"ພາສາລາວ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/lo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lo\";i:2;s:3:\"lao\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"ຕໍ່​ໄປ\";}}s:5:\"lt_LT\";a:8:{s:8:\"language\";s:5:\"lt_LT\";s:7:\"version\";s:5:\"4.9.6\";s:7:\"updated\";s:19:\"2018-05-24 09:42:27\";s:12:\"english_name\";s:10:\"Lithuanian\";s:11:\"native_name\";s:15:\"Lietuvių kalba\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.6/lt_LT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lt\";i:2;s:3:\"lit\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Tęsti\";}}s:2:\"lv\";a:8:{s:8:\"language\";s:2:\"lv\";s:7:\"version\";s:5:\"4.7.7\";s:7:\"updated\";s:19:\"2017-03-17 20:40:40\";s:12:\"english_name\";s:7:\"Latvian\";s:11:\"native_name\";s:16:\"Latviešu valoda\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.7/lv.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lv\";i:2;s:3:\"lav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Turpināt\";}}s:5:\"mk_MK\";a:8:{s:8:\"language\";s:5:\"mk_MK\";s:7:\"version\";s:5:\"4.7.7\";s:7:\"updated\";s:19:\"2017-01-26 15:54:41\";s:12:\"english_name\";s:10:\"Macedonian\";s:11:\"native_name\";s:31:\"Македонски јазик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.7/mk_MK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mk\";i:2;s:3:\"mkd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Продолжи\";}}s:5:\"ml_IN\";a:8:{s:8:\"language\";s:5:\"ml_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:43:32\";s:12:\"english_name\";s:9:\"Malayalam\";s:11:\"native_name\";s:18:\"മലയാളം\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ml_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ml\";i:2;s:3:\"mal\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"തുടരുക\";}}s:2:\"mn\";a:8:{s:8:\"language\";s:2:\"mn\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-12 07:29:35\";s:12:\"english_name\";s:9:\"Mongolian\";s:11:\"native_name\";s:12:\"Монгол\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/mn.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mn\";i:2;s:3:\"mon\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"Үргэлжлүүлэх\";}}s:2:\"mr\";a:8:{s:8:\"language\";s:2:\"mr\";s:7:\"version\";s:5:\"4.8.6\";s:7:\"updated\";s:19:\"2018-02-13 07:38:55\";s:12:\"english_name\";s:7:\"Marathi\";s:11:\"native_name\";s:15:\"मराठी\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.8.6/mr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mr\";i:2;s:3:\"mar\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"सुरु ठेवा\";}}s:5:\"ms_MY\";a:8:{s:8:\"language\";s:5:\"ms_MY\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-30 20:27:25\";s:12:\"english_name\";s:5:\"Malay\";s:11:\"native_name\";s:13:\"Bahasa Melayu\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/ms_MY.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ms\";i:2;s:3:\"msa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Teruskan\";}}s:5:\"my_MM\";a:8:{s:8:\"language\";s:5:\"my_MM\";s:7:\"version\";s:6:\"4.1.20\";s:7:\"updated\";s:19:\"2015-03-26 15:57:42\";s:12:\"english_name\";s:17:\"Myanmar (Burmese)\";s:11:\"native_name\";s:15:\"ဗမာစာ\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.1.20/my_MM.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"my\";i:2;s:3:\"mya\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:54:\"ဆက်လက်လုပ်ဆောင်ပါ။\";}}s:5:\"nb_NO\";a:8:{s:8:\"language\";s:5:\"nb_NO\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-03 07:24:43\";s:12:\"english_name\";s:19:\"Norwegian (Bokmål)\";s:11:\"native_name\";s:13:\"Norsk bokmål\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/nb_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nb\";i:2;s:3:\"nob\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsett\";}}s:5:\"ne_NP\";a:8:{s:8:\"language\";s:5:\"ne_NP\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-27 10:30:26\";s:12:\"english_name\";s:6:\"Nepali\";s:11:\"native_name\";s:18:\"नेपाली\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/ne_NP.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ne\";i:2;s:3:\"nep\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:43:\"जारी राख्नुहोस्\";}}s:5:\"nl_BE\";a:8:{s:8:\"language\";s:5:\"nl_BE\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-07-29 11:28:05\";s:12:\"english_name\";s:15:\"Dutch (Belgium)\";s:11:\"native_name\";s:20:\"Nederlands (België)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/nl_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nl_NL\";a:8:{s:8:\"language\";s:5:\"nl_NL\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-04 08:56:33\";s:12:\"english_name\";s:5:\"Dutch\";s:11:\"native_name\";s:10:\"Nederlands\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/nl_NL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:12:\"nl_NL_formal\";a:8:{s:8:\"language\";s:12:\"nl_NL_formal\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-06 12:43:59\";s:12:\"english_name\";s:14:\"Dutch (Formal)\";s:11:\"native_name\";s:20:\"Nederlands (Formeel)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/4.9.8/nl_NL_formal.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nn_NO\";a:8:{s:8:\"language\";s:5:\"nn_NO\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-05 09:45:40\";s:12:\"english_name\";s:19:\"Norwegian (Nynorsk)\";s:11:\"native_name\";s:13:\"Norsk nynorsk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/nn_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nn\";i:2;s:3:\"nno\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Hald fram\";}}s:3:\"oci\";a:8:{s:8:\"language\";s:3:\"oci\";s:7:\"version\";s:5:\"4.8.3\";s:7:\"updated\";s:19:\"2017-08-25 10:03:08\";s:12:\"english_name\";s:7:\"Occitan\";s:11:\"native_name\";s:7:\"Occitan\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.3/oci.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"oc\";i:2;s:3:\"oci\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Contunhar\";}}s:5:\"pa_IN\";a:8:{s:8:\"language\";s:5:\"pa_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-16 05:19:43\";s:12:\"english_name\";s:7:\"Punjabi\";s:11:\"native_name\";s:18:\"ਪੰਜਾਬੀ\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/pa_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pa\";i:2;s:3:\"pan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"ਜਾਰੀ ਰੱਖੋ\";}}s:5:\"pl_PL\";a:8:{s:8:\"language\";s:5:\"pl_PL\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-11 07:56:35\";s:12:\"english_name\";s:6:\"Polish\";s:11:\"native_name\";s:6:\"Polski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/pl_PL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pl\";i:2;s:3:\"pol\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Kontynuuj\";}}s:2:\"ps\";a:8:{s:8:\"language\";s:2:\"ps\";s:7:\"version\";s:6:\"4.1.20\";s:7:\"updated\";s:19:\"2015-03-29 22:19:48\";s:12:\"english_name\";s:6:\"Pashto\";s:11:\"native_name\";s:8:\"پښتو\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.1.20/ps.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ps\";i:2;s:3:\"pus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"دوام ورکړه\";}}s:5:\"pt_PT\";a:8:{s:8:\"language\";s:5:\"pt_PT\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-04 15:06:12\";s:12:\"english_name\";s:21:\"Portuguese (Portugal)\";s:11:\"native_name\";s:10:\"Português\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/pt_PT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_BR\";a:8:{s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-04 14:22:52\";s:12:\"english_name\";s:19:\"Portuguese (Brazil)\";s:11:\"native_name\";s:20:\"Português do Brasil\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/pt_BR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pt\";i:2;s:3:\"por\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:10:\"pt_PT_ao90\";a:8:{s:8:\"language\";s:10:\"pt_PT_ao90\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-09 09:30:48\";s:12:\"english_name\";s:27:\"Portuguese (Portugal, AO90)\";s:11:\"native_name\";s:17:\"Português (AO90)\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/translation/core/4.9.5/pt_PT_ao90.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:3:\"rhg\";a:8:{s:8:\"language\";s:3:\"rhg\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-16 13:03:18\";s:12:\"english_name\";s:8:\"Rohingya\";s:11:\"native_name\";s:8:\"Ruáinga\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/rhg.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"rhg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:5:\"ro_RO\";a:8:{s:8:\"language\";s:5:\"ro_RO\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-08 09:33:19\";s:12:\"english_name\";s:8:\"Romanian\";s:11:\"native_name\";s:8:\"Română\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/ro_RO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ro\";i:2;s:3:\"ron\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuă\";}}s:5:\"ru_RU\";a:8:{s:8:\"language\";s:5:\"ru_RU\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-04 08:15:10\";s:12:\"english_name\";s:7:\"Russian\";s:11:\"native_name\";s:14:\"Русский\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/ru_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ru\";i:2;s:3:\"rus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продолжить\";}}s:3:\"sah\";a:8:{s:8:\"language\";s:3:\"sah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-21 02:06:41\";s:12:\"english_name\";s:5:\"Sakha\";s:11:\"native_name\";s:14:\"Сахалыы\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/sah.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"sah\";i:3;s:3:\"sah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Салҕаа\";}}s:5:\"si_LK\";a:8:{s:8:\"language\";s:5:\"si_LK\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 06:00:52\";s:12:\"english_name\";s:7:\"Sinhala\";s:11:\"native_name\";s:15:\"සිංහල\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/si_LK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"si\";i:2;s:3:\"sin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:44:\"දිගටම කරගෙන යන්න\";}}s:5:\"sk_SK\";a:8:{s:8:\"language\";s:5:\"sk_SK\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-28 12:46:02\";s:12:\"english_name\";s:6:\"Slovak\";s:11:\"native_name\";s:11:\"Slovenčina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/sk_SK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sk\";i:2;s:3:\"slk\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Pokračovať\";}}s:5:\"sl_SI\";a:8:{s:8:\"language\";s:5:\"sl_SI\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-04 13:33:13\";s:12:\"english_name\";s:9:\"Slovenian\";s:11:\"native_name\";s:13:\"Slovenščina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/sl_SI.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sl\";i:2;s:3:\"slv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Nadaljuj\";}}s:2:\"sq\";a:8:{s:8:\"language\";s:2:\"sq\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-04 09:01:19\";s:12:\"english_name\";s:8:\"Albanian\";s:11:\"native_name\";s:5:\"Shqip\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/sq.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sq\";i:2;s:3:\"sqi\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Vazhdo\";}}s:5:\"sr_RS\";a:8:{s:8:\"language\";s:5:\"sr_RS\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-02 20:59:54\";s:12:\"english_name\";s:7:\"Serbian\";s:11:\"native_name\";s:23:\"Српски језик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/sr_RS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sr\";i:2;s:3:\"srp\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:14:\"Настави\";}}s:5:\"sv_SE\";a:8:{s:8:\"language\";s:5:\"sv_SE\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-03 22:59:07\";s:12:\"english_name\";s:7:\"Swedish\";s:11:\"native_name\";s:7:\"Svenska\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/sv_SE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sv\";i:2;s:3:\"swe\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Fortsätt\";}}s:3:\"szl\";a:8:{s:8:\"language\";s:3:\"szl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-24 19:58:14\";s:12:\"english_name\";s:8:\"Silesian\";s:11:\"native_name\";s:17:\"Ślōnskŏ gŏdka\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/szl.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"szl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:13:\"Kōntynuować\";}}s:5:\"ta_IN\";a:8:{s:8:\"language\";s:5:\"ta_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:22:47\";s:12:\"english_name\";s:5:\"Tamil\";s:11:\"native_name\";s:15:\"தமிழ்\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ta_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ta\";i:2;s:3:\"tam\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"தொடரவும்\";}}s:2:\"te\";a:8:{s:8:\"language\";s:2:\"te\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:47:39\";s:12:\"english_name\";s:6:\"Telugu\";s:11:\"native_name\";s:18:\"తెలుగు\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/te.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"te\";i:2;s:3:\"tel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"కొనసాగించు\";}}s:2:\"th\";a:8:{s:8:\"language\";s:2:\"th\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-02 17:08:41\";s:12:\"english_name\";s:4:\"Thai\";s:11:\"native_name\";s:9:\"ไทย\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.5/th.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"th\";i:2;s:3:\"tha\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"ต่อไป\";}}s:2:\"tl\";a:8:{s:8:\"language\";s:2:\"tl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-30 02:38:08\";s:12:\"english_name\";s:7:\"Tagalog\";s:11:\"native_name\";s:7:\"Tagalog\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/tl.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tl\";i:2;s:3:\"tgl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Magpatuloy\";}}s:5:\"tr_TR\";a:8:{s:8:\"language\";s:5:\"tr_TR\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-08 19:05:26\";s:12:\"english_name\";s:7:\"Turkish\";s:11:\"native_name\";s:8:\"Türkçe\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/tr_TR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tr\";i:2;s:3:\"tur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Devam\";}}s:5:\"tt_RU\";a:8:{s:8:\"language\";s:5:\"tt_RU\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-20 20:20:50\";s:12:\"english_name\";s:5:\"Tatar\";s:11:\"native_name\";s:19:\"Татар теле\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/tt_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tt\";i:2;s:3:\"tat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:17:\"дәвам итү\";}}s:3:\"tah\";a:8:{s:8:\"language\";s:3:\"tah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-06 18:39:39\";s:12:\"english_name\";s:8:\"Tahitian\";s:11:\"native_name\";s:10:\"Reo Tahiti\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/tah.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"ty\";i:2;s:3:\"tah\";i:3;s:3:\"tah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:5:\"ug_CN\";a:8:{s:8:\"language\";s:5:\"ug_CN\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-04-12 12:31:53\";s:12:\"english_name\";s:6:\"Uighur\";s:11:\"native_name\";s:16:\"ئۇيغۇرچە\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/ug_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ug\";i:2;s:3:\"uig\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:26:\"داۋاملاشتۇرۇش\";}}s:2:\"uk\";a:8:{s:8:\"language\";s:2:\"uk\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-05 14:19:18\";s:12:\"english_name\";s:9:\"Ukrainian\";s:11:\"native_name\";s:20:\"Українська\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/uk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uk\";i:2;s:3:\"ukr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продовжити\";}}s:2:\"ur\";a:8:{s:8:\"language\";s:2:\"ur\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-03 10:59:52\";s:12:\"english_name\";s:4:\"Urdu\";s:11:\"native_name\";s:8:\"اردو\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/ur.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ur\";i:2;s:3:\"urd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"جاری رکھیں\";}}s:5:\"uz_UZ\";a:8:{s:8:\"language\";s:5:\"uz_UZ\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-09 10:37:43\";s:12:\"english_name\";s:5:\"Uzbek\";s:11:\"native_name\";s:11:\"O‘zbekcha\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/uz_UZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uz\";i:2;s:3:\"uzb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Davom etish\";}}s:2:\"vi\";a:8:{s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-11 09:51:37\";s:12:\"english_name\";s:10:\"Vietnamese\";s:11:\"native_name\";s:14:\"Tiếng Việt\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/vi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"vi\";i:2;s:3:\"vie\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Tiếp tục\";}}s:5:\"zh_HK\";a:8:{s:8:\"language\";s:5:\"zh_HK\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-04-09 00:56:52\";s:12:\"english_name\";s:19:\"Chinese (Hong Kong)\";s:11:\"native_name\";s:16:\"香港中文版	\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/zh_HK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_TW\";a:8:{s:8:\"language\";s:5:\"zh_TW\";s:7:\"version\";s:5:\"4.9.4\";s:7:\"updated\";s:19:\"2018-02-13 02:41:15\";s:12:\"english_name\";s:16:\"Chinese (Taiwan)\";s:11:\"native_name\";s:12:\"繁體中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.4/zh_TW.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_CN\";a:8:{s:8:\"language\";s:5:\"zh_CN\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-17 22:20:52\";s:12:\"english_name\";s:15:\"Chinese (China)\";s:11:\"native_name\";s:12:\"简体中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/zh_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"继续\";}}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 6, '_edit_last', '1'),
(4, 6, '_edit_lock', '1536564285:1'),
(5, 7, '_wp_attached_file', '2018/09/Screenshot-from-2018-08-29-10-56-20.png'),
(6, 7, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1905;s:6:\"height\";i:991;s:4:\"file\";s:47:\"2018/09/Screenshot-from-2018-08-29-10-56-20.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:47:\"Screenshot-from-2018-08-29-10-56-20-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:47:\"Screenshot-from-2018-08-29-10-56-20-300x156.png\";s:5:\"width\";i:300;s:6:\"height\";i:156;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:47:\"Screenshot-from-2018-08-29-10-56-20-768x400.png\";s:5:\"width\";i:768;s:6:\"height\";i:400;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:48:\"Screenshot-from-2018-08-29-10-56-20-1024x533.png\";s:5:\"width\";i:1024;s:6:\"height\";i:533;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(7, 6, '_thumbnail_id', '7'),
(10, 6, '_yoast_wpseo_content_score', '90'),
(11, 6, '_yoast_wpseo_primary_category', '2'),
(12, 9, '_edit_last', '1'),
(13, 9, 'field_5b8ca3de64ead', 'a:11:{s:3:\"key\";s:19:\"field_5b8ca3de64ead\";s:5:\"label\";s:3:\"Log\";s:4:\"name\";s:3:\"log\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";s:3:\"yes\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(14, 9, 'rule', 'a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"post\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(15, 9, 'position', 'normal'),
(16, 9, 'layout', 'no_box'),
(17, 9, 'hide_on_screen', ''),
(18, 9, '_edit_lock', '1535943570:1'),
(19, 10, '_edit_last', '1'),
(20, 10, '_edit_lock', '1535957420:1'),
(21, 10, 'cfs_fields', 'a:3:{i:0;a:8:{s:2:\"id\";s:1:\"1\";s:4:\"name\";s:7:\"add_log\";s:5:\"label\";s:7:\"Add Log\";s:4:\"type\";s:4:\"loop\";s:5:\"notes\";s:0:\"\";s:9:\"parent_id\";i:0;s:6:\"weight\";i:0;s:7:\"options\";a:5:{s:11:\"row_display\";s:1:\"0\";s:9:\"row_label\";s:8:\"Loop Log\";s:12:\"button_label\";s:12:\"Add More Log\";s:9:\"limit_min\";s:0:\"\";s:9:\"limit_max\";s:0:\"\";}}i:1;a:8:{s:2:\"id\";s:1:\"3\";s:4:\"name\";s:11:\"version_log\";s:5:\"label\";s:11:\"Version Log\";s:4:\"type\";s:4:\"text\";s:5:\"notes\";s:0:\"\";s:9:\"parent_id\";i:1;s:6:\"weight\";i:1;s:7:\"options\";a:2:{s:13:\"default_value\";s:5:\"1.0.0\";s:8:\"required\";s:1:\"0\";}}i:2;a:8:{s:2:\"id\";s:1:\"2\";s:4:\"name\";s:11:\"content_log\";s:5:\"label\";s:11:\"Content Log\";s:4:\"type\";s:7:\"wysiwyg\";s:5:\"notes\";s:0:\"\";s:9:\"parent_id\";i:1;s:6:\"weight\";i:2;s:7:\"options\";a:2:{s:10:\"formatting\";s:7:\"default\";s:8:\"required\";s:1:\"0\";}}}'),
(22, 10, 'cfs_rules', 'a:1:{s:10:\"post_types\";a:2:{s:8:\"operator\";s:2:\"==\";s:6:\"values\";a:1:{i:0;s:4:\"post\";}}}'),
(23, 10, 'cfs_extras', 'a:3:{s:5:\"order\";s:1:\"0\";s:7:\"context\";s:6:\"normal\";s:11:\"hide_editor\";s:1:\"0\";}'),
(24, 9, '_wp_trash_meta_status', 'publish'),
(25, 9, '_wp_trash_meta_time', '1535944002'),
(26, 9, '_wp_desired_post_slug', 'acf_log'),
(37, 1, '_oembed_1df29c134fc64a67858f7eaeff1a5b05', '{{unknown}}'),
(42, 30, '_logy_core', 'login'),
(45, 32, '_logy_core', 'lost-password'),
(48, 34, '_logy_core', 'complete-registration'),
(58, 38, '_menu_item_type', 'post_type'),
(59, 38, '_menu_item_menu_item_parent', '77'),
(60, 38, '_menu_item_object_id', '32'),
(61, 38, '_menu_item_object', 'page'),
(62, 38, '_menu_item_target', ''),
(63, 38, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(64, 38, '_menu_item_xfn', ''),
(65, 38, '_menu_item_url', ''),
(94, 1, 'profile_views_count', '1'),
(95, 26, '_edit_lock', '1535947281:1'),
(105, 45, '_menu_item_type', 'custom'),
(106, 45, '_menu_item_menu_item_parent', '77'),
(107, 45, '_menu_item_object_id', '45'),
(108, 45, '_menu_item_object', 'custom'),
(109, 45, '_menu_item_target', ''),
(110, 45, '_menu_item_classes', 'a:2:{i:0;s:7:\"bp-menu\";i:1;s:15:\"bp-register-nav\";}'),
(111, 45, '_menu_item_xfn', ''),
(112, 45, '_menu_item_url', 'http://localhost/customp/register/'),
(114, 34, '_edit_lock', '1535947594:1'),
(124, 47, '_menu_item_type', 'custom'),
(125, 47, '_menu_item_menu_item_parent', '77'),
(126, 47, '_menu_item_object_id', '47'),
(127, 47, '_menu_item_object', 'custom'),
(128, 47, '_menu_item_target', ''),
(129, 47, '_menu_item_classes', 'a:2:{i:0;s:7:\"bp-menu\";i:1;s:13:\"bp-logout-nav\";}'),
(130, 47, '_menu_item_xfn', ''),
(131, 47, '_menu_item_url', 'http://localhost/customp/wp-login.php?action=logout&_wpnonce=250a15514b'),
(133, 1, 'profile_views_ip', '[\"::1\"]'),
(134, 2, 'profile_views_count', '1'),
(135, 2, 'profile_views_ip', '[\"::1\"]'),
(136, 48, '_edit_last', '1'),
(137, 48, '_yoast_wpseo_content_score', '60'),
(138, 48, '_edit_lock', '1535952903:1'),
(139, 51, '_edit_lock', '1536116518:1'),
(140, 51, '_edit_last', '1'),
(141, 51, '_yoast_wpseo_content_score', '30'),
(142, 51, '_bbp_last_active_time', '2018-09-03 05:52:08'),
(143, 51, '_bbp_forum_subforum_count', '0'),
(144, 51, '_bbp_reply_count', '2'),
(145, 51, '_bbp_total_reply_count', '2'),
(146, 51, '_bbp_topic_count', '1'),
(147, 51, '_bbp_total_topic_count', '1'),
(148, 51, '_bbp_topic_count_hidden', '0'),
(158, 54, '_edit_lock', '1535956301:1'),
(159, 54, '_edit_last', '1'),
(160, 54, '_bbp_activity_id', '5'),
(161, 54, '_bbp_forum_id', '51'),
(162, 54, '_bbp_topic_id', '54'),
(163, 54, '_bbp_author_ip', '::1'),
(164, 54, '_bbp_last_active_time', '2018-09-03 05:39:03'),
(165, 54, '_bbp_reply_count', '0'),
(166, 54, '_bbp_reply_count_hidden', '0'),
(167, 54, '_yoast_wpseo_content_score', '30'),
(168, 54, '_bbp_last_active_id', '54'),
(169, 54, '_bbp_voice_count', '1'),
(170, 56, '_edit_lock', '1535953184:1'),
(171, 56, '_edit_last', '1'),
(172, 56, '_bbp_activity_id', '6'),
(173, 56, '_bbp_forum_id', '51'),
(174, 56, '_bbp_topic_id', '56'),
(175, 56, '_bbp_author_ip', '::1'),
(176, 56, '_bbp_last_active_time', '2018-09-03 05:52:08'),
(177, 56, '_bbp_reply_count', '2'),
(178, 56, '_bbp_reply_count_hidden', '1'),
(179, 51, '_bbp_last_reply_id', '61'),
(180, 56, '_yoast_wpseo_content_score', '30'),
(181, 56, '_bbp_last_active_id', '61'),
(182, 56, '_bbp_voice_count', '3'),
(183, 51, '_bbp_last_topic_id', '56'),
(184, 51, '_bbp_last_active_id', '61'),
(185, 58, '_bbp_forum_id', '51'),
(186, 58, '_bbp_topic_id', '56'),
(187, 58, '_bbp_author_ip', '::1'),
(188, 56, '_bbp_last_reply_id', '61'),
(189, 58, '_bbp_activity_id', '7'),
(190, 58, '_bbp_revision_log', 'a:1:{i:59;a:2:{s:6:\"author\";i:2;s:6:\"reason\";s:0:\"\";}}'),
(191, 60, '_bbp_forum_id', '51'),
(192, 60, '_bbp_topic_id', '56'),
(193, 60, '_bbp_author_ip', '::1'),
(194, 60, '_bbp_activity_id', '8'),
(195, 61, '_bbp_forum_id', '51'),
(196, 61, '_bbp_topic_id', '56'),
(197, 61, '_bbp_reply_to', '58'),
(198, 61, '_bbp_author_ip', '::1'),
(199, 61, '_bbp_activity_id', '10'),
(200, 60, '_wp_trash_meta_status', 'publish'),
(201, 60, '_wp_trash_meta_time', '1535955729'),
(202, 60, '_wp_desired_post_slug', '60'),
(203, 63, '_edit_lock', '1535962951:1'),
(204, 63, '_edit_last', '1'),
(205, 63, '_yoast_wpseo_content_score', '30'),
(206, 63, '_bbp_last_active_time', '2018-09-03 07:46:37'),
(207, 63, '_bbp_forum_subforum_count', '0'),
(208, 63, '_bbp_reply_count', '0'),
(209, 63, '_bbp_total_reply_count', '0'),
(210, 63, '_bbp_topic_count', '0'),
(211, 63, '_bbp_total_topic_count', '0'),
(212, 63, '_bbp_topic_count_hidden', '0'),
(217, 6, 'nav_menu', 'gain-theme'),
(234, 6, 'forum_menu', 'gain-theme'),
(237, 65, '_wp_attached_file', '2018/09/logo.png'),
(238, 65, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:528;s:6:\"height\";i:135;s:4:\"file\";s:16:\"2018/09/logo.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-150x135.png\";s:5:\"width\";i:150;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"logo-300x77.png\";s:5:\"width\";i:300;s:6:\"height\";i:77;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(239, 66, '_wp_trash_meta_status', 'publish'),
(240, 66, '_wp_trash_meta_time', '1536020784'),
(254, 68, '_menu_item_type', 'post_type'),
(255, 68, '_menu_item_menu_item_parent', '0'),
(256, 68, '_menu_item_object_id', '1'),
(257, 68, '_menu_item_object', 'post'),
(258, 68, '_menu_item_target', ''),
(259, 68, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(260, 68, '_menu_item_xfn', ''),
(261, 68, '_menu_item_url', ''),
(263, 69, '_menu_item_type', 'post_type'),
(264, 69, '_menu_item_menu_item_parent', '76'),
(265, 69, '_menu_item_object_id', '6'),
(266, 69, '_menu_item_object', 'post'),
(267, 69, '_menu_item_target', ''),
(268, 69, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(269, 69, '_menu_item_xfn', ''),
(270, 69, '_menu_item_url', ''),
(272, 70, '_menu_item_type', 'post_type'),
(273, 70, '_menu_item_menu_item_parent', '76'),
(274, 70, '_menu_item_object_id', '1'),
(275, 70, '_menu_item_object', 'post'),
(276, 70, '_menu_item_target', ''),
(277, 70, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(278, 70, '_menu_item_xfn', ''),
(279, 70, '_menu_item_url', ''),
(281, 71, '_menu_item_type', 'post_type'),
(282, 71, '_menu_item_menu_item_parent', '68'),
(283, 71, '_menu_item_object_id', '6'),
(284, 71, '_menu_item_object', 'post'),
(285, 71, '_menu_item_target', ''),
(286, 71, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(287, 71, '_menu_item_xfn', ''),
(288, 71, '_menu_item_url', ''),
(290, 72, '_menu_item_type', 'post_type'),
(291, 72, '_menu_item_menu_item_parent', '68'),
(292, 72, '_menu_item_object_id', '1'),
(293, 72, '_menu_item_object', 'post'),
(294, 72, '_menu_item_target', ''),
(295, 72, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(296, 72, '_menu_item_xfn', ''),
(297, 72, '_menu_item_url', ''),
(303, 73, '_edit_lock', '1536301732:1'),
(304, 73, '_edit_last', '1'),
(307, 73, 'nav_menu', 'gain-theme'),
(308, 73, 'forum_menu', 'gain-theme'),
(309, 73, '_yoast_wpseo_content_score', '30'),
(310, 73, '_yoast_wpseo_primary_category', '2'),
(311, 75, '_wp_attached_file', '2018/09/Screenshot-from-2018-09-04-12-26-14.png'),
(312, 75, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1900;s:6:\"height\";i:983;s:4:\"file\";s:47:\"2018/09/Screenshot-from-2018-09-04-12-26-14.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:47:\"Screenshot-from-2018-09-04-12-26-14-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:47:\"Screenshot-from-2018-09-04-12-26-14-300x155.png\";s:5:\"width\";i:300;s:6:\"height\";i:155;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:47:\"Screenshot-from-2018-09-04-12-26-14-768x397.png\";s:5:\"width\";i:768;s:6:\"height\";i:397;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:48:\"Screenshot-from-2018-09-04-12-26-14-1024x530.png\";s:5:\"width\";i:1024;s:6:\"height\";i:530;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(313, 73, '_thumbnail_id', '75'),
(316, 76, '_menu_item_type', 'custom'),
(317, 76, '_menu_item_menu_item_parent', '0'),
(318, 76, '_menu_item_object_id', '76'),
(319, 76, '_menu_item_object', 'custom'),
(320, 76, '_menu_item_target', ''),
(321, 76, '_menu_item_classes', 'a:1:{i:0;s:11:\"collapse-in\";}'),
(322, 76, '_menu_item_xfn', ''),
(323, 76, '_menu_item_url', ''),
(325, 77, '_menu_item_type', 'custom'),
(326, 77, '_menu_item_menu_item_parent', '0'),
(327, 77, '_menu_item_object_id', '77'),
(328, 77, '_menu_item_object', 'custom'),
(329, 77, '_menu_item_target', ''),
(330, 77, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(331, 77, '_menu_item_xfn', ''),
(332, 77, '_menu_item_url', ''),
(334, 78, '_menu_item_type', 'custom'),
(335, 78, '_menu_item_menu_item_parent', '0'),
(336, 78, '_menu_item_object_id', '78'),
(337, 78, '_menu_item_object', 'custom'),
(338, 78, '_menu_item_target', ''),
(339, 78, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(340, 78, '_menu_item_xfn', ''),
(341, 78, '_menu_item_url', 'http://localhost'),
(342, 78, '_menu_item_orphaned', '1536049955'),
(343, 79, '_menu_item_type', 'custom'),
(344, 79, '_menu_item_menu_item_parent', '0'),
(345, 79, '_menu_item_object_id', '79'),
(346, 79, '_menu_item_object', 'custom'),
(347, 79, '_menu_item_target', ''),
(348, 79, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(349, 79, '_menu_item_xfn', ''),
(350, 79, '_menu_item_url', 'http://localhost'),
(351, 79, '_menu_item_orphaned', '1536050022'),
(352, 80, '_menu_item_type', 'custom'),
(353, 80, '_menu_item_menu_item_parent', '0'),
(354, 80, '_menu_item_object_id', '80'),
(355, 80, '_menu_item_object', 'custom'),
(356, 80, '_menu_item_target', ''),
(357, 80, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(358, 80, '_menu_item_xfn', ''),
(359, 80, '_menu_item_url', 'http://localhost'),
(360, 80, '_menu_item_orphaned', '1536050085'),
(361, 81, '_menu_item_type', 'custom'),
(362, 81, '_menu_item_menu_item_parent', '84'),
(363, 81, '_menu_item_object_id', '81'),
(364, 81, '_menu_item_object', 'custom'),
(365, 81, '_menu_item_target', ''),
(366, 81, '_menu_item_classes', 'a:1:{i:0;s:13:\"tittle-banner\";}'),
(367, 81, '_menu_item_xfn', ''),
(368, 81, '_menu_item_url', ''),
(370, 82, '_menu_item_type', 'custom'),
(371, 82, '_menu_item_menu_item_parent', '85'),
(372, 82, '_menu_item_object_id', '82'),
(373, 82, '_menu_item_object', 'custom'),
(374, 82, '_menu_item_target', ''),
(375, 82, '_menu_item_classes', 'a:1:{i:0;s:13:\"tittle-banner\";}'),
(376, 82, '_menu_item_xfn', ''),
(377, 82, '_menu_item_url', ''),
(379, 83, '_menu_item_type', 'custom'),
(380, 83, '_menu_item_menu_item_parent', '0'),
(381, 83, '_menu_item_object_id', '83'),
(382, 83, '_menu_item_object', 'custom'),
(383, 83, '_menu_item_target', ''),
(384, 83, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(385, 83, '_menu_item_xfn', ''),
(386, 83, '_menu_item_url', ''),
(388, 84, '_menu_item_type', 'custom'),
(389, 84, '_menu_item_menu_item_parent', '0'),
(390, 84, '_menu_item_object_id', '84'),
(391, 84, '_menu_item_object', 'custom'),
(392, 84, '_menu_item_target', ''),
(393, 84, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(394, 84, '_menu_item_xfn', ''),
(395, 84, '_menu_item_url', ''),
(397, 85, '_menu_item_type', 'custom'),
(398, 85, '_menu_item_menu_item_parent', '0'),
(399, 85, '_menu_item_object_id', '85'),
(400, 85, '_menu_item_object', 'custom'),
(401, 85, '_menu_item_target', ''),
(402, 85, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(403, 85, '_menu_item_xfn', ''),
(404, 85, '_menu_item_url', ''),
(415, 87, '_menu_item_type', 'post_type'),
(416, 87, '_menu_item_menu_item_parent', '83'),
(417, 87, '_menu_item_object_id', '6'),
(418, 87, '_menu_item_object', 'post'),
(419, 87, '_menu_item_target', ''),
(420, 87, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(421, 87, '_menu_item_xfn', ''),
(422, 87, '_menu_item_url', ''),
(424, 88, '_menu_item_type', 'post_type'),
(425, 88, '_menu_item_menu_item_parent', '83'),
(426, 88, '_menu_item_object_id', '1'),
(427, 88, '_menu_item_object', 'post'),
(428, 88, '_menu_item_target', ''),
(429, 88, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(430, 88, '_menu_item_xfn', ''),
(431, 88, '_menu_item_url', ''),
(433, 89, '_menu_item_type', 'custom'),
(434, 89, '_menu_item_menu_item_parent', '77'),
(435, 89, '_menu_item_object_id', '89'),
(436, 89, '_menu_item_object', 'custom'),
(437, 89, '_menu_item_target', ''),
(438, 89, '_menu_item_classes', 'a:1:{i:0;s:13:\"tittle-banner\";}'),
(439, 89, '_menu_item_xfn', ''),
(440, 89, '_menu_item_url', ''),
(451, 91, '_menu_item_type', 'post_type'),
(452, 91, '_menu_item_menu_item_parent', '84'),
(453, 91, '_menu_item_object_id', '6'),
(454, 91, '_menu_item_object', 'post'),
(455, 91, '_menu_item_target', ''),
(456, 91, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(457, 91, '_menu_item_xfn', ''),
(458, 91, '_menu_item_url', ''),
(460, 92, '_menu_item_type', 'post_type'),
(461, 92, '_menu_item_menu_item_parent', '84'),
(462, 92, '_menu_item_object_id', '1'),
(463, 92, '_menu_item_object', 'post'),
(464, 92, '_menu_item_target', ''),
(465, 92, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(466, 92, '_menu_item_xfn', ''),
(467, 92, '_menu_item_url', ''),
(469, 93, '_menu_item_type', 'post_type'),
(470, 93, '_menu_item_menu_item_parent', '85'),
(471, 93, '_menu_item_object_id', '63'),
(472, 93, '_menu_item_object', 'forum'),
(473, 93, '_menu_item_target', ''),
(474, 93, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(475, 93, '_menu_item_xfn', ''),
(476, 93, '_menu_item_url', ''),
(478, 94, '_menu_item_type', 'post_type'),
(479, 94, '_menu_item_menu_item_parent', '85'),
(480, 94, '_menu_item_object_id', '51'),
(481, 94, '_menu_item_object', 'forum'),
(482, 94, '_menu_item_target', ''),
(483, 94, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(484, 94, '_menu_item_xfn', ''),
(485, 94, '_menu_item_url', ''),
(491, 96, '_edit_lock', '1536117998:1'),
(492, 96, '_edit_last', '1'),
(495, 96, 'nav_menu', ''),
(496, 96, 'forum_menu', ''),
(497, 96, '_yoast_wpseo_content_score', '30'),
(498, 96, '_yoast_wpseo_primary_category', '2'),
(499, 98, '_edit_lock', '1536118475:1'),
(500, 98, '_edit_last', '1'),
(503, 98, 'nav_menu', ''),
(504, 98, 'forum_menu', ''),
(505, 98, '_yoast_wpseo_content_score', '60'),
(506, 98, '_yoast_wpseo_primary_category', '2'),
(534, 98, '_wp_trash_meta_status', 'publish'),
(535, 98, '_wp_trash_meta_time', '1536119087'),
(536, 98, '_wp_desired_post_slug', 'smoothie'),
(537, 96, '_wp_trash_meta_status', 'publish'),
(538, 96, '_wp_trash_meta_time', '1536119114'),
(539, 96, '_wp_desired_post_slug', 'gellato'),
(540, 103, '_menu_item_type', 'custom'),
(541, 103, '_menu_item_menu_item_parent', '0'),
(542, 103, '_menu_item_object_id', '103'),
(543, 103, '_menu_item_object', 'custom'),
(544, 103, '_menu_item_target', ''),
(545, 103, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(546, 103, '_menu_item_xfn', ''),
(547, 103, '_menu_item_url', 'http://a'),
(548, 103, '_menu_item_orphaned', '1536119829'),
(585, 108, '_menu_item_type', 'custom'),
(586, 108, '_menu_item_menu_item_parent', '0'),
(587, 108, '_menu_item_object_id', '108'),
(588, 108, '_menu_item_object', 'custom'),
(589, 108, '_menu_item_target', ''),
(590, 108, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(591, 108, '_menu_item_xfn', ''),
(592, 108, '_menu_item_url', 'http://twitter.com'),
(594, 109, '_menu_item_type', 'custom'),
(595, 109, '_menu_item_menu_item_parent', '0'),
(596, 109, '_menu_item_object_id', '109'),
(597, 109, '_menu_item_object', 'custom'),
(598, 109, '_menu_item_target', ''),
(599, 109, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(600, 109, '_menu_item_xfn', ''),
(601, 109, '_menu_item_url', 'http://instagram.com'),
(603, 110, '_menu_item_type', 'custom'),
(604, 110, '_menu_item_menu_item_parent', '0'),
(605, 110, '_menu_item_object_id', '110'),
(606, 110, '_menu_item_object', 'custom'),
(607, 110, '_menu_item_target', ''),
(608, 110, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(609, 110, '_menu_item_xfn', ''),
(610, 110, '_menu_item_url', 'http://facebook.com'),
(612, 111, '_edit_lock', '1536122802:1'),
(613, 111, '_edit_last', '1'),
(616, 111, 'nav_menu', ''),
(617, 111, 'forum_menu', ''),
(618, 111, '_yoast_wpseo_content_score', '30'),
(619, 111, '_yoast_wpseo_primary_category', '2'),
(620, 113, '_edit_last', '1'),
(623, 113, 'nav_menu', ''),
(624, 113, 'forum_menu', ''),
(625, 113, '_yoast_wpseo_content_score', '30'),
(626, 113, '_yoast_wpseo_primary_category', '2'),
(627, 113, '_edit_lock', '1536122919:1'),
(630, 113, '_wp_trash_meta_status', 'publish'),
(631, 113, '_wp_trash_meta_time', '1536123161'),
(632, 113, '_wp_desired_post_slug', 'zxcasd'),
(681, 117, '_edit_lock', '1536310668:1'),
(682, 117, '_edit_last', '1'),
(683, 117, 'field_5b921f3e09300', 'a:11:{s:3:\"key\";s:19:\"field_5b921f3e09300\";s:5:\"label\";s:9:\"Icon Post\";s:4:\"name\";s:9:\"icon_post\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:21:\"Upload Icon post here\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(685, 117, 'position', 'side'),
(686, 117, 'layout', 'no_box'),
(687, 117, 'hide_on_screen', ''),
(688, 118, '_wp_attached_file', '2018/09/icon.png'),
(689, 118, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:80;s:6:\"height\";i:80;s:4:\"file\";s:16:\"2018/09/icon.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(690, 6, 'version_log', '1.0.0'),
(691, 6, 'content_log', '<pre>- Release Theme</pre>'),
(692, 6, 'version_log', '1.1.0'),
(693, 6, 'content_log', '<pre>- release date<br />- release date<br />- release date<br />- release date<br />- release date<br />- release date<br />- release date<br />- release date<br />- release date<br />- release date<br />- release date<br /><br /></pre>'),
(694, 6, 'version_log', '1.2.0'),
(695, 6, 'content_log', '<p>LALALALALAL</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'),
(698, 119, 'icon_post', '118'),
(699, 119, '_icon_post', 'field_5b921f3e09300'),
(700, 6, 'icon_post', '118'),
(701, 6, '_icon_post', 'field_5b921f3e09300'),
(704, 117, 'rule', 'a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"post\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(705, 121, '_menu_item_type', 'custom'),
(706, 121, '_menu_item_menu_item_parent', '0'),
(707, 121, '_menu_item_object_id', '121'),
(708, 121, '_menu_item_object', 'custom'),
(709, 121, '_menu_item_target', ''),
(710, 121, '_menu_item_classes', 'a:2:{i:0;s:7:\"bp-menu\";i:1;s:12:\"bp-login-nav\";}'),
(711, 121, '_menu_item_xfn', ''),
(712, 121, '_menu_item_url', 'http://localhost/customp/wp-login.php'),
(713, 121, '_menu_item_orphaned', '1536549735'),
(714, 122, '_menu_item_type', 'custom'),
(715, 122, '_menu_item_menu_item_parent', '77'),
(716, 122, '_menu_item_object_id', '122'),
(717, 122, '_menu_item_object', 'custom'),
(718, 122, '_menu_item_target', ''),
(719, 122, '_menu_item_classes', 'a:2:{i:0;s:7:\"bp-menu\";i:1;s:12:\"bp-login-nav\";}'),
(720, 122, '_menu_item_xfn', ''),
(721, 122, '_menu_item_url', 'http://localhost/customp/login'),
(723, 73, '_wp_trash_meta_status', 'publish'),
(724, 73, '_wp_trash_meta_time', '1536551771'),
(725, 73, '_wp_desired_post_slug', 'compant-profile'),
(726, 111, '_wp_trash_meta_status', 'publish'),
(727, 111, '_wp_trash_meta_time', '1536551777'),
(728, 111, '_wp_desired_post_slug', 'zxc'),
(731, 1, '_edit_lock', '1536726523:1'),
(732, 125, '_wp_attached_file', '2018/09/alone-back-view-beautiful-674268.jpg'),
(733, 125, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:4096;s:6:\"height\";i:2726;s:4:\"file\";s:44:\"2018/09/alone-back-view-beautiful-674268.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:44:\"alone-back-view-beautiful-674268-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:44:\"alone-back-view-beautiful-674268-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:44:\"alone-back-view-beautiful-674268-768x511.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:511;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:45:\"alone-back-view-beautiful-674268-1024x682.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:682;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(734, 126, '_wp_attached_file', '2018/09/beautiful-cold-dawn-547115.jpg'),
(735, 126, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6269;s:6:\"height\";i:3055;s:4:\"file\";s:38:\"2018/09/beautiful-cold-dawn-547115.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"beautiful-cold-dawn-547115-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:38:\"beautiful-cold-dawn-547115-300x146.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:146;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:38:\"beautiful-cold-dawn-547115-768x374.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:374;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:39:\"beautiful-cold-dawn-547115-1024x499.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:499;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(736, 127, '_wp_attached_file', '2018/09/679478.jpg'),
(737, 127, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:3840;s:6:\"height\";i:2160;s:4:\"file\";s:18:\"2018/09/679478.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"679478-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"679478-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"679478-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"679478-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(738, 1, '_edit_last', '1'),
(739, 1, '_thumbnail_id', '127'),
(742, 128, 'icon_post', ''),
(743, 128, '_icon_post', 'field_5b921f3e09300'),
(744, 1, 'nav_menu', ''),
(745, 1, 'forum_menu', ''),
(746, 1, '_yoast_wpseo_content_score', '30'),
(747, 1, 'icon_post', ''),
(748, 1, '_icon_post', 'field_5b921f3e09300'),
(749, 1, '_yoast_wpseo_primary_category', '2'),
(752, 43, '_edit_lock', '1536565809:1');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-09-03 02:34:33', '2018-09-03 02:34:33', '[yuuki name=\'LMO\']', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2018-09-10 07:11:59', '2018-09-10 07:11:59', '', 0, 'http://localhost/customp/?p=1', 0, 'post', '', 1),
(2, 1, '2018-09-03 02:34:33', '2018-09-03 02:34:33', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href=\"http://localhost/customp/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2018-09-03 02:34:33', '2018-09-03 02:34:33', '', 0, 'http://localhost/customp/?page_id=2', 0, 'page', '', 0),
(3, 1, '2018-09-03 02:34:33', '2018-09-03 02:34:33', '<h2>Who we are</h2><p>Our website address is: http://localhost/customp.</p><h2>What personal data we collect and why we collect it</h2><h3>Comments</h3><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><h3>Media</h3><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><h3>Contact forms</h3><h3>Cookies</h3><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><h3>Embedded content from other websites</h3><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><h3>Analytics</h3><h2>Who we share your data with</h2><h2>How long we retain your data</h2><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><h2>What rights you have over your data</h2><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><h2>Where we send your data</h2><p>Visitor comments may be checked through an automated spam detection service.</p><h2>Your contact information</h2><h2>Additional information</h2><h3>How we protect your data</h3><h3>What data breach procedures we have in place</h3><h3>What third parties we receive data from</h3><h3>What automated decision making and/or profiling we do with user data</h3><h3>Industry regulatory disclosure requirements</h3>', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2018-09-03 02:34:33', '2018-09-03 02:34:33', '', 0, 'http://localhost/customp/?page_id=3', 0, 'page', '', 0),
(6, 1, '2018-09-03 02:55:24', '2018-09-03 02:55:24', 'First of all, we\'re very <strong>thankful</strong> for you for buying JNews theme. We hope this documentation help you to understand how this theme work.\r\n\r\nIf you find this documentation not covering your issues, please don\'t hesitate to send your question to our <a href=\"http://support.jegtheme.com/forums/forum/jnews/\">support forum</a>. We also open for suggestion to make this documentation better.\r\n\r\nAnd if you enjoy using this template and our support, please <a href=\"https://themeforest.net/downloads\">rate this theme on ThemeForest</a>.\r\n<div id=\"changelog\" class=\"doc-title-meta\"></div>', 'Gain Theme', '', 'publish', 'open', 'open', '', 'gain-theme', '', '', '2018-09-07 06:54:37', '2018-09-07 06:54:37', '', 0, 'http://localhost/customp/?p=6', 0, 'post', '', 0),
(7, 1, '2018-09-03 02:55:03', '2018-09-03 02:55:03', '', 'Screenshot from 2018-08-29 10-56-20', '', 'inherit', 'open', 'closed', '', 'screenshot-from-2018-08-29-10-56-20', '', '', '2018-09-03 02:55:03', '2018-09-03 02:55:03', '', 6, 'http://localhost/customp/wp-content/uploads/2018/09/Screenshot-from-2018-08-29-10-56-20.png', 0, 'attachment', 'image/png', 0),
(8, 1, '2018-09-03 02:55:24', '2018-09-03 02:55:24', 'First of all, we\'re very <strong>thankful</strong> for you for buying JNews theme. We hope this documentation help you to understand how this theme work.\r\n\r\nIf you find this documentation not covering your issues, please don\'t hesitate to send your question to our <a href=\"http://support.jegtheme.com/forums/forum/jnews/\">support forum</a>. We also open for suggestion to make this documentation better.\r\n\r\nAnd if you enjoy using this template and our support, please <a href=\"https://themeforest.net/downloads\">rate this theme on ThemeForest</a>.\r\n<div id=\"changelog\" class=\"doc-title-meta\"></div>', 'Gain Theme', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-09-03 02:55:24', '2018-09-03 02:55:24', '', 6, 'http://localhost/customp/theme/6-revision-v1/', 0, 'revision', '', 0),
(9, 1, '2018-09-03 03:01:40', '2018-09-03 03:01:40', '', 'Log', '', 'trash', 'closed', 'closed', '', 'acf_log__trashed', '', '', '2018-09-03 03:06:42', '2018-09-03 03:06:42', '', 0, 'http://localhost/customp/?post_type=acf&#038;p=9', 0, 'acf', '', 0),
(10, 1, '2018-09-03 03:05:50', '2018-09-03 03:05:50', '', 'Log', '', 'publish', 'closed', 'closed', '', 'log', '', '', '2018-09-03 03:08:07', '2018-09-03 03:08:07', '', 0, 'http://localhost/customp/?post_type=cfs&#038;p=10', 0, 'cfs', '', 0),
(12, 1, '2018-09-03 03:17:01', '2018-09-03 03:17:01', '', 'Activity', '', 'publish', 'closed', 'closed', '', 'activity', '', '', '2018-09-03 03:17:01', '2018-09-03 03:17:01', '', 0, 'http://localhost/customp/activity/', 0, 'page', '', 0),
(13, 1, '2018-09-03 03:17:01', '2018-09-03 03:17:01', '', 'Members', '', 'publish', 'closed', 'closed', '', 'members', '', '', '2018-09-03 03:17:01', '2018-09-03 03:17:01', '', 0, 'http://localhost/customp/members/', 0, 'page', '', 0),
(14, 1, '2018-09-03 03:17:02', '2018-09-03 03:17:02', '{{poster.name}} replied to one of your updates:\n\n<blockquote>&quot;{{usermessage}}&quot;</blockquote>\n\n<a href=\"{{{thread.url}}}\">Go to the discussion</a> to reply or catch up on the conversation.', '[{{{site.name}}}] {{poster.name}} replied to one of your updates', '{{poster.name}} replied to one of your updates:\n\n\"{{usermessage}}\"\n\nGo to the discussion to reply or catch up on the conversation: {{{thread.url}}}', 'publish', 'closed', 'closed', '', 'site-name-poster-name-replied-to-one-of-your-updates', '', '', '2018-09-03 03:17:02', '2018-09-03 03:17:02', '', 0, 'http://localhost/customp/?post_type=bp-email&p=14', 0, 'bp-email', '', 0),
(15, 1, '2018-09-03 03:17:02', '2018-09-03 03:17:02', '{{poster.name}} replied to one of your comments:\n\n<blockquote>&quot;{{usermessage}}&quot;</blockquote>\n\n<a href=\"{{{thread.url}}}\">Go to the discussion</a> to reply or catch up on the conversation.', '[{{{site.name}}}] {{poster.name}} replied to one of your comments', '{{poster.name}} replied to one of your comments:\n\n\"{{usermessage}}\"\n\nGo to the discussion to reply or catch up on the conversation: {{{thread.url}}}', 'publish', 'closed', 'closed', '', 'site-name-poster-name-replied-to-one-of-your-comments', '', '', '2018-09-03 03:17:02', '2018-09-03 03:17:02', '', 0, 'http://localhost/customp/?post_type=bp-email&p=15', 0, 'bp-email', '', 0),
(16, 1, '2018-09-03 03:17:03', '2018-09-03 03:17:03', '{{poster.name}} mentioned you in a status update:\n\n<blockquote>&quot;{{usermessage}}&quot;</blockquote>\n\n<a href=\"{{{mentioned.url}}}\">Go to the discussion</a> to reply or catch up on the conversation.', '[{{{site.name}}}] {{poster.name}} mentioned you in a status update', '{{poster.name}} mentioned you in a status update:\n\n\"{{usermessage}}\"\n\nGo to the discussion to reply or catch up on the conversation: {{{mentioned.url}}}', 'publish', 'closed', 'closed', '', 'site-name-poster-name-mentioned-you-in-a-status-update', '', '', '2018-09-03 03:17:03', '2018-09-03 03:17:03', '', 0, 'http://localhost/customp/?post_type=bp-email&p=16', 0, 'bp-email', '', 0),
(17, 1, '2018-09-03 03:17:04', '2018-09-03 03:17:04', '{{poster.name}} mentioned you in the group \"{{group.name}}\":\n\n<blockquote>&quot;{{usermessage}}&quot;</blockquote>\n\n<a href=\"{{{mentioned.url}}}\">Go to the discussion</a> to reply or catch up on the conversation.', '[{{{site.name}}}] {{poster.name}} mentioned you in an update', '{{poster.name}} mentioned you in the group \"{{group.name}}\":\n\n\"{{usermessage}}\"\n\nGo to the discussion to reply or catch up on the conversation: {{{mentioned.url}}}', 'publish', 'closed', 'closed', '', 'site-name-poster-name-mentioned-you-in-an-update', '', '', '2018-09-03 03:17:04', '2018-09-03 03:17:04', '', 0, 'http://localhost/customp/?post_type=bp-email&p=17', 0, 'bp-email', '', 0),
(18, 1, '2018-09-03 03:17:04', '2018-09-03 03:17:04', 'Thanks for registering!\n\nTo complete the activation of your account, go to the following link: <a href=\"{{{activate.url}}}\">{{{activate.url}}}</a>', '[{{{site.name}}}] Activate your account', 'Thanks for registering!\n\nTo complete the activation of your account, go to the following link: {{{activate.url}}}', 'publish', 'closed', 'closed', '', 'site-name-activate-your-account', '', '', '2018-09-03 03:17:04', '2018-09-03 03:17:04', '', 0, 'http://localhost/customp/?post_type=bp-email&p=18', 0, 'bp-email', '', 0),
(19, 1, '2018-09-03 03:17:05', '2018-09-03 03:17:05', '<a href=\"{{{initiator.url}}}\">{{initiator.name}}</a> wants to add you as a friend.\n\nTo accept this request and manage all of your pending requests, visit: <a href=\"{{{friend-requests.url}}}\">{{{friend-requests.url}}}</a>', '[{{{site.name}}}] New friendship request from {{initiator.name}}', '{{initiator.name}} wants to add you as a friend.\n\nTo accept this request and manage all of your pending requests, visit: {{{friend-requests.url}}}\n\nTo view {{initiator.name}}\'s profile, visit: {{{initiator.url}}}', 'publish', 'closed', 'closed', '', 'site-name-new-friendship-request-from-initiator-name', '', '', '2018-09-03 03:17:05', '2018-09-03 03:17:05', '', 0, 'http://localhost/customp/?post_type=bp-email&p=19', 0, 'bp-email', '', 0),
(20, 1, '2018-09-03 03:17:05', '2018-09-03 03:17:05', '<a href=\"{{{friendship.url}}}\">{{friend.name}}</a> accepted your friend request.', '[{{{site.name}}}] {{friend.name}} accepted your friendship request', '{{friend.name}} accepted your friend request.\n\nTo learn more about them, visit their profile: {{{friendship.url}}}', 'publish', 'closed', 'closed', '', 'site-name-friend-name-accepted-your-friendship-request', '', '', '2018-09-03 03:17:05', '2018-09-03 03:17:05', '', 0, 'http://localhost/customp/?post_type=bp-email&p=20', 0, 'bp-email', '', 0),
(21, 1, '2018-09-03 03:17:05', '2018-09-03 03:17:05', 'Group details for the group &quot;<a href=\"{{{group.url}}}\">{{group.name}}</a>&quot; were updated:\n<blockquote>{{changed_text}}</blockquote>', '[{{{site.name}}}] Group details updated', 'Group details for the group \"{{group.name}}\" were updated:\n\n{{changed_text}}\n\nTo view the group, visit: {{{group.url}}}', 'publish', 'closed', 'closed', '', 'site-name-group-details-updated', '', '', '2018-09-03 03:17:05', '2018-09-03 03:17:05', '', 0, 'http://localhost/customp/?post_type=bp-email&p=21', 0, 'bp-email', '', 0),
(22, 1, '2018-09-03 03:17:06', '2018-09-03 03:17:06', '<a href=\"{{{inviter.url}}}\">{{inviter.name}}</a> has invited you to join the group: &quot;{{group.name}}&quot;.\n<a href=\"{{{invites.url}}}\">Go here to accept your invitation</a> or <a href=\"{{{group.url}}}\">visit the group</a> to learn more.', '[{{{site.name}}}] You have an invitation to the group: \"{{group.name}}\"', '{{inviter.name}} has invited you to join the group: \"{{group.name}}\".\n\nTo accept your invitation, visit: {{{invites.url}}}\n\nTo learn more about the group, visit: {{{group.url}}}.\nTo view {{inviter.name}}\'s profile, visit: {{{inviter.url}}}', 'publish', 'closed', 'closed', '', 'site-name-you-have-an-invitation-to-the-group-group-name', '', '', '2018-09-03 03:17:06', '2018-09-03 03:17:06', '', 0, 'http://localhost/customp/?post_type=bp-email&p=22', 0, 'bp-email', '', 0),
(23, 1, '2018-09-03 03:17:07', '2018-09-03 03:17:07', 'You have been promoted to <b>{{promoted_to}}</b> in the group &quot;<a href=\"{{{group.url}}}\">{{group.name}}</a>&quot;.', '[{{{site.name}}}] You have been promoted in the group: \"{{group.name}}\"', 'You have been promoted to {{promoted_to}} in the group: \"{{group.name}}\".\n\nTo visit the group, go to: {{{group.url}}}', 'publish', 'closed', 'closed', '', 'site-name-you-have-been-promoted-in-the-group-group-name', '', '', '2018-09-03 03:17:07', '2018-09-03 03:17:07', '', 0, 'http://localhost/customp/?post_type=bp-email&p=23', 0, 'bp-email', '', 0),
(24, 1, '2018-09-03 03:17:07', '2018-09-03 03:17:07', '<a href=\"{{{profile.url}}}\">{{requesting-user.name}}</a> wants to join the group &quot;{{group.name}}&quot;. As you are an administrator of this group, you must either accept or reject the membership request.\n\n<a href=\"{{{group-requests.url}}}\">Go here to manage this</a> and all other pending requests.', '[{{{site.name}}}] Membership request for group: {{group.name}}', '{{requesting-user.name}} wants to join the group \"{{group.name}}\". As you are the administrator of this group, you must either accept or reject the membership request.\n\nTo manage this and all other pending requests, visit: {{{group-requests.url}}}\n\nTo view {{requesting-user.name}}\'s profile, visit: {{{profile.url}}}', 'publish', 'closed', 'closed', '', 'site-name-membership-request-for-group-group-name', '', '', '2018-09-03 03:17:07', '2018-09-03 03:17:07', '', 0, 'http://localhost/customp/?post_type=bp-email&p=24', 0, 'bp-email', '', 0),
(25, 1, '2018-09-03 03:17:08', '2018-09-03 03:17:08', '{{sender.name}} sent you a new message: &quot;{{usersubject}}&quot;\n\n<blockquote>&quot;{{usermessage}}&quot;</blockquote>\n\n<a href=\"{{{message.url}}}\">Go to the discussion</a> to reply or catch up on the conversation.', '[{{{site.name}}}] New message from {{sender.name}}', '{{sender.name}} sent you a new message: \"{{usersubject}}\"\n\n\"{{usermessage}}\"\n\nGo to the discussion to reply or catch up on the conversation: {{{message.url}}}', 'publish', 'closed', 'closed', '', 'site-name-new-message-from-sender-name', '', '', '2018-09-03 03:17:08', '2018-09-03 03:17:08', '', 0, 'http://localhost/customp/?post_type=bp-email&p=25', 0, 'bp-email', '', 0),
(26, 1, '2018-09-03 03:17:08', '2018-09-03 03:17:08', 'You recently changed the email address associated with your account on {{site.name}} to {{user.email}}. If this is correct, <a href=\"{{{verify.url}}}\">go here to confirm the change</a>.\n\nOtherwise, you can safely ignore and delete this email if you have changed your mind, or if you think you have received this email in error.', '[{{{site.name}}}] Verify your new email address', 'You recently changed the email address associated with your account on {{site.name}} to {{user.email}}. If this is correct, go to the following link to confirm the change: {{{verify.url}}}\n\nOtherwise, you can safely ignore and delete this email if you have changed your mind, or if you think you have received this email in error.', 'publish', 'closed', 'closed', '', 'site-name-verify-your-new-email-address', '', '', '2018-09-03 03:17:08', '2018-09-03 03:17:08', '', 0, 'http://localhost/customp/?post_type=bp-email&p=26', 0, 'bp-email', '', 0),
(27, 1, '2018-09-03 03:17:09', '2018-09-03 03:17:09', 'Your membership request for the group &quot;<a href=\"{{{group.url}}}\">{{group.name}}</a>&quot; has been accepted.', '[{{{site.name}}}] Membership request for group \"{{group.name}}\" accepted', 'Your membership request for the group \"{{group.name}}\" has been accepted.\n\nTo view the group, visit: {{{group.url}}}', 'publish', 'closed', 'closed', '', 'site-name-membership-request-for-group-group-name-accepted', '', '', '2018-09-03 03:17:09', '2018-09-03 03:17:09', '', 0, 'http://localhost/customp/?post_type=bp-email&p=27', 0, 'bp-email', '', 0),
(28, 1, '2018-09-03 03:17:09', '2018-09-03 03:17:09', 'Your membership request for the group &quot;<a href=\"{{{group.url}}}\">{{group.name}}</a>&quot; has been rejected.', '[{{{site.name}}}] Membership request for group \"{{group.name}}\" rejected', 'Your membership request for the group \"{{group.name}}\" has been rejected.\n\nTo request membership again, visit: {{{group.url}}}', 'publish', 'closed', 'closed', '', 'site-name-membership-request-for-group-group-name-rejected', '', '', '2018-09-03 03:17:09', '2018-09-03 03:17:09', '', 0, 'http://localhost/customp/?post_type=bp-email&p=28', 0, 'bp-email', '', 0),
(30, 1, '2018-09-03 03:20:22', '2018-09-03 03:20:22', '', 'Login', '', 'publish', 'closed', 'open', '', 'login', '', '', '2018-09-03 03:20:22', '2018-09-03 03:20:22', '', 0, 'http://localhost/customp/theme/login/', 0, 'page', '', 0),
(31, 1, '2018-09-03 03:20:22', '2018-09-03 03:20:22', '', 'Login', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2018-09-03 03:20:22', '2018-09-03 03:20:22', '', 30, 'http://localhost/customp/theme/30-revision-v1/', 0, 'revision', '', 0),
(32, 1, '2018-09-03 03:20:23', '2018-09-03 03:20:23', '', 'Password Reset', '', 'publish', 'closed', 'open', '', 'lost-password', '', '', '2018-09-03 03:20:23', '2018-09-03 03:20:23', '', 0, 'http://localhost/customp/theme/lost-password/', 0, 'page', '', 0),
(33, 1, '2018-09-03 03:20:23', '2018-09-03 03:20:23', '', 'Password Reset', '', 'inherit', 'closed', 'closed', '', '32-revision-v1', '', '', '2018-09-03 03:20:23', '2018-09-03 03:20:23', '', 32, 'http://localhost/customp/theme/32-revision-v1/', 0, 'revision', '', 0),
(34, 1, '2018-09-03 03:20:24', '2018-09-03 03:20:24', '', 'Complete Registration', '', 'publish', 'closed', 'open', '', 'complete-registration', '', '', '2018-09-03 03:20:24', '2018-09-03 03:20:24', '', 0, 'http://localhost/customp/theme/complete-registration/', 0, 'page', '', 0),
(35, 1, '2018-09-03 03:20:24', '2018-09-03 03:20:24', '', 'Complete Registration', '', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2018-09-03 03:20:24', '2018-09-03 03:20:24', '', 34, 'http://localhost/customp/theme/34-revision-v1/', 0, 'revision', '', 0),
(36, 1, '2018-09-03 03:20:25', '2018-09-03 03:20:25', 'we got a request to reset your account password. If you made this request, visit the following link To reset your password: <a href=\"{{password.reset.url}}\">{{password.reset.url}}</a>\n\n If this was a mistake, just ignore this email and nothing will happen.', '[{{{site.name}}}] Reset Your Account Password', 'we got a request to reset your account password. If you made this request, visit the following link To reset your password: {{password.reset.url}}\n\n If this was a mistake, just ignore this email and nothing will happen.', 'publish', 'closed', 'closed', '', 'site-name-reset-your-account-password', '', '', '2018-09-03 03:20:25', '2018-09-03 03:20:25', '', 0, 'http://localhost/customp/?post_type=bp-email&p=36', 0, 'bp-email', '', 0),
(38, 1, '2018-09-03 03:25:29', '2018-09-03 03:25:29', ' ', '', '', 'publish', 'closed', 'closed', '', '38', '', '', '2018-09-12 02:51:59', '2018-09-12 02:51:59', '', 0, 'http://localhost/customp/?p=38', 5, 'nav_menu_item', '', 0),
(42, 1, '2018-09-03 03:36:21', '2018-09-03 03:36:21', '', 'Register', '', 'publish', 'closed', 'closed', '', 'register', '', '', '2018-09-03 03:36:21', '2018-09-03 03:36:21', '', 0, 'http://localhost/customp/register/', 0, 'page', '', 0),
(43, 1, '2018-09-03 03:36:21', '2018-09-03 03:36:21', '', 'Activate', '', 'publish', 'closed', 'closed', '', 'activate', '', '', '2018-09-03 03:36:21', '2018-09-03 03:36:21', '', 0, 'http://localhost/customp/activate/', 0, 'page', '', 0),
(45, 1, '2018-09-03 03:48:18', '2018-09-03 03:48:18', '', 'Register', '', 'publish', 'closed', 'closed', '', 'register', '', '', '2018-09-12 02:51:59', '2018-09-12 02:51:59', '', 0, 'http://localhost/customp/?p=45', 4, 'nav_menu_item', '', 0),
(47, 1, '2018-09-03 04:04:44', '2018-09-03 04:04:44', '', 'Log Out', '', 'publish', 'closed', 'closed', '', 'log-out', '', '', '2018-09-12 02:51:59', '2018-09-12 02:51:59', '', 0, 'http://localhost/customp/?p=47', 6, 'nav_menu_item', '', 0),
(48, 1, '2018-09-03 05:32:47', '2018-09-03 05:32:47', '[bbp-forum-index]', 'Forum', '', 'publish', 'closed', 'closed', '', 'forum', '', '', '2018-09-03 05:34:33', '2018-09-03 05:34:33', '', 0, 'http://localhost/customp/?page_id=48', 0, 'page', '', 0),
(49, 1, '2018-09-03 05:32:47', '2018-09-03 05:32:47', '', 'Forum', '', 'inherit', 'closed', 'closed', '', '48-revision-v1', '', '', '2018-09-03 05:32:47', '2018-09-03 05:32:47', '', 48, 'http://localhost/customp/48-revision-v1/', 0, 'revision', '', 0),
(50, 1, '2018-09-03 05:34:33', '2018-09-03 05:34:33', '[bbp-forum-index]', 'Forum', '', 'inherit', 'closed', 'closed', '', '48-revision-v1', '', '', '2018-09-03 05:34:33', '2018-09-03 05:34:33', '', 48, 'http://localhost/customp/48-revision-v1/', 0, 'revision', '', 0),
(51, 1, '2018-09-03 05:36:38', '2018-09-03 05:36:38', '', 'Gain Theme', '', 'publish', 'closed', 'open', '', 'gain-theme', '', '', '2018-09-03 05:36:38', '2018-09-03 05:36:38', '', 0, 'http://localhost/customp/?post_type=forum&#038;p=51', 0, 'forum', '', 0),
(52, 1, '2018-09-03 05:36:38', '2018-09-03 05:36:38', '', 'Gain Theme', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2018-09-03 05:36:38', '2018-09-03 05:36:38', '', 51, 'http://localhost/customp/theme/51-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2018-09-03 05:39:02', '2018-09-03 05:39:02', '', 'FAQ', '', 'publish', 'closed', 'open', '', 'faq', '', '', '2018-09-03 05:40:49', '2018-09-03 05:40:49', '', 51, 'http://localhost/customp/?post_type=topic&#038;p=54', 0, 'topic', '', 0),
(55, 1, '2018-09-03 05:39:02', '2018-09-03 05:39:02', '', 'FAQ', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2018-09-03 05:39:02', '2018-09-03 05:39:02', '', 54, 'http://localhost/customp/theme/54-revision-v1/', 0, 'revision', '', 0),
(56, 1, '2018-09-03 05:39:40', '2018-09-03 05:39:40', '', 'Customize Gain Theme', '', 'publish', 'closed', 'open', '', 'customize-gain-theme', '', '', '2018-09-03 05:39:40', '2018-09-03 05:39:40', '', 51, 'http://localhost/customp/?post_type=topic&#038;p=56', 0, 'topic', '', 0),
(57, 1, '2018-09-03 05:39:40', '2018-09-03 05:39:40', '', 'Customize Gain Theme', '', 'inherit', 'closed', 'closed', '', '56-revision-v1', '', '', '2018-09-03 05:39:40', '2018-09-03 05:39:40', '', 56, 'http://localhost/customp/theme/56-revision-v1/', 0, 'revision', '', 0),
(58, 2, '2018-09-03 05:45:16', '2018-09-03 05:45:16', 'Dik sakedik sakedik', '', '', 'publish', 'closed', 'closed', '', '58', '', '', '2018-09-03 05:45:37', '2018-09-03 05:45:37', '', 56, 'http://localhost/customp/forums/reply/58/', 1, 'reply', '', 0),
(59, 2, '2018-09-03 05:45:37', '2018-09-03 05:45:37', 'Dik sakedik sakedik', '', '', 'inherit', 'closed', 'closed', '', '58-revision-v1', '', '', '2018-09-03 05:45:37', '2018-09-03 05:45:37', '', 58, 'http://localhost/customp/theme/58-revision-v1/', 0, 'revision', '', 0),
(60, 2, '2018-09-03 05:46:03', '2018-09-03 05:46:03', 'Hello World', '', '', 'trash', 'closed', 'closed', '', '60__trashed', '', '', '2018-09-03 06:22:09', '2018-09-03 06:22:09', '', 56, 'http://localhost/customp/forums/reply/60/', 2, 'reply', '', 0),
(61, 3, '2018-09-03 05:52:08', '2018-09-03 05:52:08', 'Lorem ipsum Dolor Sit Amet', '', '', 'publish', 'closed', 'closed', '', '61', '', '', '2018-09-03 05:52:08', '2018-09-03 05:52:08', '', 56, 'http://localhost/customp/forums/reply/61/', 3, 'reply', '', 0),
(62, 1, '2018-09-03 06:22:09', '2018-09-03 06:22:09', 'Hello World', '', '', 'inherit', 'closed', 'closed', '', '60-revision-v1', '', '', '2018-09-03 06:22:09', '2018-09-03 06:22:09', '', 60, 'http://localhost/customp/theme/60-revision-v1/', 0, 'revision', '', 0),
(63, 1, '2018-09-03 07:46:57', '2018-09-03 07:46:57', '', 'Gain Theme2', '', 'publish', 'closed', 'open', '', 'gain-theme2', '', '', '2018-09-03 07:46:57', '2018-09-03 07:46:57', '', 0, 'http://localhost/customp/?post_type=forum&#038;p=63', 0, 'forum', '', 0),
(64, 1, '2018-09-03 07:46:57', '2018-09-03 07:46:57', '', 'Gain Theme2', '', 'inherit', 'closed', 'closed', '', '63-revision-v1', '', '', '2018-09-03 07:46:57', '2018-09-03 07:46:57', '', 63, 'http://localhost/customp/theme/63-revision-v1/', 0, 'revision', '', 0),
(65, 1, '2018-09-04 00:24:00', '2018-09-04 00:24:00', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2018-09-04 00:24:00', '2018-09-04 00:24:00', '', 0, 'http://localhost/customp/wp-content/uploads/2018/09/logo.png', 0, 'attachment', 'image/png', 0),
(66, 1, '2018-09-04 00:26:24', '2018-09-04 00:26:24', '{\n    \"suport-theme::header_textcolor\": {\n        \"value\": \"blank\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-09-04 00:26:24\"\n    },\n    \"suport-theme::nav_menu_locations[menu-1]\": {\n        \"value\": 0,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-09-04 00:26:24\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '94f3f182-95fd-49f4-a608-8f74b7d1ae35', '', '', '2018-09-04 00:26:24', '2018-09-04 00:26:24', '', 0, 'http://localhost/customp/theme/94f3f182-95fd-49f4-a608-8f74b7d1ae35/', 0, 'customize_changeset', '', 0),
(68, 1, '2018-09-04 04:15:35', '2018-09-04 04:15:35', ' ', '', '', 'publish', 'closed', 'closed', '', '68', '', '', '2018-09-04 06:55:16', '2018-09-04 06:55:16', '', 0, 'http://localhost/customp/?p=68', 4, 'nav_menu_item', '', 0),
(69, 1, '2018-09-04 04:15:34', '2018-09-04 04:15:34', ' ', '', '', 'publish', 'closed', 'closed', '', '69', '', '', '2018-09-04 06:55:16', '2018-09-04 06:55:16', '', 0, 'http://localhost/customp/?p=69', 2, 'nav_menu_item', '', 0),
(70, 1, '2018-09-04 04:15:35', '2018-09-04 04:15:35', ' ', '', '', 'publish', 'closed', 'closed', '', '70', '', '', '2018-09-04 06:55:16', '2018-09-04 06:55:16', '', 0, 'http://localhost/customp/?p=70', 3, 'nav_menu_item', '', 0),
(71, 1, '2018-09-04 04:15:36', '2018-09-04 04:15:36', ' ', '', '', 'publish', 'closed', 'closed', '', '71', '', '', '2018-09-04 06:55:16', '2018-09-04 06:55:16', '', 0, 'http://localhost/customp/?p=71', 5, 'nav_menu_item', '', 0),
(72, 1, '2018-09-04 04:15:36', '2018-09-04 04:15:36', ' ', '', '', 'publish', 'closed', 'closed', '', '72', '', '', '2018-09-04 06:55:16', '2018-09-04 06:55:16', '', 0, 'http://localhost/customp/?p=72', 6, 'nav_menu_item', '', 0),
(73, 1, '2018-09-04 04:20:00', '2018-09-04 04:20:00', '', 'Company Profile', '', 'trash', 'open', 'open', '', 'compant-profile__trashed', '', '', '2018-09-10 03:56:11', '2018-09-10 03:56:11', '', 0, 'http://localhost/customp/?p=73', 0, 'post', '', 0),
(74, 1, '2018-09-04 04:20:00', '2018-09-04 04:20:00', '', 'Compant Profile', '', 'inherit', 'closed', 'closed', '', '73-revision-v1', '', '', '2018-09-04 04:20:00', '2018-09-04 04:20:00', '', 73, 'http://localhost/customp/theme/73-revision-v1/', 0, 'revision', '', 0),
(75, 1, '2018-09-04 04:26:50', '2018-09-04 04:26:50', '', 'Screenshot from 2018-09-04 12-26-14', '', 'inherit', 'open', 'closed', '', 'screenshot-from-2018-09-04-12-26-14', '', '', '2018-09-04 04:26:50', '2018-09-04 04:26:50', '', 73, 'http://localhost/customp/wp-content/uploads/2018/09/Screenshot-from-2018-09-04-12-26-14.png', 0, 'attachment', 'image/png', 0),
(76, 1, '2018-09-04 06:51:11', '2018-09-04 06:51:11', '', 'WordPress Instalation', '', 'publish', 'closed', 'closed', '', 'wordpress-instalation', '', '', '2018-09-04 06:55:16', '2018-09-04 06:55:16', '', 0, 'http://localhost/customp/?p=76', 1, 'nav_menu_item', '', 0),
(77, 1, '2018-09-04 07:38:51', '2018-09-04 07:38:51', '', '<button> <i class=\"fa fa-lock\"></i> </button>', '', 'publish', 'closed', 'closed', '', 'account', '', '', '2018-09-12 02:51:59', '2018-09-12 02:51:59', '', 0, 'http://localhost/customp/?p=77', 1, 'nav_menu_item', '', 0),
(78, 1, '2018-09-04 08:32:21', '0000-00-00 00:00:00', '', 'Account', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-09-04 08:32:21', '0000-00-00 00:00:00', '', 0, 'http://localhost/customp/?p=78', 1, 'nav_menu_item', '', 0),
(79, 1, '2018-09-04 08:33:41', '0000-00-00 00:00:00', '', 'Documentation', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-09-04 08:33:41', '0000-00-00 00:00:00', '', 0, 'http://localhost/customp/?p=79', 1, 'nav_menu_item', '', 0),
(80, 1, '2018-09-04 08:34:44', '0000-00-00 00:00:00', '', '<button> Doc </button>', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-09-04 08:34:44', '0000-00-00 00:00:00', '', 0, 'http://localhost/customp/?p=80', 1, 'nav_menu_item', '', 0),
(81, 1, '2018-09-05 01:58:34', '2018-09-05 01:58:34', '', 'Theme', '', 'publish', 'closed', 'closed', '', 'theme', '', '', '2018-09-12 02:51:59', '2018-09-12 02:51:59', '', 0, 'http://localhost/customp/?p=81', 8, 'nav_menu_item', '', 0),
(82, 1, '2018-09-05 01:58:35', '2018-09-05 01:58:35', '', 'Forum', '', 'publish', 'closed', 'closed', '', 'forum', '', '', '2018-09-12 02:52:00', '2018-09-12 02:52:00', '', 0, 'http://localhost/customp/?p=82', 12, 'nav_menu_item', '', 0),
(83, 1, '2018-09-05 01:58:35', '2018-09-05 01:58:35', '', '<button> <i class=\"fa fa-bars\"></i> </button>', '', 'publish', 'closed', 'closed', '', 'lala-3', '', '', '2018-09-12 02:52:00', '2018-09-12 02:52:00', '', 0, 'http://localhost/customp/?p=83', 15, 'nav_menu_item', '', 0),
(84, 1, '2018-09-05 01:58:34', '2018-09-05 01:58:34', '', '<button> <i class=\"fa fa-file\"></i> </button>', '', 'publish', 'closed', 'closed', '', 'lala', '', '', '2018-09-12 02:51:59', '2018-09-12 02:51:59', '', 0, 'http://localhost/customp/?p=84', 7, 'nav_menu_item', '', 0),
(85, 1, '2018-09-05 01:58:34', '2018-09-05 01:58:34', '', '<button> <i class=\"fa fa-life-ring\"></i> </button>', '', 'publish', 'closed', 'closed', '', 'lala-2', '', '', '2018-09-12 02:52:00', '2018-09-12 02:52:00', '', 0, 'http://localhost/customp/?p=85', 11, 'nav_menu_item', '', 0),
(87, 1, '2018-09-05 02:00:09', '2018-09-05 02:00:09', ' ', '', '', 'publish', 'closed', 'closed', '', '87', '', '', '2018-09-12 02:52:00', '2018-09-12 02:52:00', '', 0, 'http://localhost/customp/?p=87', 16, 'nav_menu_item', '', 0),
(88, 1, '2018-09-05 02:00:09', '2018-09-05 02:00:09', ' ', '', '', 'publish', 'closed', 'closed', '', '88', '', '', '2018-09-12 02:52:00', '2018-09-12 02:52:00', '', 0, 'http://localhost/customp/?p=88', 17, 'nav_menu_item', '', 0),
(89, 1, '2018-09-05 02:22:18', '2018-09-05 02:22:18', '', 'Account', '', 'publish', 'closed', 'closed', '', 'account-2', '', '', '2018-09-12 02:51:59', '2018-09-12 02:51:59', '', 0, 'http://localhost/customp/?p=89', 2, 'nav_menu_item', '', 0),
(91, 1, '2018-09-05 02:22:20', '2018-09-05 02:22:20', ' ', '', '', 'publish', 'closed', 'closed', '', '91', '', '', '2018-09-12 02:51:59', '2018-09-12 02:51:59', '', 0, 'http://localhost/customp/?p=91', 9, 'nav_menu_item', '', 0),
(92, 1, '2018-09-05 02:22:20', '2018-09-05 02:22:20', ' ', '', '', 'publish', 'closed', 'closed', '', '92', '', '', '2018-09-12 02:52:00', '2018-09-12 02:52:00', '', 0, 'http://localhost/customp/?p=92', 10, 'nav_menu_item', '', 0),
(93, 1, '2018-09-05 02:22:21', '2018-09-05 02:22:21', ' ', '', '', 'publish', 'closed', 'closed', '', '93', '', '', '2018-09-12 02:52:00', '2018-09-12 02:52:00', '', 0, 'http://localhost/customp/?p=93', 14, 'nav_menu_item', '', 0),
(94, 1, '2018-09-05 02:22:21', '2018-09-05 02:22:21', ' ', '', '', 'publish', 'closed', 'closed', '', '94', '', '', '2018-09-12 02:52:00', '2018-09-12 02:52:00', '', 0, 'http://localhost/customp/?p=94', 13, 'nav_menu_item', '', 0),
(95, 1, '2018-09-05 03:15:34', '2018-09-05 03:15:34', '', 'Company Profile', '', 'inherit', 'closed', 'closed', '', '73-revision-v1', '', '', '2018-09-05 03:15:34', '2018-09-05 03:15:34', '', 73, 'http://localhost/customp/theme/73-revision-v1/', 0, 'revision', '', 0),
(96, 1, '2018-09-05 03:28:25', '2018-09-05 03:28:25', 'lalala', 'Gellato', '', 'trash', 'open', 'open', '', 'gellato__trashed', '', '', '2018-09-05 03:45:14', '2018-09-05 03:45:14', '', 0, 'http://localhost/customp/?p=96', 0, 'post', '', 0),
(97, 1, '2018-09-05 03:28:25', '2018-09-05 03:28:25', 'lalala', 'Gellato', '', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2018-09-05 03:28:25', '2018-09-05 03:28:25', '', 96, 'http://localhost/customp/theme/96-revision-v1/', 0, 'revision', '', 0),
(98, 1, '2018-09-05 03:29:17', '2018-09-05 03:29:17', 'LMAO', 'Smoothie', '', 'trash', 'open', 'open', '', 'smoothie__trashed', '', '', '2018-09-05 03:44:47', '2018-09-05 03:44:47', '', 0, 'http://localhost/customp/?p=98', 0, 'post', '', 0),
(99, 1, '2018-09-05 03:29:17', '2018-09-05 03:29:17', 'LMAO', 'Smoothie', '', 'inherit', 'closed', 'closed', '', '98-revision-v1', '', '', '2018-09-05 03:29:17', '2018-09-05 03:29:17', '', 98, 'http://localhost/customp/theme/98-revision-v1/', 0, 'revision', '', 0),
(103, 1, '2018-09-05 03:57:09', '0000-00-00 00:00:00', '', 'inst', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-09-05 03:57:09', '0000-00-00 00:00:00', '', 0, 'http://localhost/customp/?p=103', 1, 'nav_menu_item', '', 0),
(108, 1, '2018-09-05 04:01:42', '2018-09-05 04:01:42', '', '<i class=\"fa fa-twitter\"></i>', '', 'publish', 'closed', 'closed', '', '108', '', '', '2018-09-05 04:16:43', '2018-09-05 04:16:43', '', 0, 'http://localhost/customp/?p=108', 2, 'nav_menu_item', '', 0),
(109, 1, '2018-09-05 04:01:42', '2018-09-05 04:01:42', '', '<i class=\"fa fa-instagram\"></i>', '', 'publish', 'closed', 'closed', '', '109', '', '', '2018-09-05 04:16:43', '2018-09-05 04:16:43', '', 0, 'http://localhost/customp/?p=109', 3, 'nav_menu_item', '', 0),
(110, 1, '2018-09-05 04:03:37', '2018-09-05 04:03:37', '', '<i class=\"fa fa-facebook-f\"></i>', '', 'publish', 'closed', 'closed', '', 'facebook', '', '', '2018-09-05 04:16:43', '2018-09-05 04:16:43', '', 0, 'http://localhost/customp/?p=110', 1, 'nav_menu_item', '', 0),
(111, 1, '2018-09-05 04:49:00', '2018-09-05 04:49:00', '', 'zxc', '', 'trash', 'open', 'open', '', 'zxc__trashed', '', '', '2018-09-10 03:56:17', '2018-09-10 03:56:17', '', 0, 'http://localhost/customp/?p=111', 0, 'post', '', 0),
(112, 1, '2018-09-05 04:49:00', '2018-09-05 04:49:00', '', 'zxc', '', 'inherit', 'closed', 'closed', '', '111-revision-v1', '', '', '2018-09-05 04:49:00', '2018-09-05 04:49:00', '', 111, 'http://localhost/customp/theme/111-revision-v1/', 0, 'revision', '', 0),
(113, 1, '2018-09-05 04:49:13', '2018-09-05 04:49:13', '', 'zxcasd', '', 'trash', 'open', 'open', '', 'zxcasd__trashed', '', '', '2018-09-05 04:52:41', '2018-09-05 04:52:41', '', 0, 'http://localhost/customp/?p=113', 0, 'post', '', 0),
(114, 1, '2018-09-05 04:49:13', '2018-09-05 04:49:13', '', 'zxcasd', '', 'inherit', 'closed', 'closed', '', '113-revision-v1', '', '', '2018-09-05 04:49:13', '2018-09-05 04:49:13', '', 113, 'http://localhost/customp/theme/113-revision-v1/', 0, 'revision', '', 0),
(115, 1, '2018-09-05 05:57:15', '2018-09-05 05:57:15', 'First of all, we\'re very <strong>thankful</strong> for you for buying JNews theme. We hope this documentation help you to understand how this theme work.\r\n\r\nIf you find this documentation not covering your issues, please don\'t hesitate to send your question to our <a href=\"http://support.jegtheme.com/forums/forum/jnews/\">support forum</a>. We also open for suggestion to make this documentation better.\r\n\r\nAnd if you enjoy using this template and our support, please <a href=\"https://themeforest.net/downloads\">rate this theme on ThemeForest</a>.\r\n<div id=\"changelog\" class=\"doc-title-meta\"></div>', 'Gain Theme', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-09-05 05:57:15', '2018-09-05 05:57:15', '', 6, 'http://localhost/customp/theme/6-revision-v1/', 0, 'revision', '', 0),
(116, 1, '2018-09-05 06:00:59', '2018-09-05 06:00:59', 'First of all, we\'re very <strong>thankful</strong> for you for buying JNews theme. We hope this documentation help you to understand how this theme work.\r\n\r\nIf you find this documentation not covering your issues, please don\'t hesitate to send your question to our <a href=\"http://support.jegtheme.com/forums/forum/jnews/\">support forum</a>. We also open for suggestion to make this documentation better.\r\n\r\nAnd if you enjoy using this template and our support, please <a href=\"https://themeforest.net/downloads\">rate this theme on ThemeForest</a>.\r\n<div id=\"changelog\" class=\"doc-title-meta\"></div>', 'Gain Theme', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-09-05 06:00:59', '2018-09-05 06:00:59', '', 6, 'http://localhost/customp/theme/6-revision-v1/', 0, 'revision', '', 0),
(117, 1, '2018-09-07 06:50:47', '2018-09-07 06:50:47', '', 'Icon Post', '', 'publish', 'closed', 'closed', '', 'acf_icon-post', '', '', '2018-09-07 06:58:04', '2018-09-07 06:58:04', '', 0, 'http://localhost/customp/?post_type=acf&#038;p=117', 100, 'acf', '', 0),
(118, 1, '2018-09-07 06:54:17', '2018-09-07 06:54:17', '', 'icon', '', 'inherit', 'open', 'closed', '', 'icon', '', '', '2018-09-07 06:54:17', '2018-09-07 06:54:17', '', 6, 'http://localhost/customp/wp-content/uploads/2018/09/icon.png', 0, 'attachment', 'image/png', 0),
(119, 1, '2018-09-07 06:54:37', '2018-09-07 06:54:37', 'First of all, we\'re very <strong>thankful</strong> for you for buying JNews theme. We hope this documentation help you to understand how this theme work.\r\n\r\nIf you find this documentation not covering your issues, please don\'t hesitate to send your question to our <a href=\"http://support.jegtheme.com/forums/forum/jnews/\">support forum</a>. We also open for suggestion to make this documentation better.\r\n\r\nAnd if you enjoy using this template and our support, please <a href=\"https://themeforest.net/downloads\">rate this theme on ThemeForest</a>.\r\n<div id=\"changelog\" class=\"doc-title-meta\"></div>', 'Gain Theme', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-09-07 06:54:37', '2018-09-07 06:54:37', '', 6, 'http://localhost/customp/theme/6-revision-v1/', 0, 'revision', '', 0),
(120, 1, '2018-09-10 03:20:42', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-09-10 03:20:42', '0000-00-00 00:00:00', '', 0, 'http://localhost/customp/?p=120', 0, 'post', '', 0),
(121, 1, '2018-09-10 03:22:15', '0000-00-00 00:00:00', '', 'Log In', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-09-10 03:22:15', '0000-00-00 00:00:00', '', 0, 'http://localhost/customp/?p=121', 1, 'nav_menu_item', '', 0),
(122, 1, '2018-09-10 03:24:48', '2018-09-10 03:24:48', '', 'Login', '', 'publish', 'closed', 'closed', '', 'login', '', '', '2018-09-12 02:51:59', '2018-09-12 02:51:59', '', 0, 'http://localhost/customp/?p=122', 3, 'nav_menu_item', '', 0),
(124, 1, '2018-09-10 07:10:55', '2018-09-10 07:10:55', '[yuuki name=\'LMO\']', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-autosave-v1', '', '', '2018-09-10 07:10:55', '2018-09-10 07:10:55', '', 1, 'http://localhost/customp/theme/1-autosave-v1/', 0, 'revision', '', 0),
(125, 1, '2018-09-10 07:10:56', '2018-09-10 07:10:56', '', 'alone-back-view-beautiful-674268', '', 'inherit', 'open', 'closed', '', 'alone-back-view-beautiful-674268', '', '', '2018-09-10 07:10:56', '2018-09-10 07:10:56', '', 1, 'http://localhost/customp/wp-content/uploads/2018/09/alone-back-view-beautiful-674268.jpg', 0, 'attachment', 'image/jpeg', 0),
(126, 1, '2018-09-10 07:10:59', '2018-09-10 07:10:59', '', 'beautiful-cold-dawn-547115', '', 'inherit', 'open', 'closed', '', 'beautiful-cold-dawn-547115', '', '', '2018-09-10 07:10:59', '2018-09-10 07:10:59', '', 1, 'http://localhost/customp/wp-content/uploads/2018/09/beautiful-cold-dawn-547115.jpg', 0, 'attachment', 'image/jpeg', 0),
(127, 1, '2018-09-10 07:11:03', '2018-09-10 07:11:03', '', '679478', '', 'inherit', 'open', 'closed', '', '679478', '', '', '2018-09-10 07:11:03', '2018-09-10 07:11:03', '', 1, 'http://localhost/customp/wp-content/uploads/2018/09/679478.jpg', 0, 'attachment', 'image/jpeg', 0),
(128, 1, '2018-09-10 07:11:33', '2018-09-10 07:11:33', '[yuuki name=\'LMO\']', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2018-09-10 07:11:33', '2018-09-10 07:11:33', '', 1, 'http://localhost/customp/theme/1-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_signups`
--

CREATE TABLE `wp_signups` (
  `signup_id` bigint(20) NOT NULL,
  `domain` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `activation_key` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `meta` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_signups`
--

INSERT INTO `wp_signups` (`signup_id`, `domain`, `path`, `title`, `user_login`, `user_email`, `registered`, `activated`, `active`, `activation_key`, `meta`) VALUES
(1, '', '', '', 'cebongcaper', 'lonazukii@gmail.com', '2018-09-03 03:40:15', '2018-09-03 03:58:20', 1, 'zZjfarRY7ROWXpcxTzXClZeW6Jb2hUPB', 'a:6:{s:7:\"field_1\";s:12:\"CEBONG_CAPER\";s:18:\"field_1_visibility\";s:6:\"public\";s:17:\"profile_field_ids\";s:1:\"1\";s:8:\"password\";s:34:\"$P$BLJP8IdjECLIAUrMQheznlsssH8NqO/\";s:9:\"sent_date\";s:19:\"2018-09-03 03:43:40\";s:10:\"count_sent\";i:3;}'),
(2, '', '', '', 'lonazukii', 'riezqwe12@gmail.com', '2018-09-03 03:49:01', '2018-09-03 03:58:51', 1, 'vkA8ZhFcL3F5m0fU4rN3hP565M9XTi9o', 'a:4:{s:7:\"field_1\";s:3:\"kii\";s:18:\"field_1_visibility\";s:6:\"public\";s:17:\"profile_field_ids\";s:1:\"1\";s:8:\"password\";s:34:\"$P$Bou9TxYnL26lggV07H.7OEw.QDjSki.\";}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Parent', 'parent', 0),
(3, 'activity-comment', 'activity-comment', 0),
(4, 'activity-comment-author', 'activity-comment-author', 0),
(5, 'activity-at-message', 'activity-at-message', 0),
(6, 'groups-at-message', 'groups-at-message', 0),
(7, 'core-user-registration', 'core-user-registration', 0),
(8, 'friends-request', 'friends-request', 0),
(9, 'friends-request-accepted', 'friends-request-accepted', 0),
(10, 'groups-details-updated', 'groups-details-updated', 0),
(11, 'groups-invitation', 'groups-invitation', 0),
(12, 'groups-member-promoted', 'groups-member-promoted', 0),
(13, 'groups-membership-request', 'groups-membership-request', 0),
(14, 'messages-unread', 'messages-unread', 0),
(15, 'settings-verify-email-change', 'settings-verify-email-change', 0),
(16, 'groups-membership-request-accepted', 'groups-membership-request-accepted', 0),
(17, 'groups-membership-request-rejected', 'groups-membership-request-rejected', 0),
(18, 'request_reset_password', 'request_reset_password', 0),
(19, 'Main', 'main', 0),
(20, 'Gain Theme', 'gain-theme', 0),
(21, 'Scocial Media', 'scocial-media', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(1, 2, 0),
(6, 2, 0),
(14, 3, 0),
(15, 4, 0),
(16, 5, 0),
(17, 6, 0),
(18, 7, 0),
(19, 8, 0),
(20, 9, 0),
(21, 10, 0),
(22, 11, 0),
(23, 12, 0),
(24, 13, 0),
(25, 14, 0),
(26, 15, 0),
(27, 16, 0),
(28, 17, 0),
(30, 1, 0),
(32, 1, 0),
(34, 1, 0),
(36, 18, 0),
(38, 19, 0),
(45, 19, 0),
(47, 19, 0),
(68, 20, 0),
(69, 20, 0),
(70, 20, 0),
(71, 20, 0),
(72, 20, 0),
(73, 2, 0),
(76, 20, 0),
(77, 19, 0),
(81, 19, 0),
(82, 19, 0),
(83, 19, 0),
(84, 19, 0),
(85, 19, 0),
(87, 19, 0),
(88, 19, 0),
(89, 19, 0),
(91, 19, 0),
(92, 19, 0),
(93, 19, 0),
(94, 19, 0),
(96, 2, 0),
(98, 2, 0),
(108, 21, 0),
(109, 21, 0),
(110, 21, 0),
(111, 2, 0),
(113, 2, 0),
(122, 19, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'category', '', 0, 2),
(3, 3, 'bp-email-type', 'A member has replied to an activity update that the recipient posted.', 0, 1),
(4, 4, 'bp-email-type', 'A member has replied to a comment on an activity update that the recipient posted.', 0, 1),
(5, 5, 'bp-email-type', 'Recipient was mentioned in an activity update.', 0, 1),
(6, 6, 'bp-email-type', 'Recipient was mentioned in a group activity update.', 0, 1),
(7, 7, 'bp-email-type', 'Recipient has registered for an account.', 0, 1),
(8, 8, 'bp-email-type', 'A member has sent a friend request to the recipient.', 0, 1),
(9, 9, 'bp-email-type', 'Recipient has had a friend request accepted by a member.', 0, 1),
(10, 10, 'bp-email-type', 'A group\'s details were updated.', 0, 1),
(11, 11, 'bp-email-type', 'A member has sent a group invitation to the recipient.', 0, 1),
(12, 12, 'bp-email-type', 'Recipient\'s status within a group has changed.', 0, 1),
(13, 13, 'bp-email-type', 'A member has requested permission to join a group.', 0, 1),
(14, 14, 'bp-email-type', 'Recipient has received a private message.', 0, 1),
(15, 15, 'bp-email-type', 'Recipient has changed their email address.', 0, 1),
(16, 16, 'bp-email-type', 'Recipient had requested to join a group, which was accepted.', 0, 1),
(17, 17, 'bp-email-type', 'Recipient had requested to join a group, which was rejected.', 0, 1),
(18, 18, 'bp-email-type', 'A member request a password reset.', 0, 1),
(19, 19, 'nav_menu', '', 0, 17),
(20, 20, 'nav_menu', '', 0, 6),
(21, 21, 'nav_menu', '', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'cebong'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:2:{s:13:\"administrator\";b:1;s:13:\"bbp_keymaster\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy,theme_editor_notice'),
(15, 1, 'show_welcome_panel', '1'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '120'),
(19, 1, '_yoast_wpseo_profile_updated', '1536563716'),
(20, 1, 'closedpostboxes_post', 'a:8:{i:0;s:9:\"submitdiv\";i:1;s:11:\"categorydiv\";i:2;s:16:\"tagsdiv-post_tag\";i:3;s:12:\"select_forum\";i:4;s:15:\"select_nav_menu\";i:5;s:12:\"postimagediv\";i:6;s:10:\"wpseo_meta\";i:7;s:12:\"cfs_input_10\";}'),
(21, 1, 'metaboxhidden_post', 'a:9:{i:0;s:10:\"forum_menu\";i:1;s:8:\"nav_menu\";i:2;s:11:\"postexcerpt\";i:3;s:13:\"trackbacksdiv\";i:4;s:10:\"postcustom\";i:5;s:16:\"commentstatusdiv\";i:6;s:11:\"commentsdiv\";i:7;s:7:\"slugdiv\";i:8;s:9:\"authordiv\";}'),
(22, 1, 'wp_user-settings', 'libraryContent=browse'),
(23, 1, 'wp_user-settings-time', '1535943322'),
(24, 1, 'last_activity', '2018-09-12 07:38:09'),
(25, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:\"link-target\";i:1;s:15:\"title-attribute\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";}'),
(26, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:12:\"add-post_tag\";i:1;s:15:\"add-post_format\";}'),
(27, 2, 'nickname', 'CEBONG_CAPER'),
(28, 2, 'first_name', 'CEBONG_CAPER'),
(29, 2, 'last_name', ''),
(30, 2, 'description', ''),
(31, 2, 'rich_editing', 'true'),
(32, 2, 'syntax_highlighting', 'true'),
(33, 2, 'comment_shortcuts', 'false'),
(34, 2, 'admin_color', 'fresh'),
(35, 2, 'use_ssl', '0'),
(36, 2, 'show_admin_bar_front', 'true'),
(37, 2, 'locale', ''),
(40, 2, '_yoast_wpseo_profile_updated', '1535946015'),
(41, 2, 'bp_xprofile_visibility_levels', 'a:1:{i:1;s:6:\"public\";}'),
(45, 1, 'nav_menu_recently_edited', '20'),
(46, 1, 'closedpostboxes_nav-menus', 'a:0:{}'),
(47, 3, 'nickname', 'kii'),
(48, 3, 'first_name', 'kii'),
(49, 3, 'last_name', ''),
(50, 3, 'description', ''),
(51, 3, 'rich_editing', 'true'),
(52, 3, 'syntax_highlighting', 'true'),
(53, 3, 'comment_shortcuts', 'false'),
(54, 3, 'admin_color', 'fresh'),
(55, 3, 'use_ssl', '0'),
(56, 3, 'show_admin_bar_front', 'true'),
(57, 3, 'locale', ''),
(60, 3, '_yoast_wpseo_profile_updated', '1535946540'),
(61, 3, 'bp_xprofile_visibility_levels', 'a:1:{i:1;s:6:\"public\";}'),
(63, 2, 'wp_capabilities', 'a:2:{s:10:\"subscriber\";b:1;s:15:\"bbp_participant\";b:1;}'),
(64, 2, 'wp_user_level', '0'),
(65, 2, 'googleplus', ''),
(66, 2, 'twitter', ''),
(67, 2, 'facebook', ''),
(68, 3, 'wp_capabilities', 'a:2:{s:10:\"subscriber\";b:1;s:15:\"bbp_participant\";b:1;}'),
(69, 3, 'wp_user_level', '0'),
(70, 3, 'googleplus', ''),
(71, 3, 'twitter', ''),
(72, 3, 'facebook', ''),
(74, 2, 'last_activity', '2018-09-03 05:48:38'),
(76, 2, 'wp__bbp_last_posted', '1535953563'),
(78, 3, 'last_activity', '2018-09-03 05:49:00'),
(79, 3, 'wp__bbp_last_posted', '1535953928'),
(86, 1, 'session_tokens', 'a:1:{s:64:\"455d089445cf1da1975f420b783adf9b1ca6511b539bf8a163b98fd9e358eea0\";a:4:{s:10:\"expiration\";i:1536897700;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:104:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36\";s:5:\"login\";i:1536724900;}}'),
(87, 1, 'wp_yoast_notifications', 'a:2:{i:0;a:2:{s:7:\"message\";s:167:\"Don\'t miss your crawl errors: <a href=\"http://localhost/customp/wp-admin/admin.php?page=wpseo_search_console&tab=settings\">connect with Google Search Console here</a>.\";s:7:\"options\";a:9:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:17:\"wpseo-dismiss-gsc\";s:5:\"nonce\";N;s:8:\"priority\";d:0.5;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:20:\"wpseo_manage_options\";s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}i:1;a:2:{s:7:\"message\";s:328:\"You still have the default WordPress tagline, even an empty one is probably better. <a href=\"http://localhost/customp/wp-admin/customize.php?autofocus[control]=blogdescription&amp;url=http%3A%2F%2Flocalhost%2Fcustomp%2Fwp-admin%2Fadmin.php%3Fpage%3Dbcodes-slider%26action%3Dedit%26id%3D1\">You can fix this in the customizer</a>.\";s:7:\"options\";a:9:{s:4:\"type\";s:5:\"error\";s:2:\"id\";s:28:\"wpseo-dismiss-tagline-notice\";s:5:\"nonce\";N;s:8:\"priority\";d:0.5;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:20:\"wpseo_manage_options\";s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'cebong', '$P$BL21xwaZVQzXBwNamgRzzbPGEuO4PT1', 'cebong', 'aridyansyah.kii@gmail.com', '', '2018-09-03 02:34:32', '', 0, 'cebong'),
(2, 'cebongcaper', '$P$B5mli2YbgAcGhwMGFaYP8Iiegm/BLE.', 'cebongcaper', 'lonazukii@gmail.com', '', '2018-09-03 03:40:14', '', 0, 'CEBONG_CAPER'),
(3, 'lonazukii', '$P$Bhza7o8YAgupfOr7LdSHocDNWA8cln0', 'lonazukii', 'riezqwe12@gmail.com', '', '2018-09-03 03:48:59', '', 0, 'kii');

-- --------------------------------------------------------

--
-- Table structure for table `wp_yoast_seo_links`
--

CREATE TABLE `wp_yoast_seo_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `target_post_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_yoast_seo_links`
--

INSERT INTO `wp_yoast_seo_links` (`id`, `url`, `post_id`, `target_post_id`, `type`) VALUES
(7, 'http://localhost/customp/', 48, 0, 'internal'),
(38, 'http://support.jegtheme.com/forums/forum/jnews/', 6, 0, 'external'),
(39, 'https://themeforest.net/downloads', 6, 0, 'external');

-- --------------------------------------------------------

--
-- Table structure for table `wp_yoast_seo_meta`
--

CREATE TABLE `wp_yoast_seo_meta` (
  `object_id` bigint(20) UNSIGNED NOT NULL,
  `internal_link_count` int(10) UNSIGNED DEFAULT NULL,
  `incoming_link_count` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_yoast_seo_meta`
--

INSERT INTO `wp_yoast_seo_meta` (`object_id`, `internal_link_count`, `incoming_link_count`) VALUES
(1, 0, 0),
(4, 0, 0),
(5, 0, 0),
(6, 0, 0),
(11, 0, 0),
(12, 0, 0),
(13, 0, 0),
(29, 0, 0),
(30, 0, 0),
(32, 0, 0),
(34, 0, 0),
(37, 0, 0),
(39, 0, 0),
(40, 0, 0),
(41, 0, 0),
(42, 0, 0),
(43, 0, 0),
(44, 0, 0),
(46, 0, 0),
(48, 1, 0),
(51, 0, 0),
(53, 0, 0),
(54, 0, 0),
(56, 0, 0),
(58, 0, 0),
(60, 0, 0),
(61, 0, 0),
(63, 0, 0),
(67, 0, 0),
(73, 0, 0),
(86, 0, 0),
(90, 0, 0),
(96, 0, 0),
(98, 0, 0),
(100, 0, 0),
(101, 0, 0),
(102, 0, 0),
(104, 0, 0),
(105, 0, 0),
(106, 0, 0),
(107, 0, 0),
(111, 0, 0),
(113, 0, 0),
(123, 0, 0),
(129, 0, 0),
(130, 0, 0),
(131, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_yuuki_plugin`
--

CREATE TABLE `wp_yuuki_plugin` (
  `id` int(11) NOT NULL,
  `url` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slider_id` int(11) NOT NULL,
  `create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_yuuki_plugin`
--

INSERT INTO `wp_yuuki_plugin` (`id`, `url`, `slider_id`, `create`) VALUES
(1, 'http://localhost/customp/wp-content/uploads/2018/09/679478.jpg', 1, '2018-09-10 07:14:07'),
(3, 'http://localhost/customp/wp-content/uploads/2018/09/alone-back-view-beautiful-674268.jpg', 1, '2018-09-12 07:23:49');

-- --------------------------------------------------------

--
-- Table structure for table `wp_yuuki_slider`
--

CREATE TABLE `wp_yuuki_slider` (
  `id` int(11) NOT NULL,
  `shortcode` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slider_name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_yuuki_slider`
--

INSERT INTO `wp_yuuki_slider` (`id`, `shortcode`, `slider_name`, `meta_value`) VALUES
(1, '[yuuki name=\'LMO\']', 'LMO', '{nav: true, dots: false,\"items\":1}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_yz_bookmark`
--

CREATE TABLE `wp_yz_bookmark` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `collection_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_bp_activity`
--
ALTER TABLE `wp_bp_activity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `date_recorded` (`date_recorded`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `secondary_item_id` (`secondary_item_id`),
  ADD KEY `component` (`component`),
  ADD KEY `type` (`type`),
  ADD KEY `mptt_left` (`mptt_left`),
  ADD KEY `mptt_right` (`mptt_right`),
  ADD KEY `hide_sitewide` (`hide_sitewide`),
  ADD KEY `is_spam` (`is_spam`);

--
-- Indexes for table `wp_bp_activity_meta`
--
ALTER TABLE `wp_bp_activity_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activity_id` (`activity_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_bp_notifications`
--
ALTER TABLE `wp_bp_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `secondary_item_id` (`secondary_item_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `is_new` (`is_new`),
  ADD KEY `component_name` (`component_name`),
  ADD KEY `component_action` (`component_action`),
  ADD KEY `useritem` (`user_id`,`is_new`);

--
-- Indexes for table `wp_bp_notifications_meta`
--
ALTER TABLE `wp_bp_notifications_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notification_id` (`notification_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_bp_xprofile_data`
--
ALTER TABLE `wp_bp_xprofile_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `field_id` (`field_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_bp_xprofile_fields`
--
ALTER TABLE `wp_bp_xprofile_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `field_order` (`field_order`),
  ADD KEY `can_delete` (`can_delete`),
  ADD KEY `is_required` (`is_required`);

--
-- Indexes for table `wp_bp_xprofile_groups`
--
ALTER TABLE `wp_bp_xprofile_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `can_delete` (`can_delete`);

--
-- Indexes for table `wp_bp_xprofile_meta`
--
ALTER TABLE `wp_bp_xprofile_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `object_id` (`object_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_cfs_sessions`
--
ALTER TABLE `wp_cfs_sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_cfs_values`
--
ALTER TABLE `wp_cfs_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `field_id_idx` (`field_id`),
  ADD KEY `post_id_idx` (`post_id`);

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_logy_users`
--
ALTER TABLE `wp_logy_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_signups`
--
ALTER TABLE `wp_signups`
  ADD PRIMARY KEY (`signup_id`),
  ADD KEY `activation_key` (`activation_key`),
  ADD KEY `user_email` (`user_email`),
  ADD KEY `user_login_email` (`user_login`,`user_email`),
  ADD KEY `domain_path` (`domain`(140),`path`(51));

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `wp_yoast_seo_links`
--
ALTER TABLE `wp_yoast_seo_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `link_direction` (`post_id`,`type`);

--
-- Indexes for table `wp_yoast_seo_meta`
--
ALTER TABLE `wp_yoast_seo_meta`
  ADD UNIQUE KEY `object_id` (`object_id`);

--
-- Indexes for table `wp_yuuki_plugin`
--
ALTER TABLE `wp_yuuki_plugin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_yuuki_slider`
--
ALTER TABLE `wp_yuuki_slider`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slider_name` (`slider_name`);

--
-- Indexes for table `wp_yz_bookmark`
--
ALTER TABLE `wp_yz_bookmark`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_bp_activity`
--
ALTER TABLE `wp_bp_activity`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `wp_bp_activity_meta`
--
ALTER TABLE `wp_bp_activity_meta`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_bp_notifications`
--
ALTER TABLE `wp_bp_notifications`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_bp_notifications_meta`
--
ALTER TABLE `wp_bp_notifications_meta`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_bp_xprofile_data`
--
ALTER TABLE `wp_bp_xprofile_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_bp_xprofile_fields`
--
ALTER TABLE `wp_bp_xprofile_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_bp_xprofile_groups`
--
ALTER TABLE `wp_bp_xprofile_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_bp_xprofile_meta`
--
ALTER TABLE `wp_bp_xprofile_meta`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_cfs_values`
--
ALTER TABLE `wp_cfs_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_logy_users`
--
ALTER TABLE `wp_logy_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1047;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=780;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `wp_signups`
--
ALTER TABLE `wp_signups`
  MODIFY `signup_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wp_yoast_seo_links`
--
ALTER TABLE `wp_yoast_seo_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `wp_yuuki_plugin`
--
ALTER TABLE `wp_yuuki_plugin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wp_yuuki_slider`
--
ALTER TABLE `wp_yuuki_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_yz_bookmark`
--
ALTER TABLE `wp_yz_bookmark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
